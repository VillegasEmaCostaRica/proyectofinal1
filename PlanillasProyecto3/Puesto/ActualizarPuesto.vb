﻿Public Class ActualizarPuesto

    Private Sub btnActuCodPuesto_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActuCodPuesto.Click
        
        Try
            codPuesto = Integer.Parse(InputBox("Igrense El Codigo Del Puesto Que Desea Actualizar ", "Codigo Puesto"))
            Dim actualizarPuestoPorCodigo As ActualizarPuestoPorCodigo
            actualizarPuestoPorCodigo = New ActualizarPuestoPorCodigo
            actualizarPuestoPorCodigo.Show()
            Me.Hide()
        Catch ex As Exception
            MsgBox("El Codigo De Puesto Tiene Que ser Númerico")
        End Try
       
    End Sub

    Private Sub ActualizarPuesto_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed

        Principal.Show()
    End Sub

   
   
End Class
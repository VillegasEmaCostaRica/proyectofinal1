﻿Public Class frmCalcularAguinaldo

    Private Sub PlanillaMensualBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.PlanillaMensualBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)

    End Sub

    Private Sub frmCalcularAguinaldo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'Planilla2DataSet.Aguinaldo' Puede moverla o quitarla según sea necesario.
        'Me.AguinaldoTableAdapter.Fill(Me.Planilla2DataSet.Aguinaldo)
        'TODO: esta línea de código carga datos en la tabla 'Planilla2DataSet.DatosPersonales' Puede moverla o quitarla según sea necesario.
        Me.DatosPersonalesTableAdapter.Fill(Me.Planilla2DataSet.DatosPersonales)
        'TODO: esta línea de código carga datos en la tabla 'Planilla2DataSet.PlanillaMensual' Puede moverla o quitarla según sea necesario.
        Me.PlanillaMensualTableAdapter.Fill(Me.Planilla2DataSet.PlanillaMensual)

    End Sub

    Private Sub btnCalcular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCalcular.Click
        Calcular()
    End Sub

    Sub Calcular()

        Dim sumaSalario As Integer
        Dim fechaIngr As DateTime
        Dim cedulaPersona As Integer
        Dim cedulaAguinaldo As Integer
        Dim nombre As String = ""
        For x As Integer = 0 To DatosPersonalesDataGridView.Rows.Count - 3

            For i As Integer = 0 To PlanillaMensualDataGridView.Rows.Count - 3

                Dim salario As Integer = Integer.Parse(PlanillaMensualDataGridView.Rows(i).Cells(4).Value.ToString())
                cedulaPersona = DatosPersonalesDataGridView.Rows(x).Cells(0).Value.ToString()
                nombre = PlanillaMensualDataGridView.Rows(x).Cells(2).Value.ToString()
                cedulaAguinaldo = PlanillaMensualDataGridView.Rows(x).Cells(0).Value.ToString()
                Dim fecha As String = PlanillaMensualDataGridView.Rows(i).Cells(22).Value.ToString()
                fechaIngr = Convert.ToDateTime(fecha)
                Dim canMeses As Integer = DateDiff(DateInterval.Month, fechaIngr, Date.Now())
                Dim AnoIngreso As Integer = fechaIngr.Year
                Dim mesIngreo As Integer = fechaIngr.Month
                Dim anoActual As Integer = Date.Now.Year
                Dim mesActual As Integer = Date.Now.Month


                If cedulaPersona = cedulaAguinaldo Then
                    If canMeses <= 12 Then

                        If (mesIngreo = 12) Or (mesIngreo >= 1 And ((AnoIngreso + 1 = anoActual) Or (AnoIngreso = anoActual))) And (mesIngreo <= 11 And (AnoIngreso + 1 = anoActual) Or (AnoIngreso = anoActual)) Then

                            If (AnoIngreso + 1 = anoActual) Or (AnoIngreso = anoActual) Then

                                If (mesActual <= 12) Then
                                    sumaSalario += salario
                                End If

                            End If

                        End If
                    End If

                End If

            Next

            If cedulaPersona = cedulaAguinaldo Then
                sumaSalario /= 12

                Try
                    Me.AguinaldoTableAdapter.InsertAguinaldo(cedulaPersona, nombre, sumaSalario, fechaIngr)
                    Me.AguinaldoTableAdapter.Fill(Me.Planilla2DataSet.Aguinaldo)
                    sumaSalario = 0
                Catch ex As Exception
                    Me.AguinaldoTableAdapter.UpdateAguinaldo(cedulaPersona, nombre, sumaSalario, fechaIngr, cedulaPersona)
                    Me.AguinaldoTableAdapter.Fill(Me.Planilla2DataSet.Aguinaldo)
                    sumaSalario = 0
                End Try


            End If
        Next


    End Sub


    Private Sub frmCalcularAguinaldo_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton6.MouseLeave, ToolStripButton1.MouseLeave, btnCalcular.MouseLeave, ArchivoToolStripMenuItem.MouseLeave, AccionesToolStripMenuItem.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub

 
    Private Sub btnCalcular_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCalcular.MouseHover
        ToolStripStatusLabel1.Text = "Calcular el Aguinaldo De  Los Empleados"
    End Sub

    Private Sub CalcularToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CalcularToolStripMenuItem.Click
        Calcular()
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        Calcular()
    End Sub

    Private Sub ToolStripButton6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton6.Click
        Me.Hide()
        frmPrincipal.Show()
    End Sub

    Private Sub RegesarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegesarToolStripMenuItem.Click
        Me.Hide()
        frmPrincipal.Show()
    End Sub

   

    Private Sub CalcularToolStripMenuItem_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CalcularToolStripMenuItem.MouseHover
        ToolStripStatusLabel1.Text = "Calcular Aguinaldo de los Empleados"
    End Sub

    Private Sub CalcularToolStripMenuItem_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CalcularToolStripMenuItem.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    Private Sub ToolStripButton6_MouseMove(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles ToolStripButton6.MouseMove
        ToolStripStatusLabel1.Text = "Regresar a la Paguina Principal"
    End Sub

    Private Sub ToolStripButton1_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.MouseHover
        ToolStripStatusLabel1.Text = "Calcular Aguinaldo de los Empleados"
    End Sub

    Private Sub RegesarToolStripMenuItem_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegesarToolStripMenuItem.MouseHover
        ToolStripStatusLabel1.Text = "Regresar a la Paguina Principal"
    End Sub

    Private Sub RegesarToolStripMenuItem_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegesarToolStripMenuItem.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub
End Class
﻿Public Class frmConsultarAguinaldo

    Private Sub AguinaldoBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.AguinaldoBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)

    End Sub

    Private Sub frmConsultarAguinaldo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'Planilla2DataSet.Aguinaldo' Puede moverla o quitarla según sea necesario.
        Me.AguinaldoTableAdapter.Fill(Me.Planilla2DataSet.Aguinaldo)

    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        buscar()
    End Sub
    Sub buscar()
        Dim tipoBusqueda As Integer = Integer.Parse(cmbTipo.SelectedIndex)
        Try


            If AguinaldoTableAdapter.buscarAguinaldo(Me.Planilla2DataSet.Aguinaldo, txtDato.Text, txtDato.Text) = "0" Then
                MsgBox("No se Encontro Ningun Registro")
                Me.AguinaldoTableAdapter.Fill(Me.Planilla2DataSet.Aguinaldo)
            End If


        Catch ex As Exception
            MsgBox("No se Encontro Ningun Registro")
        End Try
    End Sub

    Private Sub ToolStripButton6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton6.Click
        Me.Hide()
        frmPrincipal.Show()
    End Sub

    Private Sub frmConsultarAguinaldo_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDato.MouseLeave, ToolStripButton6.MouseLeave, ToolStripButton1.MouseLeave, RegesarToolStripMenuItem.MouseLeave, MyBase.MouseLeave, cmbTipo.MouseLeave, btnBuscar.MouseLeave, ArchivoToolStripMenuItem.MouseLeave, AccionesToolStripMenuItem.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    Private Sub RegesarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegesarToolStripMenuItem.Click
        Me.Hide()
        frmPrincipal.Show()
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        buscar()
    End Sub

    Private Sub CalcularToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CalcularToolStripMenuItem.Click
        buscar()
    End Sub

    Private Sub ToolStripButton6_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton6.MouseHover
        ToolStripStatusLabel1.Text = "Regresar a la Pagina Principal"
    End Sub

    Private Sub ToolStripButton1_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.MouseHover
        ToolStripStatusLabel1.Text = "Calcular el Aguinaldo De los Empleados"
    End Sub

    Private Sub cmbTipo_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbTipo.MouseHover
        ToolStripStatusLabel1.Text = "Seleccionar El tipo de Dato que quiere realizar la Busqueda"
    End Sub

    Private Sub txtDato_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDato.MouseHover
        ToolStripStatusLabel1.Text = "Dato Por el cual se va Hacer la busqueda"
    End Sub

    Private Sub btnBuscar_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.MouseHover
        ToolStripStatusLabel1.Text = "Realizar La Busqueda del Aguinaldo"
    End Sub

    Private Sub RegesarToolStripMenuItem_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegesarToolStripMenuItem.MouseHover
        ToolStripStatusLabel1.Text = "Regresar a la Pagina Principal"
    End Sub

    Private Sub CalcularToolStripMenuItem_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CalcularToolStripMenuItem.MouseHover
        ToolStripStatusLabel1.Text = "Realizar La Busqueda del Aguinaldo"
    End Sub

    Private Sub CalcularToolStripMenuItem_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CalcularToolStripMenuItem.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class formUsuario
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ContraLabel As System.Windows.Forms.Label
        Dim UsuarioLabel As System.Windows.Forms.Label
        Me.btnIngresar = New System.Windows.Forms.Button()
        Me.ContraTextBox = New System.Windows.Forms.TextBox()
        Me.UsuarioTextBox = New System.Windows.Forms.TextBox()
        Me.UsuarioRegistroBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Planilla2DataSet2 = New PlanillasProyecto3.Planilla2DataSet2()
        Me.UsuarioRegistroTableAdapter1 = New PlanillasProyecto3.Planilla2DataSet2TableAdapters.UsuarioRegistroTableAdapter()
        Me.TableAdapterManager2 = New PlanillasProyecto3.Planilla2DataSet2TableAdapters.TableAdapterManager()
        Me.Planilla2DataSet1 = New PlanillasProyecto3.Planilla2DataSet()
        Me.UsuarioRegistroBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.UsuarioRegistroTableAdapter = New PlanillasProyecto3.Planilla2DataSetTableAdapters.UsuarioRegistroTableAdapter()
        Me.TableAdapterManager1 = New PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager()
        Me.UsuarioRegistroDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        ContraLabel = New System.Windows.Forms.Label()
        UsuarioLabel = New System.Windows.Forms.Label()
        CType(Me.UsuarioRegistroBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Planilla2DataSet2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Planilla2DataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UsuarioRegistroBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UsuarioRegistroDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ContraLabel
        '
        ContraLabel.AutoSize = True
        ContraLabel.Location = New System.Drawing.Point(207, 137)
        ContraLabel.Name = "ContraLabel"
        ContraLabel.Size = New System.Drawing.Size(41, 13)
        ContraLabel.TabIndex = 4
        ContraLabel.Text = "Contra:"
        '
        'UsuarioLabel
        '
        UsuarioLabel.AutoSize = True
        UsuarioLabel.Location = New System.Drawing.Point(207, 163)
        UsuarioLabel.Name = "UsuarioLabel"
        UsuarioLabel.Size = New System.Drawing.Size(46, 13)
        UsuarioLabel.TabIndex = 6
        UsuarioLabel.Text = "Usuario:"
        '
        'btnIngresar
        '
        Me.btnIngresar.Location = New System.Drawing.Point(230, 219)
        Me.btnIngresar.Name = "btnIngresar"
        Me.btnIngresar.Size = New System.Drawing.Size(75, 23)
        Me.btnIngresar.TabIndex = 4
        Me.btnIngresar.Text = "&Ingresar"
        Me.btnIngresar.UseVisualStyleBackColor = True
        '
        'ContraTextBox
        '
        Me.ContraTextBox.Location = New System.Drawing.Point(283, 134)
        Me.ContraTextBox.Name = "ContraTextBox"
        Me.ContraTextBox.Size = New System.Drawing.Size(100, 20)
        Me.ContraTextBox.TabIndex = 5
        '
        'UsuarioTextBox
        '
        Me.UsuarioTextBox.Location = New System.Drawing.Point(283, 193)
        Me.UsuarioTextBox.Name = "UsuarioTextBox"
        Me.UsuarioTextBox.Size = New System.Drawing.Size(100, 20)
        Me.UsuarioTextBox.TabIndex = 7
        '
        'UsuarioRegistroBindingSource1
        '
        Me.UsuarioRegistroBindingSource1.DataMember = "UsuarioRegistro"
        Me.UsuarioRegistroBindingSource1.DataSource = Me.Planilla2DataSet2
        '
        'Planilla2DataSet2
        '
        Me.Planilla2DataSet2.DataSetName = "Planilla2DataSet2"
        Me.Planilla2DataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'UsuarioRegistroTableAdapter1
        '
        Me.UsuarioRegistroTableAdapter1.ClearBeforeFill = True
        '
        'TableAdapterManager2
        '
        Me.TableAdapterManager2.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager2.UpdateOrder = PlanillasProyecto3.Planilla2DataSet2TableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager2.UsuarioRegistroTableAdapter = Me.UsuarioRegistroTableAdapter1
        '
        'Planilla2DataSet1
        '
        Me.Planilla2DataSet1.DataSetName = "Planilla2DataSet"
        Me.Planilla2DataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'UsuarioRegistroBindingSource
        '
        Me.UsuarioRegistroBindingSource.DataMember = "UsuarioRegistro"
        Me.UsuarioRegistroBindingSource.DataSource = Me.Planilla2DataSet1
        '
        'UsuarioRegistroTableAdapter
        '
        Me.UsuarioRegistroTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager1
        '
        Me.TableAdapterManager1.AguinaldoTableAdapter = Nothing
        Me.TableAdapterManager1.AjustesTableAdapter = Nothing
        Me.TableAdapterManager1.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager1.DatosPersonalesTableAdapter = Nothing
        Me.TableAdapterManager1.DeduccionesTableAdapter = Nothing
        Me.TableAdapterManager1.ImpuestosTableAdapter = Nothing
        Me.TableAdapterManager1.PlanillaBisemanalTableAdapter = Nothing
        Me.TableAdapterManager1.PlanillaMensualTableAdapter = Nothing
        Me.TableAdapterManager1.PuestosTableAdapter = Nothing
        Me.TableAdapterManager1.UpdateOrder = PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager1.UsuarioRegistroTableAdapter = Me.UsuarioRegistroTableAdapter
        Me.TableAdapterManager1.VacacionesTableAdapter = Nothing
        '
        'UsuarioRegistroDataGridView
        '
        Me.UsuarioRegistroDataGridView.AutoGenerateColumns = False
        Me.UsuarioRegistroDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.UsuarioRegistroDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6})
        Me.UsuarioRegistroDataGridView.DataSource = Me.UsuarioRegistroBindingSource
        Me.UsuarioRegistroDataGridView.Location = New System.Drawing.Point(146, 248)
        Me.UsuarioRegistroDataGridView.Name = "UsuarioRegistroDataGridView"
        Me.UsuarioRegistroDataGridView.Size = New System.Drawing.Size(300, 220)
        Me.UsuarioRegistroDataGridView.TabIndex = 7
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Contra"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Contra"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Usuario"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Usuario"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "TipoUsuario"
        Me.DataGridViewTextBoxColumn6.HeaderText = "TipoUsuario"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        '
        'formUsuario
        '
        Me.ClientSize = New System.Drawing.Size(582, 630)
        Me.Controls.Add(Me.UsuarioRegistroDataGridView)
        Me.Controls.Add(ContraLabel)
        Me.Controls.Add(Me.ContraTextBox)
        Me.Controls.Add(UsuarioLabel)
        Me.Controls.Add(Me.UsuarioTextBox)
        Me.Controls.Add(Me.btnIngresar)
        Me.Name = "formUsuario"
        CType(Me.UsuarioRegistroBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Planilla2DataSet2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Planilla2DataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UsuarioRegistroBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UsuarioRegistroDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtUsuario As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnIngresarPre As System.Windows.Forms.Button
    Friend WithEvents Planilla2DataSet As PlanillasProyecto3.Planilla2DataSet
    Friend WithEvents RegistroUsuarioBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TableAdapterManager As PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager
    Friend WithEvents txtContrasena As System.Windows.Forms.MaskedTextBox
    Friend WithEvents DatosPersonalesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DatosPersonalesTableAdapter As PlanillasProyecto3.Planilla2DataSetTableAdapters.DatosPersonalesTableAdapter
    Friend WithEvents RegistroUsuarioDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnIngresar As System.Windows.Forms.Button
    Friend WithEvents ContraTextBox As System.Windows.Forms.TextBox
    Friend WithEvents UsuarioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Planilla2DataSet2 As PlanillasProyecto3.Planilla2DataSet2
    Friend WithEvents UsuarioRegistroBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents UsuarioRegistroTableAdapter1 As PlanillasProyecto3.Planilla2DataSet2TableAdapters.UsuarioRegistroTableAdapter
    Friend WithEvents TableAdapterManager2 As PlanillasProyecto3.Planilla2DataSet2TableAdapters.TableAdapterManager
    Friend WithEvents Planilla2DataSet1 As PlanillasProyecto3.Planilla2DataSet
    Friend WithEvents UsuarioRegistroBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents UsuarioRegistroTableAdapter As PlanillasProyecto3.Planilla2DataSetTableAdapters.UsuarioRegistroTableAdapter
    Friend WithEvents TableAdapterManager1 As PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager
    Friend WithEvents UsuarioRegistroDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

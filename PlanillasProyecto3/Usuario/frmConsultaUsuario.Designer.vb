﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConsultaUsuario
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Planilla2DataSet = New PlanillasProyecto3.Planilla2DataSet()
        Me.UsuarioRegistroBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.UsuarioRegistroTableAdapter = New PlanillasProyecto3.Planilla2DataSetTableAdapters.UsuarioRegistroTableAdapter()
        Me.TableAdapterManager = New PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cmbTipo = New System.Windows.Forms.ComboBox()
        Me.txtDato = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UsuarioRegistroDataGridView1 = New System.Windows.Forms.DataGridView()
        CType(Me.Planilla2DataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UsuarioRegistroBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.UsuarioRegistroDataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Planilla2DataSet
        '
        Me.Planilla2DataSet.DataSetName = "Planilla2DataSet"
        Me.Planilla2DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'UsuarioRegistroBindingSource
        '
        Me.UsuarioRegistroBindingSource.DataMember = "UsuarioRegistro"
        Me.UsuarioRegistroBindingSource.DataSource = Me.Planilla2DataSet
        '
        'UsuarioRegistroTableAdapter
        '
        Me.UsuarioRegistroTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.AguinaldoTableAdapter = Nothing
        Me.TableAdapterManager.AjustesTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.DatosPersonalesTableAdapter = Nothing
        Me.TableAdapterManager.DeduccionesTableAdapter = Nothing
        Me.TableAdapterManager.ImpuestosTableAdapter = Nothing
        Me.TableAdapterManager.PlanillaBisemanalTableAdapter = Nothing
        Me.TableAdapterManager.PlanillaMensualTableAdapter = Nothing
        Me.TableAdapterManager.PuestosTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.UsuarioRegistroTableAdapter = Me.UsuarioRegistroTableAdapter
        Me.TableAdapterManager.VacacionesTableAdapter = Nothing
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmbTipo)
        Me.GroupBox1.Controls.Add(Me.txtDato)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.btnBuscar)
        Me.GroupBox1.Location = New System.Drawing.Point(62, 43)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(444, 87)
        Me.GroupBox1.TabIndex = 13
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Dato Para Consultar Usuario"
        '
        'cmbTipo
        '
        Me.cmbTipo.FormattingEnabled = True
        Me.cmbTipo.Items.AddRange(New Object() {"Nombre", "Contraseña", "Tipo Usuario"})
        Me.cmbTipo.Location = New System.Drawing.Point(62, 50)
        Me.cmbTipo.Name = "cmbTipo"
        Me.cmbTipo.Size = New System.Drawing.Size(121, 21)
        Me.cmbTipo.TabIndex = 13
        '
        'txtDato
        '
        Me.txtDato.Location = New System.Drawing.Point(230, 50)
        Me.txtDato.Name = "txtDato"
        Me.txtDato.Size = New System.Drawing.Size(108, 20)
        Me.txtDato.TabIndex = 12
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(208, 34)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(81, 13)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = "Dato Busqueda"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 31)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(122, 16)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Tipo De Busqueda"
        '
        'btnBuscar
        '
        Me.btnBuscar.Location = New System.Drawing.Point(363, 47)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(75, 23)
        Me.btnBuscar.TabIndex = 6
        Me.btnBuscar.Text = "&Buscar"
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "TipoUsuario"
        Me.DataGridViewTextBoxColumn6.HeaderText = "TipoUsuario"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Usuario"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Usuario"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Contra"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Contra"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        '
        'UsuarioRegistroDataGridView1
        '
        Me.UsuarioRegistroDataGridView1.AutoGenerateColumns = False
        Me.UsuarioRegistroDataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.UsuarioRegistroDataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6})
        Me.UsuarioRegistroDataGridView1.DataSource = Me.UsuarioRegistroBindingSource
        Me.UsuarioRegistroDataGridView1.Location = New System.Drawing.Point(124, 170)
        Me.UsuarioRegistroDataGridView1.Name = "UsuarioRegistroDataGridView1"
        Me.UsuarioRegistroDataGridView1.Size = New System.Drawing.Size(343, 220)
        Me.UsuarioRegistroDataGridView1.TabIndex = 13
        '
        'frmConsultaUsuario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(577, 411)
        Me.Controls.Add(Me.UsuarioRegistroDataGridView1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmConsultaUsuario"
        Me.Text = "frmConsultaUsuario"
        CType(Me.Planilla2DataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UsuarioRegistroBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.UsuarioRegistroDataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Planilla2DataSet As PlanillasProyecto3.Planilla2DataSet
    Friend WithEvents UsuarioRegistroBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents UsuarioRegistroTableAdapter As PlanillasProyecto3.Planilla2DataSetTableAdapters.UsuarioRegistroTableAdapter
    Friend WithEvents TableAdapterManager As PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
    Friend WithEvents txtDato As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmbTipo As System.Windows.Forms.ComboBox
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UsuarioRegistroDataGridView1 As System.Windows.Forms.DataGridView
End Class

﻿Public Class formUsuario

    Public Shared tipoEmpleado As String

    Private Sub formUsuario_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'Planilla2DataSet1.UsuarioRegistro' Puede moverla o quitarla según sea necesario.
        Me.UsuarioRegistroTableAdapter.Fill(Me.Planilla2DataSet1.UsuarioRegistro)
        'TODO: esta línea de código carga datos en la tabla 'Planilla2DataSet2.UsuarioRegistro' Puede moverla o quitarla según sea necesario.
        Me.UsuarioRegistroTableAdapter1.Fill(Me.Planilla2DataSet2.UsuarioRegistro)
        'TODO: esta línea de código carga datos en la tabla 'Planilla2DataSet1.UsuarioRegistro' Puede moverla o quitarla según sea necesario.
        Me.UsuarioRegistroTableAdapter1.Fill(Me.Planilla2DataSet2.UsuarioRegistro)
        'TODO: esta línea de código carga datos en la tabla 'Planilla2DataSet1.UsuarioRegistro' Puede moverla o quitarla según sea necesario.
        Me.UsuarioRegistroTableAdapter1.Fill(Me.Planilla2DataSet2.UsuarioRegistro)
        'TODO: esta línea de código carga datos en la tabla 'Planilla2DataSet.DatosPersonales' Puede moverla o quitarla según sea necesario.

        'TODO: esta línea de código carga datos en la tabla 'Planilla2DataSet.RegistroUsuario' Puede moverla o quitarla según sea necesario.


    End Sub

    Private Sub btnIngresarSinPre_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Hide()
        frmPrincipal.Show()

    End Sub


    Private Sub RegistroUsuarioBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.RegistroUsuarioBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)

    End Sub

    Private Sub RegistroUsuarioBindingSource1BindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.TableAdapterManager2.UpdateAll(Me.Planilla2DataSet2)

    End Sub

    Private Sub formUsuario_Load_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'Planilla2DataSet1.RegistroUsuario' Puede moverla o quitarla según sea necesario.


    End Sub

    Private Sub UsuarioRegistroBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.UsuarioRegistroDataGridView.EndEdit()
        Me.TableAdapterManager2.UpdateAll(Me.Planilla2DataSet2)

    End Sub

    Private Sub UsuarioRegistroBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.UsuarioRegistroBindingSource1.EndEdit()
        Me.TableAdapterManager2.UpdateAll(Me.Planilla2DataSet2)

    End Sub

    Private Sub btnIngresar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIngresar.Click

        Try
            Dim contra As String = Me.UsuarioRegistroTableAdapter1.FillBy(Me.Planilla2DataSet2.UsuarioRegistro, ContraTextBox.Text)
            Dim usu As String = Me.UsuarioRegistroTableAdapter1.FillBy2(Me.Planilla2DataSet2.UsuarioRegistro, UsuarioTextBox.Text)

            If contra = "1" And usu = "1" Then

                If Me.UsuarioRegistroTableAdapter1.FillBy3(Me.Planilla2DataSet2.UsuarioRegistro, "Administrador", ContraTextBox.Text) = "1" Then

                    Me.Hide()
                    frmPrincipal.Show()

                ElseIf Me.UsuarioRegistroTableAdapter1.FillBy3(Me.Planilla2DataSet2.UsuarioRegistro, "Digitador", ContraTextBox.Text) = "1" Then
                    frmPrincipal.EliminarDatosToolStripMenuItem.Enabled = False
                    frmPrincipal.EliminarPuestoToolStripMenuItem.Enabled = False
                    Me.Hide()
                    frmPrincipal.Show()
                ElseIf Me.UsuarioRegistroTableAdapter1.FillBy3(Me.Planilla2DataSet2.UsuarioRegistro, "usuario Restringido", ContraTextBox.Text) = "1" Then
                    frmPrincipal.IngresarDatosToolStripMenuItem.Enabled = False
                    frmPrincipal.ActualizarDatosToolStripMenuItem.Enabled = False
                    frmPrincipal.EliminarDatosToolStripMenuItem.Enabled = False
                    frmPrincipal.IngresarPuestosToolStripMenuItem.Enabled = False
                    frmPrincipal.ActualizarPuestoToolStripMenuItem.Enabled = False
                    frmPrincipal.EliminarPuestoToolStripMenuItem.Enabled = False
                    Me.Hide()
                    frmPrincipal.Show()
                End If
            Else
                MsgBox("Incorrecto El Usuario O Contraseña")
                ' contrasenaIngresada = ""
                ' usuarioIngresado = ""
            End If


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try


    End Sub

    
End Class
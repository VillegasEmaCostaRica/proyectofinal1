﻿Public Class ActualizarPorNombre

    Private Sub DatosPersonalesBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DatosPersonalesBindingNavigatorSaveItem.Click
        Me.Validate()
        Me.DatosPersonalesBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)

    End Sub

    Private Sub ActualizarPorNombre_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'Planilla2DataSet.DatosPersonales' Puede moverla o quitarla según sea necesario.
        Me.DatosPersonalesTableAdapter.Fill(Me.Planilla2DataSet.DatosPersonales)

    End Sub

    

    Private Sub ActualizarPorNombre_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed

        Principal.Show()
    End Sub
End Class
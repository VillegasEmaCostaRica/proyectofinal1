﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class GuardarPersona
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim NoCEDULALabel As System.Windows.Forms.Label
        Dim _1__APELLIDOLabel As System.Windows.Forms.Label
        Dim _2__APELLIDOLabel As System.Windows.Forms.Label
        Dim NOMBRELabel As System.Windows.Forms.Label
        Dim E_MAILLabel As System.Windows.Forms.Label
        Dim CODPUESTOLabel As System.Windows.Forms.Label
        Dim FECHA_INGRESOLabel As System.Windows.Forms.Label
        Dim N__CONYUGESLabel As System.Windows.Forms.Label
        Dim N__HIJOSLabel As System.Windows.Forms.Label
        Dim GRADO_ACADEMICOLabel As System.Windows.Forms.Label
        Dim BANCO_A_DEPOSITARLabel As System.Windows.Forms.Label
        Dim NoCUENTALabel As System.Windows.Forms.Label
        Dim TelefonoLabel As System.Windows.Forms.Label
        Dim CelularLabel As System.Windows.Forms.Label
        Dim DireccionLabel As System.Windows.Forms.Label
        Me.DatosPersonalesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Planilla2DataSet = New Proyecto3.Planilla2DataSet()
        Me.NoCEDULATextBox = New System.Windows.Forms.TextBox()
        Me._1__APELLIDOTextBox = New System.Windows.Forms.TextBox()
        Me._2__APELLIDOTextBox = New System.Windows.Forms.TextBox()
        Me.NOMBRETextBox = New System.Windows.Forms.TextBox()
        Me.E_MAILTextBox = New System.Windows.Forms.TextBox()
        Me.CODPUESTOTextBox = New System.Windows.Forms.TextBox()
        Me.FECHA_INGRESODateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.N__CONYUGESCheckBox = New System.Windows.Forms.CheckBox()
        Me.N__HIJOSTextBox = New System.Windows.Forms.TextBox()
        Me.GRADO_ACADEMICOTextBox = New System.Windows.Forms.TextBox()
        Me.BANCO_A_DEPOSITARTextBox = New System.Windows.Forms.TextBox()
        Me.NoCUENTATextBox = New System.Windows.Forms.TextBox()
        Me.TelefonoTextBox = New System.Windows.Forms.TextBox()
        Me.CelularTextBox = New System.Windows.Forms.TextBox()
        Me.DireccionTextBox = New System.Windows.Forms.TextBox()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.DatosPersonalesTableAdapter = New Proyecto3.Planilla2DataSetTableAdapters.DatosPersonalesTableAdapter()
        Me.TableAdapterManager = New Proyecto3.Planilla2DataSetTableAdapters.TableAdapterManager()
        Me.proveedorError = New System.Windows.Forms.ErrorProvider(Me.components)
        NoCEDULALabel = New System.Windows.Forms.Label()
        _1__APELLIDOLabel = New System.Windows.Forms.Label()
        _2__APELLIDOLabel = New System.Windows.Forms.Label()
        NOMBRELabel = New System.Windows.Forms.Label()
        E_MAILLabel = New System.Windows.Forms.Label()
        CODPUESTOLabel = New System.Windows.Forms.Label()
        FECHA_INGRESOLabel = New System.Windows.Forms.Label()
        N__CONYUGESLabel = New System.Windows.Forms.Label()
        N__HIJOSLabel = New System.Windows.Forms.Label()
        GRADO_ACADEMICOLabel = New System.Windows.Forms.Label()
        BANCO_A_DEPOSITARLabel = New System.Windows.Forms.Label()
        NoCUENTALabel = New System.Windows.Forms.Label()
        TelefonoLabel = New System.Windows.Forms.Label()
        CelularLabel = New System.Windows.Forms.Label()
        DireccionLabel = New System.Windows.Forms.Label()
        CType(Me.DatosPersonalesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Planilla2DataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.proveedorError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'NoCEDULALabel
        '
        NoCEDULALabel.AutoSize = True
        NoCEDULALabel.Location = New System.Drawing.Point(155, 154)
        NoCEDULALabel.Name = "NoCEDULALabel"
        NoCEDULALabel.Size = New System.Drawing.Size(70, 13)
        NoCEDULALabel.TabIndex = 1
        NoCEDULALabel.Text = "No CEDULA:"
        '
        '_1__APELLIDOLabel
        '
        _1__APELLIDOLabel.AutoSize = True
        _1__APELLIDOLabel.Location = New System.Drawing.Point(155, 180)
        _1__APELLIDOLabel.Name = "_1__APELLIDOLabel"
        _1__APELLIDOLabel.Size = New System.Drawing.Size(75, 13)
        _1__APELLIDOLabel.TabIndex = 3
        _1__APELLIDOLabel.Text = "1° APELLIDO:"
        '
        '_2__APELLIDOLabel
        '
        _2__APELLIDOLabel.AutoSize = True
        _2__APELLIDOLabel.Location = New System.Drawing.Point(155, 206)
        _2__APELLIDOLabel.Name = "_2__APELLIDOLabel"
        _2__APELLIDOLabel.Size = New System.Drawing.Size(75, 13)
        _2__APELLIDOLabel.TabIndex = 5
        _2__APELLIDOLabel.Text = "2° APELLIDO:"
        '
        'NOMBRELabel
        '
        NOMBRELabel.AutoSize = True
        NOMBRELabel.Location = New System.Drawing.Point(155, 232)
        NOMBRELabel.Name = "NOMBRELabel"
        NOMBRELabel.Size = New System.Drawing.Size(57, 13)
        NOMBRELabel.TabIndex = 7
        NOMBRELabel.Text = "NOMBRE:"
        '
        'E_MAILLabel
        '
        E_MAILLabel.AutoSize = True
        E_MAILLabel.Location = New System.Drawing.Point(155, 284)
        E_MAILLabel.Name = "E_MAILLabel"
        E_MAILLabel.Size = New System.Drawing.Size(45, 13)
        E_MAILLabel.TabIndex = 11
        E_MAILLabel.Text = "E-MAIL:"
        '
        'CODPUESTOLabel
        '
        CODPUESTOLabel.AutoSize = True
        CODPUESTOLabel.Location = New System.Drawing.Point(155, 310)
        CODPUESTOLabel.Name = "CODPUESTOLabel"
        CODPUESTOLabel.Size = New System.Drawing.Size(80, 13)
        CODPUESTOLabel.TabIndex = 13
        CODPUESTOLabel.Text = "COD PUESTO:"
        '
        'FECHA_INGRESOLabel
        '
        FECHA_INGRESOLabel.AutoSize = True
        FECHA_INGRESOLabel.Location = New System.Drawing.Point(155, 337)
        FECHA_INGRESOLabel.Name = "FECHA_INGRESOLabel"
        FECHA_INGRESOLabel.Size = New System.Drawing.Size(97, 13)
        FECHA_INGRESOLabel.TabIndex = 15
        FECHA_INGRESOLabel.Text = "FECHA INGRESO:"
        '
        'N__CONYUGESLabel
        '
        N__CONYUGESLabel.AutoSize = True
        N__CONYUGESLabel.Location = New System.Drawing.Point(155, 390)
        N__CONYUGESLabel.Name = "N__CONYUGESLabel"
        N__CONYUGESLabel.Size = New System.Drawing.Size(54, 13)
        N__CONYUGESLabel.TabIndex = 19
        N__CONYUGESLabel.Text = "CASADO:"
        '
        'N__HIJOSLabel
        '
        N__HIJOSLabel.AutoSize = True
        N__HIJOSLabel.Location = New System.Drawing.Point(155, 418)
        N__HIJOSLabel.Name = "N__HIJOSLabel"
        N__HIJOSLabel.Size = New System.Drawing.Size(56, 13)
        N__HIJOSLabel.TabIndex = 21
        N__HIJOSLabel.Text = "N° HIJOS:"
        '
        'GRADO_ACADEMICOLabel
        '
        GRADO_ACADEMICOLabel.AutoSize = True
        GRADO_ACADEMICOLabel.Location = New System.Drawing.Point(155, 444)
        GRADO_ACADEMICOLabel.Name = "GRADO_ACADEMICOLabel"
        GRADO_ACADEMICOLabel.Size = New System.Drawing.Size(115, 13)
        GRADO_ACADEMICOLabel.TabIndex = 23
        GRADO_ACADEMICOLabel.Text = "GRADO ACADEMICO:"
        '
        'BANCO_A_DEPOSITARLabel
        '
        BANCO_A_DEPOSITARLabel.AutoSize = True
        BANCO_A_DEPOSITARLabel.Location = New System.Drawing.Point(155, 470)
        BANCO_A_DEPOSITARLabel.Name = "BANCO_A_DEPOSITARLabel"
        BANCO_A_DEPOSITARLabel.Size = New System.Drawing.Size(122, 13)
        BANCO_A_DEPOSITARLabel.TabIndex = 25
        BANCO_A_DEPOSITARLabel.Text = "BANCO A DEPOSITAR:"
        '
        'NoCUENTALabel
        '
        NoCUENTALabel.AutoSize = True
        NoCUENTALabel.Location = New System.Drawing.Point(155, 496)
        NoCUENTALabel.Name = "NoCUENTALabel"
        NoCUENTALabel.Size = New System.Drawing.Size(105, 13)
        NoCUENTALabel.TabIndex = 27
        NoCUENTALabel.Text = "NUMERO CUENTA:"
        '
        'TelefonoLabel
        '
        TelefonoLabel.AutoSize = True
        TelefonoLabel.Location = New System.Drawing.Point(155, 522)
        TelefonoLabel.Name = "TelefonoLabel"
        TelefonoLabel.Size = New System.Drawing.Size(67, 13)
        TelefonoLabel.TabIndex = 29
        TelefonoLabel.Text = "TELEFONO:"
        '
        'CelularLabel
        '
        CelularLabel.AutoSize = True
        CelularLabel.Location = New System.Drawing.Point(155, 548)
        CelularLabel.Name = "CelularLabel"
        CelularLabel.Size = New System.Drawing.Size(59, 13)
        CelularLabel.TabIndex = 31
        CelularLabel.Text = "CELULAR:"
        '
        'DireccionLabel
        '
        DireccionLabel.AutoSize = True
        DireccionLabel.Location = New System.Drawing.Point(155, 574)
        DireccionLabel.Name = "DireccionLabel"
        DireccionLabel.Size = New System.Drawing.Size(69, 13)
        DireccionLabel.TabIndex = 33
        DireccionLabel.Text = "DIRECCION:"
        '
        'DatosPersonalesBindingSource
        '
        Me.DatosPersonalesBindingSource.DataMember = "DatosPersonales"
        Me.DatosPersonalesBindingSource.DataSource = Me.Planilla2DataSet
        '
        'Planilla2DataSet
        '
        Me.Planilla2DataSet.DataSetName = "Planilla2DataSet"
        Me.Planilla2DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'NoCEDULATextBox
        '
        Me.NoCEDULATextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "NoCEDULA", True))
        Me.NoCEDULATextBox.Location = New System.Drawing.Point(283, 151)
        Me.NoCEDULATextBox.Name = "NoCEDULATextBox"
        Me.NoCEDULATextBox.Size = New System.Drawing.Size(200, 20)
        Me.NoCEDULATextBox.TabIndex = 2
        '
        '_1__APELLIDOTextBox
        '
        Me._1__APELLIDOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "1° APELLIDO", True))
        Me._1__APELLIDOTextBox.Location = New System.Drawing.Point(283, 177)
        Me._1__APELLIDOTextBox.Name = "_1__APELLIDOTextBox"
        Me._1__APELLIDOTextBox.Size = New System.Drawing.Size(200, 20)
        Me._1__APELLIDOTextBox.TabIndex = 4
        '
        '_2__APELLIDOTextBox
        '
        Me._2__APELLIDOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "2° APELLIDO", True))
        Me._2__APELLIDOTextBox.Location = New System.Drawing.Point(283, 203)
        Me._2__APELLIDOTextBox.Name = "_2__APELLIDOTextBox"
        Me._2__APELLIDOTextBox.Size = New System.Drawing.Size(200, 20)
        Me._2__APELLIDOTextBox.TabIndex = 6
        '
        'NOMBRETextBox
        '
        Me.NOMBRETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "NOMBRE", True))
        Me.NOMBRETextBox.Location = New System.Drawing.Point(283, 229)
        Me.NOMBRETextBox.Name = "NOMBRETextBox"
        Me.NOMBRETextBox.Size = New System.Drawing.Size(200, 20)
        Me.NOMBRETextBox.TabIndex = 8
        '
        'E_MAILTextBox
        '
        Me.E_MAILTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "E-MAIL", True))
        Me.E_MAILTextBox.Location = New System.Drawing.Point(283, 281)
        Me.E_MAILTextBox.Name = "E_MAILTextBox"
        Me.E_MAILTextBox.Size = New System.Drawing.Size(200, 20)
        Me.E_MAILTextBox.TabIndex = 12
        '
        'CODPUESTOTextBox
        '
        Me.CODPUESTOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "CODPUESTO", True))
        Me.CODPUESTOTextBox.Location = New System.Drawing.Point(283, 307)
        Me.CODPUESTOTextBox.Name = "CODPUESTOTextBox"
        Me.CODPUESTOTextBox.Size = New System.Drawing.Size(200, 20)
        Me.CODPUESTOTextBox.TabIndex = 14
        '
        'FECHA_INGRESODateTimePicker
        '
        Me.FECHA_INGRESODateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.DatosPersonalesBindingSource, "FECHA INGRESO", True))
        Me.FECHA_INGRESODateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.FECHA_INGRESODateTimePicker.Location = New System.Drawing.Point(283, 333)
        Me.FECHA_INGRESODateTimePicker.Name = "FECHA_INGRESODateTimePicker"
        Me.FECHA_INGRESODateTimePicker.Size = New System.Drawing.Size(200, 20)
        Me.FECHA_INGRESODateTimePicker.TabIndex = 16
        Me.FECHA_INGRESODateTimePicker.Value = New Date(2012, 11, 8, 0, 0, 0, 0)
        '
        'N__CONYUGESCheckBox
        '
        Me.N__CONYUGESCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.DatosPersonalesBindingSource, "N° CONYUGES", True))
        Me.N__CONYUGESCheckBox.Location = New System.Drawing.Point(283, 385)
        Me.N__CONYUGESCheckBox.Name = "N__CONYUGESCheckBox"
        Me.N__CONYUGESCheckBox.Size = New System.Drawing.Size(200, 24)
        Me.N__CONYUGESCheckBox.TabIndex = 20
        Me.N__CONYUGESCheckBox.Text = "Si"
        Me.N__CONYUGESCheckBox.UseVisualStyleBackColor = True
        '
        'N__HIJOSTextBox
        '
        Me.N__HIJOSTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "N° HIJOS", True))
        Me.N__HIJOSTextBox.Location = New System.Drawing.Point(283, 415)
        Me.N__HIJOSTextBox.Name = "N__HIJOSTextBox"
        Me.N__HIJOSTextBox.Size = New System.Drawing.Size(200, 20)
        Me.N__HIJOSTextBox.TabIndex = 22
        '
        'GRADO_ACADEMICOTextBox
        '
        Me.GRADO_ACADEMICOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "GRADO ACADEMICO", True))
        Me.GRADO_ACADEMICOTextBox.Location = New System.Drawing.Point(283, 441)
        Me.GRADO_ACADEMICOTextBox.Name = "GRADO_ACADEMICOTextBox"
        Me.GRADO_ACADEMICOTextBox.Size = New System.Drawing.Size(200, 20)
        Me.GRADO_ACADEMICOTextBox.TabIndex = 24
        '
        'BANCO_A_DEPOSITARTextBox
        '
        Me.BANCO_A_DEPOSITARTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "BANCO A DEPOSITAR", True))
        Me.BANCO_A_DEPOSITARTextBox.Location = New System.Drawing.Point(283, 467)
        Me.BANCO_A_DEPOSITARTextBox.Name = "BANCO_A_DEPOSITARTextBox"
        Me.BANCO_A_DEPOSITARTextBox.Size = New System.Drawing.Size(200, 20)
        Me.BANCO_A_DEPOSITARTextBox.TabIndex = 26
        '
        'NoCUENTATextBox
        '
        Me.NoCUENTATextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "NoCUENTA", True))
        Me.NoCUENTATextBox.Location = New System.Drawing.Point(283, 493)
        Me.NoCUENTATextBox.Name = "NoCUENTATextBox"
        Me.NoCUENTATextBox.Size = New System.Drawing.Size(200, 20)
        Me.NoCUENTATextBox.TabIndex = 28
        '
        'TelefonoTextBox
        '
        Me.TelefonoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "Telefono", True))
        Me.TelefonoTextBox.Location = New System.Drawing.Point(283, 519)
        Me.TelefonoTextBox.Name = "TelefonoTextBox"
        Me.TelefonoTextBox.Size = New System.Drawing.Size(200, 20)
        Me.TelefonoTextBox.TabIndex = 30
        '
        'CelularTextBox
        '
        Me.CelularTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "Celular", True))
        Me.CelularTextBox.Location = New System.Drawing.Point(283, 545)
        Me.CelularTextBox.Name = "CelularTextBox"
        Me.CelularTextBox.Size = New System.Drawing.Size(200, 20)
        Me.CelularTextBox.TabIndex = 32
        '
        'DireccionTextBox
        '
        Me.DireccionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "Direccion", True))
        Me.DireccionTextBox.Location = New System.Drawing.Point(283, 571)
        Me.DireccionTextBox.Name = "DireccionTextBox"
        Me.DireccionTextBox.Size = New System.Drawing.Size(200, 20)
        Me.DireccionTextBox.TabIndex = 34
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(550, 380)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 35
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'DatosPersonalesTableAdapter
        '
        Me.DatosPersonalesTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.AjustesTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.DatosPersonalesTableAdapter = Me.DatosPersonalesTableAdapter
        Me.TableAdapterManager.DeduccionCooperativaTableAdapter = Nothing
        Me.TableAdapterManager.PlanillaBisemanalTableAdapter = Nothing
        Me.TableAdapterManager.PlanillaMensualTableAdapter = Nothing
        Me.TableAdapterManager.PuestosTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = Proyecto3.Planilla2DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'proveedorError
        '
        Me.proveedorError.ContainerControl = Me
        '
        'GuardarPersona
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(685, 611)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(NoCEDULALabel)
        Me.Controls.Add(Me.NoCEDULATextBox)
        Me.Controls.Add(_1__APELLIDOLabel)
        Me.Controls.Add(Me._1__APELLIDOTextBox)
        Me.Controls.Add(_2__APELLIDOLabel)
        Me.Controls.Add(Me._2__APELLIDOTextBox)
        Me.Controls.Add(NOMBRELabel)
        Me.Controls.Add(Me.NOMBRETextBox)
        Me.Controls.Add(E_MAILLabel)
        Me.Controls.Add(Me.E_MAILTextBox)
        Me.Controls.Add(CODPUESTOLabel)
        Me.Controls.Add(Me.CODPUESTOTextBox)
        Me.Controls.Add(FECHA_INGRESOLabel)
        Me.Controls.Add(Me.FECHA_INGRESODateTimePicker)
        Me.Controls.Add(N__CONYUGESLabel)
        Me.Controls.Add(Me.N__CONYUGESCheckBox)
        Me.Controls.Add(N__HIJOSLabel)
        Me.Controls.Add(Me.N__HIJOSTextBox)
        Me.Controls.Add(GRADO_ACADEMICOLabel)
        Me.Controls.Add(Me.GRADO_ACADEMICOTextBox)
        Me.Controls.Add(BANCO_A_DEPOSITARLabel)
        Me.Controls.Add(Me.BANCO_A_DEPOSITARTextBox)
        Me.Controls.Add(NoCUENTALabel)
        Me.Controls.Add(Me.NoCUENTATextBox)
        Me.Controls.Add(TelefonoLabel)
        Me.Controls.Add(Me.TelefonoTextBox)
        Me.Controls.Add(CelularLabel)
        Me.Controls.Add(Me.CelularTextBox)
        Me.Controls.Add(DireccionLabel)
        Me.Controls.Add(Me.DireccionTextBox)
        Me.Name = "GuardarPersona"
        Me.Text = "Form1"
        CType(Me.DatosPersonalesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Planilla2DataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.proveedorError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Planilla2DataSet As Proyecto3.Planilla2DataSet
    Friend WithEvents DatosPersonalesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DatosPersonalesTableAdapter As Proyecto3.Planilla2DataSetTableAdapters.DatosPersonalesTableAdapter
    Friend WithEvents TableAdapterManager As Proyecto3.Planilla2DataSetTableAdapters.TableAdapterManager
    Friend WithEvents NoCEDULATextBox As System.Windows.Forms.TextBox
    Friend WithEvents _1__APELLIDOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents _2__APELLIDOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NOMBRETextBox As System.Windows.Forms.TextBox
    Friend WithEvents E_MAILTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CODPUESTOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents FECHA_INGRESODateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents N__CONYUGESCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents N__HIJOSTextBox As System.Windows.Forms.TextBox
    Friend WithEvents GRADO_ACADEMICOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents BANCO_A_DEPOSITARTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NoCUENTATextBox As System.Windows.Forms.TextBox
    Friend WithEvents TelefonoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CelularTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DireccionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents proveedorError As System.Windows.Forms.ErrorProvider

End Class

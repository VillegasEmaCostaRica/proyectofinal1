﻿Public Class frmConsultaImpuesto

    Public datoBusqueda As String

    Private Sub ImpuestosBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub frmConsultaImpuesto_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Planilla2DataSet.Impuestos' table. You can move, or remove it, as needed.
        Me.ImpuestosTableAdapter.Fill(Me.Planilla2DataSet.Impuestos)
    End Sub

    Sub baseDatosVacia()

        If ImpuestosDataGridView.Item(0, 0).Value Is Nothing Then
            ' Show the user a message
            MessageBox.Show("Base datos vacia")
            txtDato.Clear()
        End If

    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        buscarRegistro()
        baseDatosVacia()

    End Sub


    Sub buscarRegistro()
        Try
            If ImpuestosTableAdapter.FillBy(Me.Planilla2DataSet.Impuestos, Integer.Parse(txtDato.Text)) Then
                MsgBox("Registro Encontrado")
            Else
                MsgBox("Registro no encontrado")
                Me.ImpuestosTableAdapter.Fill(Me.Planilla2DataSet.Impuestos)
                txtDato.Clear()

            End If
        Catch ex As Exception
            MsgBox("Verifique los datos de entrada" + ex.Message)
        End Try
        
    End Sub


    Private Sub validarCajasTexo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDato.KeyPress

        Dim objeto As TextBox
        objeto = CType(sender, TextBox)

        If (Not Char.IsNumber(e.KeyChar) And e.KeyChar <> Microsoft.VisualBasic.ChrW(8)) Then
            e.Handled = True
            ToolStripStatusLabel1.Text = "Sólo se ingresa número en el campo"
            ErrorProvider1.SetError(objeto, "Sólo se ingresa número en el campo")
            Beep()
        Else
            ErrorProvider1.Clear()
            ToolStripStatusLabel1.Text = "Status"
        End If
    End Sub

    Private Sub BuscarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BuscarToolStripMenuItem.Click
        buscarRegistro()
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        buscarRegistro()
    End Sub

    Private Sub ImpuestosDataGridView_MouseDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles ImpuestosDataGridView.MouseDoubleClick
        frmImpuesto.Show()
        Me.Close()
    End Sub

    Private Sub ImpuestosDataGridView_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles ImpuestosDataGridView.CellClick
        ToolStripStatusLabel1.Text = "Doble click para seleccionar un registro  de la lista"
        Try
            txtDato.Text = Me.ImpuestosDataGridView.Rows(e.RowIndex).Cells(0).Value()
        Catch ex As Exception

        End Try

    End Sub

    Private Sub RegresarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegresarToolStripMenuItem.Click
        Me.Close()
        frmImpuesto.Show()

    End Sub

    Private Sub frmConsultaImpuesto_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        If txtDato.Text = "" Then
        Else
            My.Forms.frmImpuesto.CodImpuestoTextBox.Text = txtDato.Text
            ImpuestosTableAdapter.FillBy(My.Forms.frmImpuesto.Planilla2DataSet.Impuestos, Integer.Parse(Me.txtDato.Text))
        End If
        frmImpuesto.Show()
    End Sub

    Private Sub ToolStrip1_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles ToolStrip1.ItemClicked

    End Sub

    Private Sub ImpuestosDataGridView_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImpuestosDataGridView.MouseHover
        ToolStripStatusLabel1.Text = "Doble click para seleccionar el resgistro deseado"
    End Sub

    Private Sub ImpuestosDataGridView_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImpuestosDataGridView.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        eliminar()
    End Sub


    Sub eliminar()
        Try
            datoBusqueda = ""

            If txtDato.Text = "" Then
                MsgBox("Campo vacio: Verique")
            Else
                If MsgBox("Seguro desea eliminar el registro seleccionado", vbYesNo) = vbYes Then
                    ImpuestosTableAdapter.DeleteQuery(Integer.Parse(txtDato.Text))
                    MsgBox("Registro eliminado")
                    Me.ImpuestosTableAdapter.Fill(Me.Planilla2DataSet.Impuestos)
                    txtDato.Clear()
                End If
            End If
        Catch ex As Exception
            MsgBox("Verifique los datos de entrada")
        Finally
            txtDato.Clear()
        End Try

    End Sub


    Sub verificarTablaVacia()

        If ImpuestosDataGridView.Item(0, 0).Value Is Nothing Then
            ' Show the user a message
            MessageBox.Show("Registro no encontrado")
            BuscarToolStripMenuItem.Enabled = False
        End If


    End Sub
    Private Sub ToolStripButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton3.Click
        eliminar()
    End Sub

    Private Sub EliminarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        eliminar()

    End Sub

    Private Sub EliminarToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EliminarToolStripMenuItem1.Click
        Me.ImpuestosTableAdapter.Fill(Me.Planilla2DataSet.Impuestos)

    End Sub

    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click
        Me.ImpuestosTableAdapter.Fill(Me.Planilla2DataSet.Impuestos)

    End Sub

    Private Sub AccionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AccionesToolStripMenuItem.Click

    End Sub

    Private Sub EliminarToolStripMenuItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EliminarToolStripMenuItem.Click
        eliminar()
    End Sub

    Private Sub ToolStripButton4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton4.Click
        Me.Close()
        frmImpuesto.Show()
    End Sub

    Private Sub ImpuestosDataGridView_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles ImpuestosDataGridView.CellContentClick

    End Sub
End Class
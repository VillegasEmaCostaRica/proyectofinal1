﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAjustes
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CODIGO_DE_PUESTOLabel As System.Windows.Forms.Label
        Dim DESCRIPCIONLabel As System.Windows.Forms.Label
        Dim DEPARTAMENTOLabel As System.Windows.Forms.Label
        Dim SALARIO_UNICOLabel As System.Windows.Forms.Label
        Dim EMPLEADO_CONFIANZALabel As System.Windows.Forms.Label
        Dim GRADO_ACADEMICOLabel1 As System.Windows.Forms.Label
        Dim CodCedulaAjusteLabel As System.Windows.Forms.Label
        Dim CodPUESTO_NUEVOLabel As System.Windows.Forms.Label
        Dim DESCRIPCIONPUESTONUEVOLabel As System.Windows.Forms.Label
        Dim SALARIO_NUEVOLabel As System.Windows.Forms.Label
        Dim GRADO_ACADREQUERIDOLabel As System.Windows.Forms.Label
        Dim TOTALAJUSTARLabel As System.Windows.Forms.Label
        Dim TDIASAJUSTARLabel As System.Windows.Forms.Label
        Dim DiarioLabel As System.Windows.Forms.Label
        Dim DiferenciaLabel As System.Windows.Forms.Label
        Dim _CUMPLE_REQUISITO_Label As System.Windows.Forms.Label
        Dim NOMBRE_COMPLETOLabel1 As System.Windows.Forms.Label
        Dim E_MAILLabel1 As System.Windows.Forms.Label
        Dim CODPUESTOLabel1 As System.Windows.Forms.Label
        Dim TelefonoLabel1 As System.Windows.Forms.Label
        Dim CelularLabel1 As System.Windows.Forms.Label
        Dim DireccionLabel1 As System.Windows.Forms.Label
        Dim GRADO_ACADEMICOLabel2 As System.Windows.Forms.Label
        Dim NoCEDULALabel As System.Windows.Forms.Label
        Me.btnBuscarEmpleado = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnConsultar = New System.Windows.Forms.Button()
        Me.NoCEDULATextBox = New System.Windows.Forms.TextBox()
        Me.DatosPersonalesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Planilla2DataSet = New PlanillasProyecto3.Planilla2DataSet()
        Me.GRADO_ACADEMICOTextBox = New System.Windows.Forms.TextBox()
        Me.TelefonoTextBox = New System.Windows.Forms.TextBox()
        Me.CelularTextBox = New System.Windows.Forms.TextBox()
        Me.DireccionTextBox = New System.Windows.Forms.TextBox()
        Me.CODPUESTOTextBox = New System.Windows.Forms.TextBox()
        Me.NOMBRE_COMPLETOTextBox = New System.Windows.Forms.TextBox()
        Me.E_MAILTextBox = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.DiferenciaTextBox = New System.Windows.Forms.TextBox()
        Me.AjustesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TOTALAJUSTARTextBox = New System.Windows.Forms.TextBox()
        Me.DiarioTextBox = New System.Windows.Forms.TextBox()
        Me.CodCedulaAjusteTextBox = New System.Windows.Forms.TextBox()
        Me.CodPUESTO_NUEVOTextBox = New System.Windows.Forms.TextBox()
        Me.PuestosBindingSource2 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Planilla2DataSet1 = New PlanillasProyecto3.Planilla2DataSet1()
        Me.DESCRIPCIONPUESTONUEVOTextBox = New System.Windows.Forms.TextBox()
        Me.SALARIO_NUEVOTextBox = New System.Windows.Forms.TextBox()
        Me.GRADO_ACADREQUERIDOTextBox = New System.Windows.Forms.TextBox()
        Me.PuestosBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.CODIGO_DE_PUESTOTextBox = New System.Windows.Forms.TextBox()
        Me.PuestosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button1 = New System.Windows.Forms.Button()
        Me.DESCRIPCIONTextBox = New System.Windows.Forms.TextBox()
        Me.DEPARTAMENTOTextBox = New System.Windows.Forms.TextBox()
        Me.SALARIO_UNICOTextBox = New System.Windows.Forms.TextBox()
        Me.EMPLEADO_CONFIANZACheckBox = New System.Windows.Forms.CheckBox()
        Me.GRADO_ACADEMICOTextBox1 = New System.Windows.Forms.TextBox()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.btnActualizar = New System.Windows.Forms.Button()
        Me.btnBorrar = New System.Windows.Forms.Button()
        Me.PuestosTableAdapter1 = New PlanillasProyecto3.Planilla2DataSet1TableAdapters.PuestosTableAdapter()
        Me.TableAdapterManager1 = New PlanillasProyecto3.Planilla2DataSet1TableAdapters.TableAdapterManager()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.RegresarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsularToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DatosPersonalesTableAdapter = New PlanillasProyecto3.Planilla2DataSetTableAdapters.DatosPersonalesTableAdapter()
        Me.TableAdapterManager = New PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager()
        Me.AjustesTableAdapter = New PlanillasProyecto3.Planilla2DataSetTableAdapters.AjustesTableAdapter()
        Me.PuestosTableAdapter = New PlanillasProyecto3.Planilla2DataSetTableAdapters.PuestosTableAdapter()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        CODIGO_DE_PUESTOLabel = New System.Windows.Forms.Label()
        DESCRIPCIONLabel = New System.Windows.Forms.Label()
        DEPARTAMENTOLabel = New System.Windows.Forms.Label()
        SALARIO_UNICOLabel = New System.Windows.Forms.Label()
        EMPLEADO_CONFIANZALabel = New System.Windows.Forms.Label()
        GRADO_ACADEMICOLabel1 = New System.Windows.Forms.Label()
        CodCedulaAjusteLabel = New System.Windows.Forms.Label()
        CodPUESTO_NUEVOLabel = New System.Windows.Forms.Label()
        DESCRIPCIONPUESTONUEVOLabel = New System.Windows.Forms.Label()
        SALARIO_NUEVOLabel = New System.Windows.Forms.Label()
        GRADO_ACADREQUERIDOLabel = New System.Windows.Forms.Label()
        TOTALAJUSTARLabel = New System.Windows.Forms.Label()
        TDIASAJUSTARLabel = New System.Windows.Forms.Label()
        DiarioLabel = New System.Windows.Forms.Label()
        DiferenciaLabel = New System.Windows.Forms.Label()
        _CUMPLE_REQUISITO_Label = New System.Windows.Forms.Label()
        NOMBRE_COMPLETOLabel1 = New System.Windows.Forms.Label()
        E_MAILLabel1 = New System.Windows.Forms.Label()
        CODPUESTOLabel1 = New System.Windows.Forms.Label()
        TelefonoLabel1 = New System.Windows.Forms.Label()
        CelularLabel1 = New System.Windows.Forms.Label()
        DireccionLabel1 = New System.Windows.Forms.Label()
        GRADO_ACADEMICOLabel2 = New System.Windows.Forms.Label()
        NoCEDULALabel = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DatosPersonalesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Planilla2DataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.AjustesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PuestosBindingSource2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Planilla2DataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PuestosBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.PuestosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'CODIGO_DE_PUESTOLabel
        '
        CODIGO_DE_PUESTOLabel.AutoSize = True
        CODIGO_DE_PUESTOLabel.Location = New System.Drawing.Point(16, 27)
        CODIGO_DE_PUESTOLabel.Name = "CODIGO_DE_PUESTOLabel"
        CODIGO_DE_PUESTOLabel.Size = New System.Drawing.Size(117, 13)
        CODIGO_DE_PUESTOLabel.TabIndex = 77
        CODIGO_DE_PUESTOLabel.Text = "CODIGO DE PUESTO:"
        '
        'DESCRIPCIONLabel
        '
        DESCRIPCIONLabel.AutoSize = True
        DESCRIPCIONLabel.Location = New System.Drawing.Point(16, 53)
        DESCRIPCIONLabel.Name = "DESCRIPCIONLabel"
        DESCRIPCIONLabel.Size = New System.Drawing.Size(83, 13)
        DESCRIPCIONLabel.TabIndex = 79
        DESCRIPCIONLabel.Text = "DESCRIPCION:"
        '
        'DEPARTAMENTOLabel
        '
        DEPARTAMENTOLabel.AutoSize = True
        DEPARTAMENTOLabel.Location = New System.Drawing.Point(16, 79)
        DEPARTAMENTOLabel.Name = "DEPARTAMENTOLabel"
        DEPARTAMENTOLabel.Size = New System.Drawing.Size(100, 13)
        DEPARTAMENTOLabel.TabIndex = 81
        DEPARTAMENTOLabel.Text = "DEPARTAMENTO:"
        '
        'SALARIO_UNICOLabel
        '
        SALARIO_UNICOLabel.AutoSize = True
        SALARIO_UNICOLabel.Location = New System.Drawing.Point(16, 105)
        SALARIO_UNICOLabel.Name = "SALARIO_UNICOLabel"
        SALARIO_UNICOLabel.Size = New System.Drawing.Size(93, 13)
        SALARIO_UNICOLabel.TabIndex = 83
        SALARIO_UNICOLabel.Text = "SALARIO UNICO:"
        '
        'EMPLEADO_CONFIANZALabel
        '
        EMPLEADO_CONFIANZALabel.AutoSize = True
        EMPLEADO_CONFIANZALabel.Location = New System.Drawing.Point(16, 133)
        EMPLEADO_CONFIANZALabel.Name = "EMPLEADO_CONFIANZALabel"
        EMPLEADO_CONFIANZALabel.Size = New System.Drawing.Size(133, 13)
        EMPLEADO_CONFIANZALabel.TabIndex = 85
        EMPLEADO_CONFIANZALabel.Text = "EMPLEADO CONFIANZA:"
        '
        'GRADO_ACADEMICOLabel1
        '
        GRADO_ACADEMICOLabel1.AutoSize = True
        GRADO_ACADEMICOLabel1.Location = New System.Drawing.Point(16, 161)
        GRADO_ACADEMICOLabel1.Name = "GRADO_ACADEMICOLabel1"
        GRADO_ACADEMICOLabel1.Size = New System.Drawing.Size(115, 13)
        GRADO_ACADEMICOLabel1.TabIndex = 87
        GRADO_ACADEMICOLabel1.Text = "GRADO ACADEMICO:"
        '
        'CodCedulaAjusteLabel
        '
        CodCedulaAjusteLabel.AutoSize = True
        CodCedulaAjusteLabel.Location = New System.Drawing.Point(6, 28)
        CodCedulaAjusteLabel.Name = "CodCedulaAjusteLabel"
        CodCedulaAjusteLabel.Size = New System.Drawing.Size(97, 13)
        CodCedulaAjusteLabel.TabIndex = 98
        CodCedulaAjusteLabel.Text = "Cod Cedula Ajuste:"
        '
        'CodPUESTO_NUEVOLabel
        '
        CodPUESTO_NUEVOLabel.AutoSize = True
        CodPUESTO_NUEVOLabel.Location = New System.Drawing.Point(6, 54)
        CodPUESTO_NUEVOLabel.Name = "CodPUESTO_NUEVOLabel"
        CodPUESTO_NUEVOLabel.Size = New System.Drawing.Size(117, 13)
        CodPUESTO_NUEVOLabel.TabIndex = 100
        CodPUESTO_NUEVOLabel.Text = "Cod PUESTO NUEVO:"
        '
        'DESCRIPCIONPUESTONUEVOLabel
        '
        DESCRIPCIONPUESTONUEVOLabel.AutoSize = True
        DESCRIPCIONPUESTONUEVOLabel.Location = New System.Drawing.Point(6, 80)
        DESCRIPCIONPUESTONUEVOLabel.Name = "DESCRIPCIONPUESTONUEVOLabel"
        DESCRIPCIONPUESTONUEVOLabel.Size = New System.Drawing.Size(165, 13)
        DESCRIPCIONPUESTONUEVOLabel.TabIndex = 102
        DESCRIPCIONPUESTONUEVOLabel.Text = "DESCRIPCIONPUESTONUEVO:"
        '
        'SALARIO_NUEVOLabel
        '
        SALARIO_NUEVOLabel.AutoSize = True
        SALARIO_NUEVOLabel.Location = New System.Drawing.Point(6, 106)
        SALARIO_NUEVOLabel.Name = "SALARIO_NUEVOLabel"
        SALARIO_NUEVOLabel.Size = New System.Drawing.Size(97, 13)
        SALARIO_NUEVOLabel.TabIndex = 104
        SALARIO_NUEVOLabel.Text = "SALARIO NUEVO:"
        '
        'GRADO_ACADREQUERIDOLabel
        '
        GRADO_ACADREQUERIDOLabel.AutoSize = True
        GRADO_ACADREQUERIDOLabel.Location = New System.Drawing.Point(6, 132)
        GRADO_ACADREQUERIDOLabel.Name = "GRADO_ACADREQUERIDOLabel"
        GRADO_ACADREQUERIDOLabel.Size = New System.Drawing.Size(146, 13)
        GRADO_ACADREQUERIDOLabel.TabIndex = 106
        GRADO_ACADREQUERIDOLabel.Text = "GRADO ACADREQUERIDO:"
        '
        'TOTALAJUSTARLabel
        '
        TOTALAJUSTARLabel.AutoSize = True
        TOTALAJUSTARLabel.Location = New System.Drawing.Point(415, 119)
        TOTALAJUSTARLabel.Name = "TOTALAJUSTARLabel"
        TOTALAJUSTARLabel.Size = New System.Drawing.Size(94, 13)
        TOTALAJUSTARLabel.TabIndex = 124
        TOTALAJUSTARLabel.Text = "TOTALAJUSTAR:"
        '
        'TDIASAJUSTARLabel
        '
        TDIASAJUSTARLabel.AutoSize = True
        TDIASAJUSTARLabel.Location = New System.Drawing.Point(415, 54)
        TDIASAJUSTARLabel.Name = "TDIASAJUSTARLabel"
        TDIASAJUSTARLabel.Size = New System.Drawing.Size(35, 13)
        TDIASAJUSTARLabel.TabIndex = 122
        TDIASAJUSTARLabel.Text = "DIAS:"
        '
        'DiarioLabel
        '
        DiarioLabel.AutoSize = True
        DiarioLabel.Location = New System.Drawing.Point(415, 97)
        DiarioLabel.Name = "DiarioLabel"
        DiarioLabel.Size = New System.Drawing.Size(37, 13)
        DiarioLabel.TabIndex = 120
        DiarioLabel.Text = "Diario:"
        '
        'DiferenciaLabel
        '
        DiferenciaLabel.AutoSize = True
        DiferenciaLabel.Location = New System.Drawing.Point(415, 76)
        DiferenciaLabel.Name = "DiferenciaLabel"
        DiferenciaLabel.Size = New System.Drawing.Size(58, 13)
        DiferenciaLabel.TabIndex = 118
        DiferenciaLabel.Text = "Diferencia:"
        '
        '_CUMPLE_REQUISITO_Label
        '
        _CUMPLE_REQUISITO_Label.AutoSize = True
        _CUMPLE_REQUISITO_Label.Location = New System.Drawing.Point(412, 28)
        _CUMPLE_REQUISITO_Label.Name = "_CUMPLE_REQUISITO_Label"
        _CUMPLE_REQUISITO_Label.Size = New System.Drawing.Size(72, 13)
        _CUMPLE_REQUISITO_Label.TabIndex = 126
        _CUMPLE_REQUISITO_Label.Text = " REQUISITO:"
        '
        'NOMBRE_COMPLETOLabel1
        '
        NOMBRE_COMPLETOLabel1.AutoSize = True
        NOMBRE_COMPLETOLabel1.Location = New System.Drawing.Point(14, 61)
        NOMBRE_COMPLETOLabel1.Name = "NOMBRE_COMPLETOLabel1"
        NOMBRE_COMPLETOLabel1.Size = New System.Drawing.Size(119, 13)
        NOMBRE_COMPLETOLabel1.TabIndex = 100
        NOMBRE_COMPLETOLabel1.Text = "NOMBRE COMPLETO:"
        '
        'E_MAILLabel1
        '
        E_MAILLabel1.AutoSize = True
        E_MAILLabel1.Location = New System.Drawing.Point(14, 87)
        E_MAILLabel1.Name = "E_MAILLabel1"
        E_MAILLabel1.Size = New System.Drawing.Size(45, 13)
        E_MAILLabel1.TabIndex = 102
        E_MAILLabel1.Text = "E-MAIL:"
        '
        'CODPUESTOLabel1
        '
        CODPUESTOLabel1.AutoSize = True
        CODPUESTOLabel1.Location = New System.Drawing.Point(14, 113)
        CODPUESTOLabel1.Name = "CODPUESTOLabel1"
        CODPUESTOLabel1.Size = New System.Drawing.Size(77, 13)
        CODPUESTOLabel1.TabIndex = 104
        CODPUESTOLabel1.Text = "CODPUESTO:"
        '
        'TelefonoLabel1
        '
        TelefonoLabel1.AutoSize = True
        TelefonoLabel1.Location = New System.Drawing.Point(14, 165)
        TelefonoLabel1.Name = "TelefonoLabel1"
        TelefonoLabel1.Size = New System.Drawing.Size(52, 13)
        TelefonoLabel1.TabIndex = 122
        TelefonoLabel1.Text = "Telefono:"
        '
        'CelularLabel1
        '
        CelularLabel1.AutoSize = True
        CelularLabel1.Location = New System.Drawing.Point(14, 191)
        CelularLabel1.Name = "CelularLabel1"
        CelularLabel1.Size = New System.Drawing.Size(42, 13)
        CelularLabel1.TabIndex = 124
        CelularLabel1.Text = "Celular:"
        '
        'DireccionLabel1
        '
        DireccionLabel1.AutoSize = True
        DireccionLabel1.Location = New System.Drawing.Point(14, 217)
        DireccionLabel1.Name = "DireccionLabel1"
        DireccionLabel1.Size = New System.Drawing.Size(55, 13)
        DireccionLabel1.TabIndex = 126
        DireccionLabel1.Text = "Direccion:"
        '
        'GRADO_ACADEMICOLabel2
        '
        GRADO_ACADEMICOLabel2.AutoSize = True
        GRADO_ACADEMICOLabel2.Location = New System.Drawing.Point(14, 139)
        GRADO_ACADEMICOLabel2.Name = "GRADO_ACADEMICOLabel2"
        GRADO_ACADEMICOLabel2.Size = New System.Drawing.Size(115, 13)
        GRADO_ACADEMICOLabel2.TabIndex = 128
        GRADO_ACADEMICOLabel2.Text = "GRADO ACADEMICO:"
        '
        'NoCEDULALabel
        '
        NoCEDULALabel.AutoSize = True
        NoCEDULALabel.Location = New System.Drawing.Point(14, 31)
        NoCEDULALabel.Name = "NoCEDULALabel"
        NoCEDULALabel.Size = New System.Drawing.Size(70, 13)
        NoCEDULALabel.TabIndex = 129
        NoCEDULALabel.Text = "No CEDULA:"
        '
        'btnBuscarEmpleado
        '
        Me.btnBuscarEmpleado.Location = New System.Drawing.Point(332, 33)
        Me.btnBuscarEmpleado.Name = "btnBuscarEmpleado"
        Me.btnBuscarEmpleado.Size = New System.Drawing.Size(80, 45)
        Me.btnBuscarEmpleado.TabIndex = 1
        Me.btnBuscarEmpleado.Text = "Buscar &Empleado"
        Me.ToolTip1.SetToolTip(Me.btnBuscarEmpleado, "Busca al empleado por el Numero de Cedula")
        Me.btnBuscarEmpleado.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnConsultar)
        Me.GroupBox1.Controls.Add(NoCEDULALabel)
        Me.GroupBox1.Controls.Add(Me.NoCEDULATextBox)
        Me.GroupBox1.Controls.Add(GRADO_ACADEMICOLabel2)
        Me.GroupBox1.Controls.Add(Me.GRADO_ACADEMICOTextBox)
        Me.GroupBox1.Controls.Add(TelefonoLabel1)
        Me.GroupBox1.Controls.Add(Me.TelefonoTextBox)
        Me.GroupBox1.Controls.Add(CelularLabel1)
        Me.GroupBox1.Controls.Add(Me.CelularTextBox)
        Me.GroupBox1.Controls.Add(DireccionLabel1)
        Me.GroupBox1.Controls.Add(Me.btnBuscarEmpleado)
        Me.GroupBox1.Controls.Add(Me.DireccionTextBox)
        Me.GroupBox1.Controls.Add(CODPUESTOLabel1)
        Me.GroupBox1.Controls.Add(Me.CODPUESTOTextBox)
        Me.GroupBox1.Controls.Add(NOMBRE_COMPLETOLabel1)
        Me.GroupBox1.Controls.Add(Me.NOMBRE_COMPLETOTextBox)
        Me.GroupBox1.Controls.Add(E_MAILLabel1)
        Me.GroupBox1.Controls.Add(Me.E_MAILTextBox)
        Me.GroupBox1.Location = New System.Drawing.Point(15, 32)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(449, 254)
        Me.GroupBox1.TabIndex = 77
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos Personales"
        '
        'btnConsultar
        '
        Me.btnConsultar.Location = New System.Drawing.Point(332, 145)
        Me.btnConsultar.Name = "btnConsultar"
        Me.btnConsultar.Size = New System.Drawing.Size(79, 49)
        Me.btnConsultar.TabIndex = 2
        Me.btnConsultar.Text = "&Consultar Lista"
        Me.ToolTip1.SetToolTip(Me.btnConsultar, "Ver los Empleados Guardados")
        Me.btnConsultar.UseVisualStyleBackColor = True
        '
        'NoCEDULATextBox
        '
        Me.NoCEDULATextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "NoCEDULA", True))
        Me.NoCEDULATextBox.Location = New System.Drawing.Point(142, 27)
        Me.NoCEDULATextBox.Name = "NoCEDULATextBox"
        Me.NoCEDULATextBox.Size = New System.Drawing.Size(166, 20)
        Me.NoCEDULATextBox.TabIndex = 0
        '
        'DatosPersonalesBindingSource
        '
        Me.DatosPersonalesBindingSource.DataMember = "DatosPersonales"
        Me.DatosPersonalesBindingSource.DataSource = Me.Planilla2DataSet
        '
        'Planilla2DataSet
        '
        Me.Planilla2DataSet.DataSetName = "Planilla2DataSet"
        Me.Planilla2DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GRADO_ACADEMICOTextBox
        '
        Me.GRADO_ACADEMICOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "GRADO ACADEMICO", True))
        Me.GRADO_ACADEMICOTextBox.Enabled = False
        Me.GRADO_ACADEMICOTextBox.Location = New System.Drawing.Point(143, 136)
        Me.GRADO_ACADEMICOTextBox.Name = "GRADO_ACADEMICOTextBox"
        Me.GRADO_ACADEMICOTextBox.Size = New System.Drawing.Size(166, 20)
        Me.GRADO_ACADEMICOTextBox.TabIndex = 129
        '
        'TelefonoTextBox
        '
        Me.TelefonoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "Telefono", True))
        Me.TelefonoTextBox.Enabled = False
        Me.TelefonoTextBox.Location = New System.Drawing.Point(142, 162)
        Me.TelefonoTextBox.Name = "TelefonoTextBox"
        Me.TelefonoTextBox.Size = New System.Drawing.Size(166, 20)
        Me.TelefonoTextBox.TabIndex = 123
        '
        'CelularTextBox
        '
        Me.CelularTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "Celular", True))
        Me.CelularTextBox.Enabled = False
        Me.CelularTextBox.Location = New System.Drawing.Point(142, 188)
        Me.CelularTextBox.Name = "CelularTextBox"
        Me.CelularTextBox.Size = New System.Drawing.Size(166, 20)
        Me.CelularTextBox.TabIndex = 125
        '
        'DireccionTextBox
        '
        Me.DireccionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "Direccion", True))
        Me.DireccionTextBox.Enabled = False
        Me.DireccionTextBox.Location = New System.Drawing.Point(142, 214)
        Me.DireccionTextBox.Name = "DireccionTextBox"
        Me.DireccionTextBox.Size = New System.Drawing.Size(166, 20)
        Me.DireccionTextBox.TabIndex = 127
        '
        'CODPUESTOTextBox
        '
        Me.CODPUESTOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "CODPUESTO", True))
        Me.CODPUESTOTextBox.Enabled = False
        Me.CODPUESTOTextBox.Location = New System.Drawing.Point(143, 110)
        Me.CODPUESTOTextBox.Name = "CODPUESTOTextBox"
        Me.CODPUESTOTextBox.Size = New System.Drawing.Size(166, 20)
        Me.CODPUESTOTextBox.TabIndex = 105
        '
        'NOMBRE_COMPLETOTextBox
        '
        Me.NOMBRE_COMPLETOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "NOMBRE COMPLETO", True))
        Me.NOMBRE_COMPLETOTextBox.Enabled = False
        Me.NOMBRE_COMPLETOTextBox.Location = New System.Drawing.Point(142, 58)
        Me.NOMBRE_COMPLETOTextBox.Name = "NOMBRE_COMPLETOTextBox"
        Me.NOMBRE_COMPLETOTextBox.Size = New System.Drawing.Size(166, 20)
        Me.NOMBRE_COMPLETOTextBox.TabIndex = 101
        '
        'E_MAILTextBox
        '
        Me.E_MAILTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "E-MAIL", True))
        Me.E_MAILTextBox.Enabled = False
        Me.E_MAILTextBox.Location = New System.Drawing.Point(143, 84)
        Me.E_MAILTextBox.Name = "E_MAILTextBox"
        Me.E_MAILTextBox.Size = New System.Drawing.Size(166, 20)
        Me.E_MAILTextBox.TabIndex = 103
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.ComboBox2)
        Me.GroupBox2.Controls.Add(Me.ComboBox1)
        Me.GroupBox2.Controls.Add(_CUMPLE_REQUISITO_Label)
        Me.GroupBox2.Controls.Add(DiferenciaLabel)
        Me.GroupBox2.Controls.Add(Me.DiferenciaTextBox)
        Me.GroupBox2.Controls.Add(DiarioLabel)
        Me.GroupBox2.Controls.Add(TOTALAJUSTARLabel)
        Me.GroupBox2.Controls.Add(Me.TOTALAJUSTARTextBox)
        Me.GroupBox2.Controls.Add(Me.DiarioTextBox)
        Me.GroupBox2.Controls.Add(CodCedulaAjusteLabel)
        Me.GroupBox2.Controls.Add(TDIASAJUSTARLabel)
        Me.GroupBox2.Controls.Add(Me.CodCedulaAjusteTextBox)
        Me.GroupBox2.Controls.Add(CodPUESTO_NUEVOLabel)
        Me.GroupBox2.Controls.Add(Me.CodPUESTO_NUEVOTextBox)
        Me.GroupBox2.Controls.Add(DESCRIPCIONPUESTONUEVOLabel)
        Me.GroupBox2.Controls.Add(Me.DESCRIPCIONPUESTONUEVOTextBox)
        Me.GroupBox2.Controls.Add(SALARIO_NUEVOLabel)
        Me.GroupBox2.Controls.Add(Me.SALARIO_NUEVOTextBox)
        Me.GroupBox2.Controls.Add(GRADO_ACADREQUERIDOLabel)
        Me.GroupBox2.Controls.Add(Me.GRADO_ACADREQUERIDOTextBox)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 319)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(648, 166)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Datos Ajustes"
        '
        'ComboBox2
        '
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Items.AddRange(New Object() {"7", "8", "9", "10", "11", "12", "13", "14", "15"})
        Me.ComboBox2.Location = New System.Drawing.Point(529, 50)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(104, 21)
        Me.ComboBox2.TabIndex = 1
        Me.ToolTip1.SetToolTip(Me.ComboBox2, "Dias que se le Ajustar al Trabajador ")
        '
        'ComboBox1
        '
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Items.AddRange(New Object() {"Si", "No"})
        Me.ComboBox1.Location = New System.Drawing.Point(529, 25)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(104, 21)
        Me.ComboBox1.TabIndex = 0
        Me.ComboBox1.Text = "Si"
        Me.ToolTip1.SetToolTip(Me.ComboBox1, "Cumple con todo los requisitos Para tener el Puesto")
        '
        'DiferenciaTextBox
        '
        Me.DiferenciaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.AjustesBindingSource, "Diferencia", True))
        Me.DiferenciaTextBox.Location = New System.Drawing.Point(529, 73)
        Me.DiferenciaTextBox.Name = "DiferenciaTextBox"
        Me.DiferenciaTextBox.ReadOnly = True
        Me.DiferenciaTextBox.Size = New System.Drawing.Size(104, 20)
        Me.DiferenciaTextBox.TabIndex = 6
        Me.DiferenciaTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AjustesBindingSource
        '
        Me.AjustesBindingSource.DataMember = "Ajustes"
        Me.AjustesBindingSource.DataSource = Me.Planilla2DataSet
        '
        'TOTALAJUSTARTextBox
        '
        Me.TOTALAJUSTARTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.AjustesBindingSource, "TOTALAJUSTAR", True))
        Me.TOTALAJUSTARTextBox.Location = New System.Drawing.Point(529, 116)
        Me.TOTALAJUSTARTextBox.Name = "TOTALAJUSTARTextBox"
        Me.TOTALAJUSTARTextBox.ReadOnly = True
        Me.TOTALAJUSTARTextBox.Size = New System.Drawing.Size(104, 20)
        Me.TOTALAJUSTARTextBox.TabIndex = 8
        Me.TOTALAJUSTARTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'DiarioTextBox
        '
        Me.DiarioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.AjustesBindingSource, "Diario", True))
        Me.DiarioTextBox.Location = New System.Drawing.Point(529, 94)
        Me.DiarioTextBox.Name = "DiarioTextBox"
        Me.DiarioTextBox.ReadOnly = True
        Me.DiarioTextBox.Size = New System.Drawing.Size(104, 20)
        Me.DiarioTextBox.TabIndex = 7
        Me.DiarioTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'CodCedulaAjusteTextBox
        '
        Me.CodCedulaAjusteTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.AjustesBindingSource, "CodCedulaAjuste", True))
        Me.CodCedulaAjusteTextBox.Location = New System.Drawing.Point(174, 25)
        Me.CodCedulaAjusteTextBox.Name = "CodCedulaAjusteTextBox"
        Me.CodCedulaAjusteTextBox.ReadOnly = True
        Me.CodCedulaAjusteTextBox.Size = New System.Drawing.Size(187, 20)
        Me.CodCedulaAjusteTextBox.TabIndex = 0
        '
        'CodPUESTO_NUEVOTextBox
        '
        Me.CodPUESTO_NUEVOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PuestosBindingSource2, "CODIGO DE PUESTO", True))
        Me.CodPUESTO_NUEVOTextBox.Location = New System.Drawing.Point(174, 51)
        Me.CodPUESTO_NUEVOTextBox.Name = "CodPUESTO_NUEVOTextBox"
        Me.CodPUESTO_NUEVOTextBox.ReadOnly = True
        Me.CodPUESTO_NUEVOTextBox.Size = New System.Drawing.Size(187, 20)
        Me.CodPUESTO_NUEVOTextBox.TabIndex = 1
        '
        'PuestosBindingSource2
        '
        Me.PuestosBindingSource2.DataMember = "Puestos"
        Me.PuestosBindingSource2.DataSource = Me.Planilla2DataSet1
        '
        'Planilla2DataSet1
        '
        Me.Planilla2DataSet1.DataSetName = "Planilla2DataSet1"
        Me.Planilla2DataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DESCRIPCIONPUESTONUEVOTextBox
        '
        Me.DESCRIPCIONPUESTONUEVOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PuestosBindingSource2, "DESCRIPCION", True))
        Me.DESCRIPCIONPUESTONUEVOTextBox.Location = New System.Drawing.Point(174, 77)
        Me.DESCRIPCIONPUESTONUEVOTextBox.Name = "DESCRIPCIONPUESTONUEVOTextBox"
        Me.DESCRIPCIONPUESTONUEVOTextBox.ReadOnly = True
        Me.DESCRIPCIONPUESTONUEVOTextBox.Size = New System.Drawing.Size(187, 20)
        Me.DESCRIPCIONPUESTONUEVOTextBox.TabIndex = 2
        '
        'SALARIO_NUEVOTextBox
        '
        Me.SALARIO_NUEVOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PuestosBindingSource2, "SALARIO UNICO", True))
        Me.SALARIO_NUEVOTextBox.Location = New System.Drawing.Point(174, 103)
        Me.SALARIO_NUEVOTextBox.Name = "SALARIO_NUEVOTextBox"
        Me.SALARIO_NUEVOTextBox.ReadOnly = True
        Me.SALARIO_NUEVOTextBox.Size = New System.Drawing.Size(187, 20)
        Me.SALARIO_NUEVOTextBox.TabIndex = 3
        '
        'GRADO_ACADREQUERIDOTextBox
        '
        Me.GRADO_ACADREQUERIDOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PuestosBindingSource2, "GRADO ACADEMICO", True))
        Me.GRADO_ACADREQUERIDOTextBox.Location = New System.Drawing.Point(174, 129)
        Me.GRADO_ACADREQUERIDOTextBox.Name = "GRADO_ACADREQUERIDOTextBox"
        Me.GRADO_ACADREQUERIDOTextBox.ReadOnly = True
        Me.GRADO_ACADREQUERIDOTextBox.Size = New System.Drawing.Size(187, 20)
        Me.GRADO_ACADREQUERIDOTextBox.TabIndex = 4
        '
        'PuestosBindingSource1
        '
        Me.PuestosBindingSource1.DataMember = "Puestos"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(CODIGO_DE_PUESTOLabel)
        Me.GroupBox3.Controls.Add(Me.CODIGO_DE_PUESTOTextBox)
        Me.GroupBox3.Controls.Add(Me.Button1)
        Me.GroupBox3.Controls.Add(DESCRIPCIONLabel)
        Me.GroupBox3.Controls.Add(Me.DESCRIPCIONTextBox)
        Me.GroupBox3.Controls.Add(DEPARTAMENTOLabel)
        Me.GroupBox3.Controls.Add(Me.DEPARTAMENTOTextBox)
        Me.GroupBox3.Controls.Add(SALARIO_UNICOLabel)
        Me.GroupBox3.Controls.Add(Me.SALARIO_UNICOTextBox)
        Me.GroupBox3.Controls.Add(EMPLEADO_CONFIANZALabel)
        Me.GroupBox3.Controls.Add(Me.EMPLEADO_CONFIANZACheckBox)
        Me.GroupBox3.Controls.Add(GRADO_ACADEMICOLabel1)
        Me.GroupBox3.Controls.Add(Me.GRADO_ACADEMICOTextBox1)
        Me.GroupBox3.Location = New System.Drawing.Point(496, 34)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(313, 252)
        Me.GroupBox3.TabIndex = 0
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Datos Puestos"
        '
        'CODIGO_DE_PUESTOTextBox
        '
        Me.CODIGO_DE_PUESTOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PuestosBindingSource, "CODIGO DE PUESTO", True))
        Me.CODIGO_DE_PUESTOTextBox.Location = New System.Drawing.Point(155, 24)
        Me.CODIGO_DE_PUESTOTextBox.Name = "CODIGO_DE_PUESTOTextBox"
        Me.CODIGO_DE_PUESTOTextBox.ReadOnly = True
        Me.CODIGO_DE_PUESTOTextBox.Size = New System.Drawing.Size(104, 20)
        Me.CODIGO_DE_PUESTOTextBox.TabIndex = 0
        '
        'PuestosBindingSource
        '
        Me.PuestosBindingSource.DataMember = "Puestos"
        Me.PuestosBindingSource.DataSource = Me.Planilla2DataSet
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(155, 189)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(88, 43)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "&Seleccionar Nuevo Puesto"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'DESCRIPCIONTextBox
        '
        Me.DESCRIPCIONTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PuestosBindingSource, "DESCRIPCION", True))
        Me.DESCRIPCIONTextBox.Location = New System.Drawing.Point(155, 50)
        Me.DESCRIPCIONTextBox.Name = "DESCRIPCIONTextBox"
        Me.DESCRIPCIONTextBox.ReadOnly = True
        Me.DESCRIPCIONTextBox.Size = New System.Drawing.Size(104, 20)
        Me.DESCRIPCIONTextBox.TabIndex = 1
        '
        'DEPARTAMENTOTextBox
        '
        Me.DEPARTAMENTOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PuestosBindingSource, "DEPARTAMENTO", True))
        Me.DEPARTAMENTOTextBox.Location = New System.Drawing.Point(155, 76)
        Me.DEPARTAMENTOTextBox.Name = "DEPARTAMENTOTextBox"
        Me.DEPARTAMENTOTextBox.ReadOnly = True
        Me.DEPARTAMENTOTextBox.Size = New System.Drawing.Size(104, 20)
        Me.DEPARTAMENTOTextBox.TabIndex = 2
        '
        'SALARIO_UNICOTextBox
        '
        Me.SALARIO_UNICOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PuestosBindingSource, "SALARIO UNICO", True))
        Me.SALARIO_UNICOTextBox.Location = New System.Drawing.Point(155, 102)
        Me.SALARIO_UNICOTextBox.Name = "SALARIO_UNICOTextBox"
        Me.SALARIO_UNICOTextBox.ReadOnly = True
        Me.SALARIO_UNICOTextBox.Size = New System.Drawing.Size(104, 20)
        Me.SALARIO_UNICOTextBox.TabIndex = 3
        '
        'EMPLEADO_CONFIANZACheckBox
        '
        Me.EMPLEADO_CONFIANZACheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.PuestosBindingSource, "EMPLEADO CONFIANZA", True))
        Me.EMPLEADO_CONFIANZACheckBox.Enabled = False
        Me.EMPLEADO_CONFIANZACheckBox.Location = New System.Drawing.Point(155, 128)
        Me.EMPLEADO_CONFIANZACheckBox.Name = "EMPLEADO_CONFIANZACheckBox"
        Me.EMPLEADO_CONFIANZACheckBox.Size = New System.Drawing.Size(104, 24)
        Me.EMPLEADO_CONFIANZACheckBox.TabIndex = 4
        Me.EMPLEADO_CONFIANZACheckBox.Text = "Si"
        Me.EMPLEADO_CONFIANZACheckBox.UseVisualStyleBackColor = True
        '
        'GRADO_ACADEMICOTextBox1
        '
        Me.GRADO_ACADEMICOTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PuestosBindingSource, "GRADO ACADEMICO", True))
        Me.GRADO_ACADEMICOTextBox1.Location = New System.Drawing.Point(155, 158)
        Me.GRADO_ACADEMICOTextBox1.Name = "GRADO_ACADEMICOTextBox1"
        Me.GRADO_ACADEMICOTextBox1.ReadOnly = True
        Me.GRADO_ACADEMICOTextBox1.Size = New System.Drawing.Size(104, 20)
        Me.GRADO_ACADEMICOTextBox1.TabIndex = 5
        '
        'btnAgregar
        '
        Me.btnAgregar.Location = New System.Drawing.Point(243, 491)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(80, 29)
        Me.btnAgregar.TabIndex = 0
        Me.btnAgregar.Text = "&Agregar"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'btnActualizar
        '
        Me.btnActualizar.Location = New System.Drawing.Point(341, 491)
        Me.btnActualizar.Name = "btnActualizar"
        Me.btnActualizar.Size = New System.Drawing.Size(80, 29)
        Me.btnActualizar.TabIndex = 2
        Me.btnActualizar.Text = "&Actualizar"
        Me.btnActualizar.UseVisualStyleBackColor = True
        '
        'btnBorrar
        '
        Me.btnBorrar.Location = New System.Drawing.Point(427, 491)
        Me.btnBorrar.Name = "btnBorrar"
        Me.btnBorrar.Size = New System.Drawing.Size(80, 29)
        Me.btnBorrar.TabIndex = 3
        Me.btnBorrar.Text = "Borrar"
        Me.btnBorrar.UseVisualStyleBackColor = True
        '
        'PuestosTableAdapter1
        '
        Me.PuestosTableAdapter1.ClearBeforeFill = True
        '
        'TableAdapterManager1
        '
        Me.TableAdapterManager1.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager1.PuestosTableAdapter = Me.PuestosTableAdapter1
        Me.TableAdapterManager1.UpdateOrder = PlanillasProyecto3.Planilla2DataSet1TableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 663)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(838, 22)
        Me.StatusStrip1.TabIndex = 87
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RegresarToolStripMenuItem, Me.ConsularToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(838, 24)
        Me.MenuStrip1.TabIndex = 88
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'RegresarToolStripMenuItem
        '
        Me.RegresarToolStripMenuItem.Name = "RegresarToolStripMenuItem"
        Me.RegresarToolStripMenuItem.Size = New System.Drawing.Size(64, 20)
        Me.RegresarToolStripMenuItem.Text = "&Regresar"
        '
        'ConsularToolStripMenuItem
        '
        Me.ConsularToolStripMenuItem.Name = "ConsularToolStripMenuItem"
        Me.ConsularToolStripMenuItem.Size = New System.Drawing.Size(66, 20)
        Me.ConsularToolStripMenuItem.Text = "&Consular"
        '
        'DatosPersonalesTableAdapter
        '
        Me.DatosPersonalesTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.AjustesTableAdapter = Me.AjustesTableAdapter
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.DatosPersonalesTableAdapter = Me.DatosPersonalesTableAdapter
        Me.TableAdapterManager.DeduccionesTableAdapter = Nothing
        Me.TableAdapterManager.ImpuestosTableAdapter = Nothing
        Me.TableAdapterManager.PlanillaBisemanalTableAdapter = Nothing
        Me.TableAdapterManager.PlanillaMensualTableAdapter = Nothing
        Me.TableAdapterManager.PuestosTableAdapter = Me.PuestosTableAdapter
        Me.TableAdapterManager.UpdateOrder = PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'AjustesTableAdapter
        '
        Me.AjustesTableAdapter.ClearBeforeFill = True
        '
        'PuestosTableAdapter
        '
        Me.PuestosTableAdapter.ClearBeforeFill = True
        '
        'frmAjustes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(838, 685)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.btnBorrar)
        Me.Controls.Add(Me.btnActualizar)
        Me.Controls.Add(Me.btnAgregar)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmAjustes"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.DatosPersonalesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Planilla2DataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.AjustesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PuestosBindingSource2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Planilla2DataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PuestosBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.PuestosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Planilla2DataSet As PlanillasProyecto3.Planilla2DataSet
    Friend WithEvents DatosPersonalesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DatosPersonalesTableAdapter As PlanillasProyecto3.Planilla2DataSetTableAdapters.DatosPersonalesTableAdapter
    Friend WithEvents TableAdapterManager As PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager
    Friend WithEvents PuestosTableAdapter As PlanillasProyecto3.Planilla2DataSetTableAdapters.PuestosTableAdapter
    Friend WithEvents PuestosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents btnBuscarEmpleado As System.Windows.Forms.Button
    Friend WithEvents AjustesTableAdapter As PlanillasProyecto3.Planilla2DataSetTableAdapters.AjustesTableAdapter
    Friend WithEvents AjustesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents CODIGO_DE_PUESTOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DESCRIPCIONTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DEPARTAMENTOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SALARIO_UNICOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents EMPLEADO_CONFIANZACheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents GRADO_ACADEMICOTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents CodCedulaAjusteTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CodPUESTO_NUEVOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DESCRIPCIONPUESTONUEVOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SALARIO_NUEVOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents GRADO_ACADREQUERIDOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DiferenciaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DiarioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TOTALAJUSTARTextBox As System.Windows.Forms.TextBox
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents btnActualizar As System.Windows.Forms.Button
    Friend WithEvents btnBorrar As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents PuestosBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents Planilla2DataSet1 As PlanillasProyecto3.Planilla2DataSet1
    Friend WithEvents PuestosBindingSource2 As System.Windows.Forms.BindingSource
    Friend WithEvents PuestosTableAdapter1 As PlanillasProyecto3.Planilla2DataSet1TableAdapters.PuestosTableAdapter
    Friend WithEvents TableAdapterManager1 As PlanillasProyecto3.Planilla2DataSet1TableAdapters.TableAdapterManager
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents RegresarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsularToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NoCEDULATextBox As System.Windows.Forms.TextBox
    Friend WithEvents GRADO_ACADEMICOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TelefonoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CelularTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DireccionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CODPUESTOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NOMBRE_COMPLETOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents E_MAILTextBox As System.Windows.Forms.TextBox
    Friend WithEvents btnConsultar As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
End Class

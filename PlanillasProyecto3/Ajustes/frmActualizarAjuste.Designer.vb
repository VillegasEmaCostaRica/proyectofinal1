﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmActualizarAjuste
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CodCedulaAjusteLabel As System.Windows.Forms.Label
        Dim CodPUESTO_NUEVOLabel As System.Windows.Forms.Label
        Dim DESCRIPCIONPUESTONUEVOLabel As System.Windows.Forms.Label
        Dim SALARIO_NUEVOLabel As System.Windows.Forms.Label
        Dim GRADO_ACADREQUERIDOLabel As System.Windows.Forms.Label
        Dim _CUMPLE_REQUISITO_Label As System.Windows.Forms.Label
        Dim DiferenciaLabel As System.Windows.Forms.Label
        Dim DiarioLabel As System.Windows.Forms.Label
        Dim TDIASAJUSTARLabel As System.Windows.Forms.Label
        Dim TOTALAJUSTARLabel As System.Windows.Forms.Label
        Me.Planilla2DataSet = New PlanillasProyecto3.Planilla2DataSet()
        Me.AjustesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.AjustesTableAdapter = New PlanillasProyecto3.Planilla2DataSetTableAdapters.AjustesTableAdapter()
        Me.TableAdapterManager = New PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnActualizar = New System.Windows.Forms.Button()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.CodCedulaAjusteTextBox = New System.Windows.Forms.TextBox()
        Me.CodPUESTO_NUEVOTextBox = New System.Windows.Forms.TextBox()
        Me.DESCRIPCIONPUESTONUEVOTextBox = New System.Windows.Forms.TextBox()
        Me.SALARIO_NUEVOTextBox = New System.Windows.Forms.TextBox()
        Me.GRADO_ACADREQUERIDOTextBox = New System.Windows.Forms.TextBox()
        Me._CUMPLE_REQUISITO_CheckBox = New System.Windows.Forms.CheckBox()
        Me.DiferenciaTextBox = New System.Windows.Forms.TextBox()
        Me.DiarioTextBox = New System.Windows.Forms.TextBox()
        Me.TDIASAJUSTARTextBox = New System.Windows.Forms.TextBox()
        Me.TOTALAJUSTARTextBox = New System.Windows.Forms.TextBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        CodCedulaAjusteLabel = New System.Windows.Forms.Label()
        CodPUESTO_NUEVOLabel = New System.Windows.Forms.Label()
        DESCRIPCIONPUESTONUEVOLabel = New System.Windows.Forms.Label()
        SALARIO_NUEVOLabel = New System.Windows.Forms.Label()
        GRADO_ACADREQUERIDOLabel = New System.Windows.Forms.Label()
        _CUMPLE_REQUISITO_Label = New System.Windows.Forms.Label()
        DiferenciaLabel = New System.Windows.Forms.Label()
        DiarioLabel = New System.Windows.Forms.Label()
        TDIASAJUSTARLabel = New System.Windows.Forms.Label()
        TOTALAJUSTARLabel = New System.Windows.Forms.Label()
        CType(Me.Planilla2DataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AjustesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'CodCedulaAjusteLabel
        '
        CodCedulaAjusteLabel.AutoSize = True
        CodCedulaAjusteLabel.Location = New System.Drawing.Point(15, 37)
        CodCedulaAjusteLabel.Name = "CodCedulaAjusteLabel"
        CodCedulaAjusteLabel.Size = New System.Drawing.Size(97, 13)
        CodCedulaAjusteLabel.TabIndex = 21
        CodCedulaAjusteLabel.Text = "Cod Cedula Ajuste:"
        '
        'CodPUESTO_NUEVOLabel
        '
        CodPUESTO_NUEVOLabel.AutoSize = True
        CodPUESTO_NUEVOLabel.Location = New System.Drawing.Point(15, 63)
        CodPUESTO_NUEVOLabel.Name = "CodPUESTO_NUEVOLabel"
        CodPUESTO_NUEVOLabel.Size = New System.Drawing.Size(117, 13)
        CodPUESTO_NUEVOLabel.TabIndex = 23
        CodPUESTO_NUEVOLabel.Text = "Cod PUESTO NUEVO:"
        '
        'DESCRIPCIONPUESTONUEVOLabel
        '
        DESCRIPCIONPUESTONUEVOLabel.AutoSize = True
        DESCRIPCIONPUESTONUEVOLabel.Location = New System.Drawing.Point(15, 89)
        DESCRIPCIONPUESTONUEVOLabel.Name = "DESCRIPCIONPUESTONUEVOLabel"
        DESCRIPCIONPUESTONUEVOLabel.Size = New System.Drawing.Size(165, 13)
        DESCRIPCIONPUESTONUEVOLabel.TabIndex = 25
        DESCRIPCIONPUESTONUEVOLabel.Text = "DESCRIPCIONPUESTONUEVO:"
        '
        'SALARIO_NUEVOLabel
        '
        SALARIO_NUEVOLabel.AutoSize = True
        SALARIO_NUEVOLabel.Location = New System.Drawing.Point(15, 115)
        SALARIO_NUEVOLabel.Name = "SALARIO_NUEVOLabel"
        SALARIO_NUEVOLabel.Size = New System.Drawing.Size(97, 13)
        SALARIO_NUEVOLabel.TabIndex = 27
        SALARIO_NUEVOLabel.Text = "SALARIO NUEVO:"
        '
        'GRADO_ACADREQUERIDOLabel
        '
        GRADO_ACADREQUERIDOLabel.AutoSize = True
        GRADO_ACADREQUERIDOLabel.Location = New System.Drawing.Point(15, 141)
        GRADO_ACADREQUERIDOLabel.Name = "GRADO_ACADREQUERIDOLabel"
        GRADO_ACADREQUERIDOLabel.Size = New System.Drawing.Size(146, 13)
        GRADO_ACADREQUERIDOLabel.TabIndex = 29
        GRADO_ACADREQUERIDOLabel.Text = "GRADO ACADREQUERIDO:"
        '
        '_CUMPLE_REQUISITO_Label
        '
        _CUMPLE_REQUISITO_Label.AutoSize = True
        _CUMPLE_REQUISITO_Label.Location = New System.Drawing.Point(15, 169)
        _CUMPLE_REQUISITO_Label.Name = "_CUMPLE_REQUISITO_Label"
        _CUMPLE_REQUISITO_Label.Size = New System.Drawing.Size(128, 13)
        _CUMPLE_REQUISITO_Label.TabIndex = 31
        _CUMPLE_REQUISITO_Label.Text = "¿CUMPLE REQUISITO?:"
        '
        'DiferenciaLabel
        '
        DiferenciaLabel.AutoSize = True
        DiferenciaLabel.Location = New System.Drawing.Point(15, 197)
        DiferenciaLabel.Name = "DiferenciaLabel"
        DiferenciaLabel.Size = New System.Drawing.Size(58, 13)
        DiferenciaLabel.TabIndex = 33
        DiferenciaLabel.Text = "Diferencia:"
        '
        'DiarioLabel
        '
        DiarioLabel.AutoSize = True
        DiarioLabel.Location = New System.Drawing.Point(15, 223)
        DiarioLabel.Name = "DiarioLabel"
        DiarioLabel.Size = New System.Drawing.Size(37, 13)
        DiarioLabel.TabIndex = 35
        DiarioLabel.Text = "Diario:"
        '
        'TDIASAJUSTARLabel
        '
        TDIASAJUSTARLabel.AutoSize = True
        TDIASAJUSTARLabel.Location = New System.Drawing.Point(15, 249)
        TDIASAJUSTARLabel.Name = "TDIASAJUSTARLabel"
        TDIASAJUSTARLabel.Size = New System.Drawing.Size(91, 13)
        TDIASAJUSTARLabel.TabIndex = 37
        TDIASAJUSTARLabel.Text = "TDIASAJUSTAR:"
        '
        'TOTALAJUSTARLabel
        '
        TOTALAJUSTARLabel.AutoSize = True
        TOTALAJUSTARLabel.Location = New System.Drawing.Point(15, 275)
        TOTALAJUSTARLabel.Name = "TOTALAJUSTARLabel"
        TOTALAJUSTARLabel.Size = New System.Drawing.Size(94, 13)
        TOTALAJUSTARLabel.TabIndex = 39
        TOTALAJUSTARLabel.Text = "TOTALAJUSTAR:"
        '
        'Planilla2DataSet
        '
        Me.Planilla2DataSet.DataSetName = "Planilla2DataSet"
        Me.Planilla2DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'AjustesBindingSource
        '
        Me.AjustesBindingSource.DataMember = "Ajustes"
        Me.AjustesBindingSource.DataSource = Me.Planilla2DataSet
        '
        'AjustesTableAdapter
        '
        Me.AjustesTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.AjustesTableAdapter = Me.AjustesTableAdapter
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.DatosPersonalesTableAdapter = Nothing
        Me.TableAdapterManager.DeduccionesTableAdapter = Nothing
        Me.TableAdapterManager.ImpuestosTableAdapter = Nothing
        Me.TableAdapterManager.PlanillaBisemanalTableAdapter = Nothing
        Me.TableAdapterManager.PlanillaMensualTableAdapter = Nothing
        Me.TableAdapterManager.PuestosTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnActualizar)
        Me.GroupBox1.Controls.Add(Me.btnBuscar)
        Me.GroupBox1.Controls.Add(CodCedulaAjusteLabel)
        Me.GroupBox1.Controls.Add(Me.CodCedulaAjusteTextBox)
        Me.GroupBox1.Controls.Add(CodPUESTO_NUEVOLabel)
        Me.GroupBox1.Controls.Add(Me.CodPUESTO_NUEVOTextBox)
        Me.GroupBox1.Controls.Add(DESCRIPCIONPUESTONUEVOLabel)
        Me.GroupBox1.Controls.Add(Me.DESCRIPCIONPUESTONUEVOTextBox)
        Me.GroupBox1.Controls.Add(SALARIO_NUEVOLabel)
        Me.GroupBox1.Controls.Add(Me.SALARIO_NUEVOTextBox)
        Me.GroupBox1.Controls.Add(GRADO_ACADREQUERIDOLabel)
        Me.GroupBox1.Controls.Add(Me.GRADO_ACADREQUERIDOTextBox)
        Me.GroupBox1.Controls.Add(_CUMPLE_REQUISITO_Label)
        Me.GroupBox1.Controls.Add(Me._CUMPLE_REQUISITO_CheckBox)
        Me.GroupBox1.Controls.Add(DiferenciaLabel)
        Me.GroupBox1.Controls.Add(Me.DiferenciaTextBox)
        Me.GroupBox1.Controls.Add(DiarioLabel)
        Me.GroupBox1.Controls.Add(Me.DiarioTextBox)
        Me.GroupBox1.Controls.Add(TDIASAJUSTARLabel)
        Me.GroupBox1.Controls.Add(Me.TDIASAJUSTARTextBox)
        Me.GroupBox1.Controls.Add(TOTALAJUSTARLabel)
        Me.GroupBox1.Controls.Add(Me.TOTALAJUSTARTextBox)
        Me.GroupBox1.Location = New System.Drawing.Point(125, 58)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(416, 358)
        Me.GroupBox1.TabIndex = 21
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos Ajuste"
        '
        'btnActualizar
        '
        Me.btnActualizar.Location = New System.Drawing.Point(199, 312)
        Me.btnActualizar.Name = "btnActualizar"
        Me.btnActualizar.Size = New System.Drawing.Size(75, 23)
        Me.btnActualizar.TabIndex = 42
        Me.btnActualizar.Text = "&Actualizar"
        Me.ToolTip1.SetToolTip(Me.btnActualizar, "Actualizar Los Ajustes De un Empleado")
        Me.btnActualizar.UseVisualStyleBackColor = True
        '
        'btnBuscar
        '
        Me.btnBuscar.Location = New System.Drawing.Point(316, 30)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(72, 27)
        Me.btnBuscar.TabIndex = 41
        Me.btnBuscar.Text = "&Buscar"
        Me.ToolTip1.SetToolTip(Me.btnBuscar, "Ver Los Ajustes Guardados Para Seleccionar uno")
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'CodCedulaAjusteTextBox
        '
        Me.CodCedulaAjusteTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.AjustesBindingSource, "CodCedulaAjuste", True))
        Me.CodCedulaAjusteTextBox.Location = New System.Drawing.Point(186, 34)
        Me.CodCedulaAjusteTextBox.Name = "CodCedulaAjusteTextBox"
        Me.CodCedulaAjusteTextBox.Size = New System.Drawing.Size(104, 20)
        Me.CodCedulaAjusteTextBox.TabIndex = 22
        '
        'CodPUESTO_NUEVOTextBox
        '
        Me.CodPUESTO_NUEVOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.AjustesBindingSource, "CodPUESTO NUEVO", True))
        Me.CodPUESTO_NUEVOTextBox.Location = New System.Drawing.Point(186, 60)
        Me.CodPUESTO_NUEVOTextBox.Name = "CodPUESTO_NUEVOTextBox"
        Me.CodPUESTO_NUEVOTextBox.Size = New System.Drawing.Size(104, 20)
        Me.CodPUESTO_NUEVOTextBox.TabIndex = 24
        '
        'DESCRIPCIONPUESTONUEVOTextBox
        '
        Me.DESCRIPCIONPUESTONUEVOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.AjustesBindingSource, "DESCRIPCIONPUESTONUEVO", True))
        Me.DESCRIPCIONPUESTONUEVOTextBox.Location = New System.Drawing.Point(186, 86)
        Me.DESCRIPCIONPUESTONUEVOTextBox.Name = "DESCRIPCIONPUESTONUEVOTextBox"
        Me.DESCRIPCIONPUESTONUEVOTextBox.Size = New System.Drawing.Size(104, 20)
        Me.DESCRIPCIONPUESTONUEVOTextBox.TabIndex = 26
        '
        'SALARIO_NUEVOTextBox
        '
        Me.SALARIO_NUEVOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.AjustesBindingSource, "SALARIO NUEVO", True))
        Me.SALARIO_NUEVOTextBox.Location = New System.Drawing.Point(186, 112)
        Me.SALARIO_NUEVOTextBox.Name = "SALARIO_NUEVOTextBox"
        Me.SALARIO_NUEVOTextBox.Size = New System.Drawing.Size(104, 20)
        Me.SALARIO_NUEVOTextBox.TabIndex = 28
        '
        'GRADO_ACADREQUERIDOTextBox
        '
        Me.GRADO_ACADREQUERIDOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.AjustesBindingSource, "GRADO ACADREQUERIDO", True))
        Me.GRADO_ACADREQUERIDOTextBox.Location = New System.Drawing.Point(186, 138)
        Me.GRADO_ACADREQUERIDOTextBox.Name = "GRADO_ACADREQUERIDOTextBox"
        Me.GRADO_ACADREQUERIDOTextBox.Size = New System.Drawing.Size(104, 20)
        Me.GRADO_ACADREQUERIDOTextBox.TabIndex = 30
        '
        '_CUMPLE_REQUISITO_CheckBox
        '
        Me._CUMPLE_REQUISITO_CheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.AjustesBindingSource, "¿CUMPLE REQUISITO?", True))
        Me._CUMPLE_REQUISITO_CheckBox.Location = New System.Drawing.Point(186, 164)
        Me._CUMPLE_REQUISITO_CheckBox.Name = "_CUMPLE_REQUISITO_CheckBox"
        Me._CUMPLE_REQUISITO_CheckBox.Size = New System.Drawing.Size(104, 24)
        Me._CUMPLE_REQUISITO_CheckBox.TabIndex = 32
        Me._CUMPLE_REQUISITO_CheckBox.Text = "Si"
        Me._CUMPLE_REQUISITO_CheckBox.UseVisualStyleBackColor = True
        '
        'DiferenciaTextBox
        '
        Me.DiferenciaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.AjustesBindingSource, "Diferencia", True))
        Me.DiferenciaTextBox.Location = New System.Drawing.Point(186, 194)
        Me.DiferenciaTextBox.Name = "DiferenciaTextBox"
        Me.DiferenciaTextBox.Size = New System.Drawing.Size(104, 20)
        Me.DiferenciaTextBox.TabIndex = 34
        '
        'DiarioTextBox
        '
        Me.DiarioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.AjustesBindingSource, "Diario", True))
        Me.DiarioTextBox.Location = New System.Drawing.Point(186, 220)
        Me.DiarioTextBox.Name = "DiarioTextBox"
        Me.DiarioTextBox.Size = New System.Drawing.Size(104, 20)
        Me.DiarioTextBox.TabIndex = 36
        '
        'TDIASAJUSTARTextBox
        '
        Me.TDIASAJUSTARTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.AjustesBindingSource, "TDIASAJUSTAR", True))
        Me.TDIASAJUSTARTextBox.Location = New System.Drawing.Point(186, 246)
        Me.TDIASAJUSTARTextBox.Name = "TDIASAJUSTARTextBox"
        Me.TDIASAJUSTARTextBox.Size = New System.Drawing.Size(104, 20)
        Me.TDIASAJUSTARTextBox.TabIndex = 38
        '
        'TOTALAJUSTARTextBox
        '
        Me.TOTALAJUSTARTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.AjustesBindingSource, "TOTALAJUSTAR", True))
        Me.TOTALAJUSTARTextBox.Location = New System.Drawing.Point(186, 272)
        Me.TOTALAJUSTARTextBox.Name = "TOTALAJUSTARTextBox"
        Me.TOTALAJUSTARTextBox.Size = New System.Drawing.Size(104, 20)
        Me.TOTALAJUSTARTextBox.TabIndex = 40
        '
        'frmActualizarAjuste
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(655, 428)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmActualizarAjuste"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmActualizarAjuste"
        CType(Me.Planilla2DataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AjustesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Planilla2DataSet As PlanillasProyecto3.Planilla2DataSet
    Friend WithEvents AjustesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents AjustesTableAdapter As PlanillasProyecto3.Planilla2DataSetTableAdapters.AjustesTableAdapter
    Friend WithEvents TableAdapterManager As PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnActualizar As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
    Friend WithEvents CodCedulaAjusteTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CodPUESTO_NUEVOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DESCRIPCIONPUESTONUEVOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SALARIO_NUEVOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents GRADO_ACADREQUERIDOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents _CUMPLE_REQUISITO_CheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents DiferenciaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DiarioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TDIASAJUSTARTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TOTALAJUSTARTextBox As System.Windows.Forms.TextBox
End Class

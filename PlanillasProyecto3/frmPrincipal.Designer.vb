﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPrincipal
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.DatosEmpleadoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresarDatosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultarDatosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ActualizarDatosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EliminarDatosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PuestosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresarPuestosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultarPuestosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ActualizarPuestoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EliminarPuestoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AjustesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresarAjustesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultarAjustesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EliminarAjusteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeduccucionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresarDeduccionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PlanillaMensualToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresarPlanillaMensualToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ModificarPlanillaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PlanillaBisemanalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresarPlanillaBisemanalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ModificarPlanillaMensualBisemanalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OpcionesUsuarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AgregarUsuarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EliminarUsuarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ActualizarUsuarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultarUsuarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImpuestosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteríaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteEmpleadosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteVacacionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AyudaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GarantiasSocialesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CalculoDeVacionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultaDeVacacionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CalcularAguinaldoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultarAguinaldoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Planilla2DataSet = New PlanillasProyecto3.Planilla2DataSet()
        Me.ImpuestosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ImpuestosTableAdapter = New PlanillasProyecto3.Planilla2DataSetTableAdapters.ImpuestosTableAdapter()
        Me.TableAdapterManager = New PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager()
        Me.DatosPersonalesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DatosPersonalesTableAdapter = New PlanillasProyecto3.Planilla2DataSetTableAdapters.DatosPersonalesTableAdapter()
        Me.UsuarioRegistroBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.UsuarioRegistroTableAdapter = New PlanillasProyecto3.Planilla2DataSetTableAdapters.UsuarioRegistroTableAdapter()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.Planilla2DataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImpuestosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DatosPersonalesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UsuarioRegistroBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.PlanillasProyecto3.My.Resources.Resources._0412_avvillas_cambia_formato_pago_planilla_asistida
        Me.PictureBox1.Location = New System.Drawing.Point(318, 281)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(318, 164)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 568)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(982, 22)
        Me.StatusStrip1.TabIndex = 1
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DatosEmpleadoToolStripMenuItem, Me.PuestosToolStripMenuItem, Me.AjustesToolStripMenuItem, Me.DeduccucionesToolStripMenuItem, Me.PlanillaMensualToolStripMenuItem, Me.PlanillaBisemanalToolStripMenuItem, Me.OpcionesUsuarioToolStripMenuItem, Me.ImpuestosToolStripMenuItem, Me.ReporteríaToolStripMenuItem, Me.AyudaToolStripMenuItem, Me.GarantiasSocialesToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(982, 24)
        Me.MenuStrip1.TabIndex = 44
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'DatosEmpleadoToolStripMenuItem
        '
        Me.DatosEmpleadoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IngresarDatosToolStripMenuItem, Me.ConsultarDatosToolStripMenuItem, Me.ActualizarDatosToolStripMenuItem, Me.EliminarDatosToolStripMenuItem})
        Me.DatosEmpleadoToolStripMenuItem.Name = "DatosEmpleadoToolStripMenuItem"
        Me.DatosEmpleadoToolStripMenuItem.Size = New System.Drawing.Size(105, 20)
        Me.DatosEmpleadoToolStripMenuItem.Text = "&Datos Empleado"
        '
        'IngresarDatosToolStripMenuItem
        '
        Me.IngresarDatosToolStripMenuItem.Name = "IngresarDatosToolStripMenuItem"
        Me.IngresarDatosToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.I), System.Windows.Forms.Keys)
        Me.IngresarDatosToolStripMenuItem.Size = New System.Drawing.Size(201, 22)
        Me.IngresarDatosToolStripMenuItem.Text = "&Ingresar Datos"
        '
        'ConsultarDatosToolStripMenuItem
        '
        Me.ConsultarDatosToolStripMenuItem.Name = "ConsultarDatosToolStripMenuItem"
        Me.ConsultarDatosToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.C), System.Windows.Forms.Keys)
        Me.ConsultarDatosToolStripMenuItem.Size = New System.Drawing.Size(201, 22)
        Me.ConsultarDatosToolStripMenuItem.Text = "&Consultar Datos"
        '
        'ActualizarDatosToolStripMenuItem
        '
        Me.ActualizarDatosToolStripMenuItem.Name = "ActualizarDatosToolStripMenuItem"
        Me.ActualizarDatosToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.A), System.Windows.Forms.Keys)
        Me.ActualizarDatosToolStripMenuItem.Size = New System.Drawing.Size(201, 22)
        Me.ActualizarDatosToolStripMenuItem.Text = "&Actualizar Datos"
        '
        'EliminarDatosToolStripMenuItem
        '
        Me.EliminarDatosToolStripMenuItem.Name = "EliminarDatosToolStripMenuItem"
        Me.EliminarDatosToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.E), System.Windows.Forms.Keys)
        Me.EliminarDatosToolStripMenuItem.Size = New System.Drawing.Size(201, 22)
        Me.EliminarDatosToolStripMenuItem.Text = "&Eliminar Datos"
        '
        'PuestosToolStripMenuItem
        '
        Me.PuestosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IngresarPuestosToolStripMenuItem, Me.ConsultarPuestosToolStripMenuItem, Me.ActualizarPuestoToolStripMenuItem, Me.EliminarPuestoToolStripMenuItem})
        Me.PuestosToolStripMenuItem.Name = "PuestosToolStripMenuItem"
        Me.PuestosToolStripMenuItem.Size = New System.Drawing.Size(60, 20)
        Me.PuestosToolStripMenuItem.Text = "&Puestos"
        '
        'IngresarPuestosToolStripMenuItem
        '
        Me.IngresarPuestosToolStripMenuItem.Name = "IngresarPuestosToolStripMenuItem"
        Me.IngresarPuestosToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Shift) _
                    Or System.Windows.Forms.Keys.I), System.Windows.Forms.Keys)
        Me.IngresarPuestosToolStripMenuItem.Size = New System.Drawing.Size(243, 22)
        Me.IngresarPuestosToolStripMenuItem.Text = "&Ingresar Puestos"
        '
        'ConsultarPuestosToolStripMenuItem
        '
        Me.ConsultarPuestosToolStripMenuItem.Name = "ConsultarPuestosToolStripMenuItem"
        Me.ConsultarPuestosToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Shift) _
                    Or System.Windows.Forms.Keys.C), System.Windows.Forms.Keys)
        Me.ConsultarPuestosToolStripMenuItem.Size = New System.Drawing.Size(243, 22)
        Me.ConsultarPuestosToolStripMenuItem.Text = "&Consultar Puestos"
        '
        'ActualizarPuestoToolStripMenuItem
        '
        Me.ActualizarPuestoToolStripMenuItem.Name = "ActualizarPuestoToolStripMenuItem"
        Me.ActualizarPuestoToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Shift) _
                    Or System.Windows.Forms.Keys.A), System.Windows.Forms.Keys)
        Me.ActualizarPuestoToolStripMenuItem.Size = New System.Drawing.Size(243, 22)
        Me.ActualizarPuestoToolStripMenuItem.Text = "&Actualizar Puesto"
        '
        'EliminarPuestoToolStripMenuItem
        '
        Me.EliminarPuestoToolStripMenuItem.Name = "EliminarPuestoToolStripMenuItem"
        Me.EliminarPuestoToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Shift) _
                    Or System.Windows.Forms.Keys.E), System.Windows.Forms.Keys)
        Me.EliminarPuestoToolStripMenuItem.Size = New System.Drawing.Size(243, 22)
        Me.EliminarPuestoToolStripMenuItem.Text = "&Eliminar Puesto"
        '
        'AjustesToolStripMenuItem
        '
        Me.AjustesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IngresarAjustesToolStripMenuItem, Me.ConsultarAjustesToolStripMenuItem, Me.EliminarAjusteToolStripMenuItem, Me.ToolStripMenuItem2})
        Me.AjustesToolStripMenuItem.Name = "AjustesToolStripMenuItem"
        Me.AjustesToolStripMenuItem.Size = New System.Drawing.Size(57, 20)
        Me.AjustesToolStripMenuItem.Text = "&Ajustes"
        '
        'IngresarAjustesToolStripMenuItem
        '
        Me.IngresarAjustesToolStripMenuItem.Name = "IngresarAjustesToolStripMenuItem"
        Me.IngresarAjustesToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.IngresarAjustesToolStripMenuItem.Text = "&Ingresar Ajustes"
        '
        'ConsultarAjustesToolStripMenuItem
        '
        Me.ConsultarAjustesToolStripMenuItem.Name = "ConsultarAjustesToolStripMenuItem"
        Me.ConsultarAjustesToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.ConsultarAjustesToolStripMenuItem.Text = "&Consultar Ajustes"
        '
        'EliminarAjusteToolStripMenuItem
        '
        Me.EliminarAjusteToolStripMenuItem.Name = "EliminarAjusteToolStripMenuItem"
        Me.EliminarAjusteToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.EliminarAjusteToolStripMenuItem.Text = "&Eliminar Ajuste"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(166, 22)
        Me.ToolStripMenuItem2.Text = "&Actualizar Ajuste"
        '
        'DeduccucionesToolStripMenuItem
        '
        Me.DeduccucionesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IngresarDeduccionesToolStripMenuItem})
        Me.DeduccucionesToolStripMenuItem.Name = "DeduccucionesToolStripMenuItem"
        Me.DeduccucionesToolStripMenuItem.Size = New System.Drawing.Size(87, 20)
        Me.DeduccucionesToolStripMenuItem.Text = "&Deducciones"
        '
        'IngresarDeduccionesToolStripMenuItem
        '
        Me.IngresarDeduccionesToolStripMenuItem.Name = "IngresarDeduccionesToolStripMenuItem"
        Me.IngresarDeduccionesToolStripMenuItem.Size = New System.Drawing.Size(187, 22)
        Me.IngresarDeduccionesToolStripMenuItem.Text = "&Ingresar Deducciones"
        '
        'PlanillaMensualToolStripMenuItem
        '
        Me.PlanillaMensualToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IngresarPlanillaMensualToolStripMenuItem, Me.ModificarPlanillaToolStripMenuItem})
        Me.PlanillaMensualToolStripMenuItem.Name = "PlanillaMensualToolStripMenuItem"
        Me.PlanillaMensualToolStripMenuItem.Size = New System.Drawing.Size(105, 20)
        Me.PlanillaMensualToolStripMenuItem.Text = "&Planilla Mensual"
        '
        'IngresarPlanillaMensualToolStripMenuItem
        '
        Me.IngresarPlanillaMensualToolStripMenuItem.Name = "IngresarPlanillaMensualToolStripMenuItem"
        Me.IngresarPlanillaMensualToolStripMenuItem.Size = New System.Drawing.Size(220, 22)
        Me.IngresarPlanillaMensualToolStripMenuItem.Text = "&Ingresar Planilla Mensual"
        '
        'ModificarPlanillaToolStripMenuItem
        '
        Me.ModificarPlanillaToolStripMenuItem.Name = "ModificarPlanillaToolStripMenuItem"
        Me.ModificarPlanillaToolStripMenuItem.Size = New System.Drawing.Size(220, 22)
        Me.ModificarPlanillaToolStripMenuItem.Text = "&Modificar Planilla  Mensual "
        '
        'PlanillaBisemanalToolStripMenuItem
        '
        Me.PlanillaBisemanalToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IngresarPlanillaBisemanalToolStripMenuItem, Me.ModificarPlanillaMensualBisemanalToolStripMenuItem})
        Me.PlanillaBisemanalToolStripMenuItem.Name = "PlanillaBisemanalToolStripMenuItem"
        Me.PlanillaBisemanalToolStripMenuItem.Size = New System.Drawing.Size(114, 20)
        Me.PlanillaBisemanalToolStripMenuItem.Text = "&Planilla Bisemanal"
        '
        'IngresarPlanillaBisemanalToolStripMenuItem
        '
        Me.IngresarPlanillaBisemanalToolStripMenuItem.Name = "IngresarPlanillaBisemanalToolStripMenuItem"
        Me.IngresarPlanillaBisemanalToolStripMenuItem.Size = New System.Drawing.Size(223, 22)
        Me.IngresarPlanillaBisemanalToolStripMenuItem.Text = "&Ingresar Planilla Bisemanal"
        '
        'ModificarPlanillaMensualBisemanalToolStripMenuItem
        '
        Me.ModificarPlanillaMensualBisemanalToolStripMenuItem.Name = "ModificarPlanillaMensualBisemanalToolStripMenuItem"
        Me.ModificarPlanillaMensualBisemanalToolStripMenuItem.Size = New System.Drawing.Size(223, 22)
        Me.ModificarPlanillaMensualBisemanalToolStripMenuItem.Text = "&Modificar Planilla Bisemanal"
        '
        'OpcionesUsuarioToolStripMenuItem
        '
        Me.OpcionesUsuarioToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AgregarUsuarioToolStripMenuItem, Me.EliminarUsuarioToolStripMenuItem, Me.ActualizarUsuarioToolStripMenuItem, Me.ConsultarUsuarioToolStripMenuItem})
        Me.OpcionesUsuarioToolStripMenuItem.Name = "OpcionesUsuarioToolStripMenuItem"
        Me.OpcionesUsuarioToolStripMenuItem.Size = New System.Drawing.Size(112, 20)
        Me.OpcionesUsuarioToolStripMenuItem.Text = "&Opciones Usuario"
        '
        'AgregarUsuarioToolStripMenuItem
        '
        Me.AgregarUsuarioToolStripMenuItem.Name = "AgregarUsuarioToolStripMenuItem"
        Me.AgregarUsuarioToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.F), System.Windows.Forms.Keys)
        Me.AgregarUsuarioToolStripMenuItem.Size = New System.Drawing.Size(211, 22)
        Me.AgregarUsuarioToolStripMenuItem.Text = "&Agregar Usuario "
        '
        'EliminarUsuarioToolStripMenuItem
        '
        Me.EliminarUsuarioToolStripMenuItem.Name = "EliminarUsuarioToolStripMenuItem"
        Me.EliminarUsuarioToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.B), System.Windows.Forms.Keys)
        Me.EliminarUsuarioToolStripMenuItem.Size = New System.Drawing.Size(211, 22)
        Me.EliminarUsuarioToolStripMenuItem.Text = "&Eliminar Usuario"
        '
        'ActualizarUsuarioToolStripMenuItem
        '
        Me.ActualizarUsuarioToolStripMenuItem.Name = "ActualizarUsuarioToolStripMenuItem"
        Me.ActualizarUsuarioToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.D), System.Windows.Forms.Keys)
        Me.ActualizarUsuarioToolStripMenuItem.Size = New System.Drawing.Size(211, 22)
        Me.ActualizarUsuarioToolStripMenuItem.Text = "Ac&tualizar Usuario"
        '
        'ConsultarUsuarioToolStripMenuItem
        '
        Me.ConsultarUsuarioToolStripMenuItem.Name = "ConsultarUsuarioToolStripMenuItem"
        Me.ConsultarUsuarioToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.H), System.Windows.Forms.Keys)
        Me.ConsultarUsuarioToolStripMenuItem.Size = New System.Drawing.Size(211, 22)
        Me.ConsultarUsuarioToolStripMenuItem.Text = "&Consultar Usuario"
        '
        'ImpuestosToolStripMenuItem
        '
        Me.ImpuestosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IngresarToolStripMenuItem})
        Me.ImpuestosToolStripMenuItem.Name = "ImpuestosToolStripMenuItem"
        Me.ImpuestosToolStripMenuItem.Size = New System.Drawing.Size(74, 20)
        Me.ImpuestosToolStripMenuItem.Text = "&Impuestos"
        '
        'IngresarToolStripMenuItem
        '
        Me.IngresarToolStripMenuItem.Name = "IngresarToolStripMenuItem"
        Me.IngresarToolStripMenuItem.Size = New System.Drawing.Size(116, 22)
        Me.IngresarToolStripMenuItem.Text = "&Ingresar"
        '
        'ReporteríaToolStripMenuItem
        '
        Me.ReporteríaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ReporteEmpleadosToolStripMenuItem, Me.ToolStripMenuItem1, Me.ToolStripMenuItem3, Me.ReporteVacacionesToolStripMenuItem})
        Me.ReporteríaToolStripMenuItem.Name = "ReporteríaToolStripMenuItem"
        Me.ReporteríaToolStripMenuItem.Size = New System.Drawing.Size(73, 20)
        Me.ReporteríaToolStripMenuItem.Text = "&Reportería"
        '
        'ReporteEmpleadosToolStripMenuItem
        '
        Me.ReporteEmpleadosToolStripMenuItem.Name = "ReporteEmpleadosToolStripMenuItem"
        Me.ReporteEmpleadosToolStripMenuItem.Size = New System.Drawing.Size(177, 22)
        Me.ReporteEmpleadosToolStripMenuItem.Text = "Reporte Empleados"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(177, 22)
        Me.ToolStripMenuItem1.Text = "Reporte Puestos"
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        Me.ToolStripMenuItem3.Size = New System.Drawing.Size(177, 22)
        Me.ToolStripMenuItem3.Text = "Reporte Aguinaldo"
        '
        'ReporteVacacionesToolStripMenuItem
        '
        Me.ReporteVacacionesToolStripMenuItem.Name = "ReporteVacacionesToolStripMenuItem"
        Me.ReporteVacacionesToolStripMenuItem.Size = New System.Drawing.Size(177, 22)
        Me.ReporteVacacionesToolStripMenuItem.Text = "Reporte Vacaciones"
        '
        'AyudaToolStripMenuItem
        '
        Me.AyudaToolStripMenuItem.Name = "AyudaToolStripMenuItem"
        Me.AyudaToolStripMenuItem.Size = New System.Drawing.Size(53, 20)
        Me.AyudaToolStripMenuItem.Text = "Ayuda"
        '
        'GarantiasSocialesToolStripMenuItem
        '
        Me.GarantiasSocialesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CalculoDeVacionesToolStripMenuItem, Me.ConsultaDeVacacionesToolStripMenuItem, Me.CalcularAguinaldoToolStripMenuItem, Me.ConsultarAguinaldoToolStripMenuItem})
        Me.GarantiasSocialesToolStripMenuItem.Name = "GarantiasSocialesToolStripMenuItem"
        Me.GarantiasSocialesToolStripMenuItem.Size = New System.Drawing.Size(113, 20)
        Me.GarantiasSocialesToolStripMenuItem.Text = "Garantias Sociales"
        '
        'CalculoDeVacionesToolStripMenuItem
        '
        Me.CalculoDeVacionesToolStripMenuItem.Name = "CalculoDeVacionesToolStripMenuItem"
        Me.CalculoDeVacionesToolStripMenuItem.Size = New System.Drawing.Size(200, 22)
        Me.CalculoDeVacionesToolStripMenuItem.Text = "Calculo De Vaciones"
        '
        'ConsultaDeVacacionesToolStripMenuItem
        '
        Me.ConsultaDeVacacionesToolStripMenuItem.Name = "ConsultaDeVacacionesToolStripMenuItem"
        Me.ConsultaDeVacacionesToolStripMenuItem.Size = New System.Drawing.Size(200, 22)
        Me.ConsultaDeVacacionesToolStripMenuItem.Text = "Consulta De Vacaciones"
        '
        'CalcularAguinaldoToolStripMenuItem
        '
        Me.CalcularAguinaldoToolStripMenuItem.Name = "CalcularAguinaldoToolStripMenuItem"
        Me.CalcularAguinaldoToolStripMenuItem.Size = New System.Drawing.Size(200, 22)
        Me.CalcularAguinaldoToolStripMenuItem.Text = "Calcular Aguinaldo"
        '
        'ConsultarAguinaldoToolStripMenuItem
        '
        Me.ConsultarAguinaldoToolStripMenuItem.Name = "ConsultarAguinaldoToolStripMenuItem"
        Me.ConsultarAguinaldoToolStripMenuItem.Size = New System.Drawing.Size(200, 22)
        Me.ConsultarAguinaldoToolStripMenuItem.Text = "Consultar Aguinaldo"
        '
        'Planilla2DataSet
        '
        Me.Planilla2DataSet.DataSetName = "Planilla2DataSet"
        Me.Planilla2DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ImpuestosBindingSource
        '
        Me.ImpuestosBindingSource.DataMember = "Impuestos"
        Me.ImpuestosBindingSource.DataSource = Me.Planilla2DataSet
        '
        'ImpuestosTableAdapter
        '
        Me.ImpuestosTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.AguinaldoTableAdapter = Nothing
        Me.TableAdapterManager.AjustesTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.DatosPersonalesTableAdapter = Nothing
        Me.TableAdapterManager.DeduccionesTableAdapter = Nothing
        Me.TableAdapterManager.ImpuestosTableAdapter = Me.ImpuestosTableAdapter
        Me.TableAdapterManager.PlanillaBisemanalTableAdapter = Nothing
        Me.TableAdapterManager.PlanillaMensualTableAdapter = Nothing
        Me.TableAdapterManager.PuestosTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.UsuarioRegistroTableAdapter = Nothing
        Me.TableAdapterManager.VacacionesTableAdapter = Nothing
        '
        'DatosPersonalesBindingSource
        '
        Me.DatosPersonalesBindingSource.DataMember = "DatosPersonales"
        Me.DatosPersonalesBindingSource.DataSource = Me.Planilla2DataSet
        '
        'DatosPersonalesTableAdapter
        '
        Me.DatosPersonalesTableAdapter.ClearBeforeFill = True
        '
        'UsuarioRegistroBindingSource
        '
        Me.UsuarioRegistroBindingSource.DataMember = "UsuarioRegistro"
        Me.UsuarioRegistroBindingSource.DataSource = Me.Planilla2DataSet
        '
        'UsuarioRegistroTableAdapter
        '
        Me.UsuarioRegistroTableAdapter.ClearBeforeFill = True
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.PlanillasProyecto3.My.Resources.Resources.icono_calc
        Me.PictureBox2.Location = New System.Drawing.Point(55, 117)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(172, 170)
        Me.PictureBox2.TabIndex = 45
        Me.PictureBox2.TabStop = False
        '
        'frmPrincipal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.PlanillasProyecto3.My.Resources.Resources.planillas_de_pago
        Me.ClientSize = New System.Drawing.Size(982, 590)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.PictureBox1)
        Me.MaximizeBox = False
        Me.Name = "frmPrincipal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmPrincipal"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.Planilla2DataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImpuestosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DatosPersonalesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UsuarioRegistroBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents DatosEmpleadoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IngresarDatosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultarDatosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ActualizarDatosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EliminarDatosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PuestosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IngresarPuestosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultarPuestosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ActualizarPuestoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EliminarPuestoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AjustesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IngresarAjustesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultarAjustesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ActualizarAjusteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeduccucionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IngresarDeduccionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PlanillaMensualToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IngresarPlanillaMensualToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PlanillaBisemanalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IngresarPlanillaBisemanalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReporteríaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OpcionesUsuarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AgregarUsuarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EliminarUsuarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ActualizarUsuarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultarUsuarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ImpuestosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IngresarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Planilla2DataSet As PlanillasProyecto3.Planilla2DataSet
    Friend WithEvents ImpuestosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ImpuestosTableAdapter As PlanillasProyecto3.Planilla2DataSetTableAdapters.ImpuestosTableAdapter
    Friend WithEvents TableAdapterManager As PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager
    Friend WithEvents EliminarAjusteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DatosPersonalesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DatosPersonalesTableAdapter As PlanillasProyecto3.Planilla2DataSetTableAdapters.DatosPersonalesTableAdapter
    Friend WithEvents UsuarioRegistroBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents UsuarioRegistroTableAdapter As PlanillasProyecto3.Planilla2DataSetTableAdapters.UsuarioRegistroTableAdapter
    Friend WithEvents ReporteEmpleadosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem3 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReporteVacacionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AyudaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GarantiasSocialesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CalculoDeVacionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultaDeVacacionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CalcularAguinaldoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultarAguinaldoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ModificarPlanillaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ModificarPlanillaMensualBisemanalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox

End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPlanillaMensuals
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ProcentajeImpuesto1Label As System.Windows.Forms.Label
        Dim ProcentajeImpuesto2Label As System.Windows.Forms.Label
        Dim CodCedulaAjusteLabel As System.Windows.Forms.Label
        Dim TOTALAJUSTARLabel As System.Windows.Forms.Label
        Dim SALARIO_UNICOLabel As System.Windows.Forms.Label
        Dim CodDeduccionLabel As System.Windows.Forms.Label
        Dim SumaTotalLabel As System.Windows.Forms.Label
        Dim NoCEDULALabel As System.Windows.Forms.Label
        Dim NOMBRE_COMPLETOLabel As System.Windows.Forms.Label
        Dim CODPUESTOLabel As System.Windows.Forms.Label
        Dim FECHA_INGRESOLabel As System.Windows.Forms.Label
        Dim AÑOSSERVLabel As System.Windows.Forms.Label
        Dim N__CONYUGESLabel As System.Windows.Forms.Label
        Dim N__HIJOSLabel As System.Windows.Forms.Label
        Dim FechaPlanMensualLabel As System.Windows.Forms.Label
        Dim __ANUALLabel As System.Windows.Forms.Label
        Dim ANUALIDADLabel As System.Windows.Forms.Label
        Dim SALARIO_DIARIOLabel As System.Windows.Forms.Label
        Dim LIQUIDO_A_PAGARLabel As System.Windows.Forms.Label
        Dim DIAS_TRABAJLabel As System.Windows.Forms.Label
        Dim InstitucionIncapacidadLabel As System.Windows.Forms.Label
        Dim HORAS_EXTRAS_REGULARLabel As System.Windows.Forms.Label
        Dim HORAS_EXTRAS_DOBLESLabel As System.Windows.Forms.Label
        Dim AsientoLabel As System.Windows.Forms.Label
        Dim IMP_RENTALabel As System.Windows.Forms.Label
        Dim EXTRASLabel As System.Windows.Forms.Label
        Dim CESANTIALabel As System.Windows.Forms.Label
        Dim SUBSIDIOLabel As System.Windows.Forms.Label
        Dim CCSSLabel As System.Windows.Forms.Label
        Dim TOTAL_DIAS_INCAPLabel As System.Windows.Forms.Label
        Dim FechaINCAPINILabel As System.Windows.Forms.Label
        Dim FechaINCAPFINLabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim ProcentajeImpuesto1Label1 As System.Windows.Forms.Label
        Dim ProcentajeImpuesto2Label1 As System.Windows.Forms.Label
        Dim CreditosFiscalesHijoLabel1 As System.Windows.Forms.Label
        Dim CreditosFiscalesConyugeLabel1 As System.Windows.Forms.Label
        Dim CodImpuestoLabel As System.Windows.Forms.Label
        Dim CreditosFiscalesConyugeLabel As System.Windows.Forms.Label
        Dim CreditosFiscalesHijoLabel As System.Windows.Forms.Label
        Dim CodImpuestoLabel1 As System.Windows.Forms.Label
        Dim ProcentajeImpuesto1Label2 As System.Windows.Forms.Label
        Dim ProcentajeImpuesto2Label2 As System.Windows.Forms.Label
        Dim CreditosFiscalesHijoLabel2 As System.Windows.Forms.Label
        Dim CreditosFiscalesConyugeLabel2 As System.Windows.Forms.Label
        Me.ProcentajeImpuesto1TextBox = New System.Windows.Forms.TextBox()
        Me.ProcentajeImpuesto2TextBox = New System.Windows.Forms.TextBox()
        Me.CreditosFiscalesHijoTextBox = New System.Windows.Forms.TextBox()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtAños = New System.Windows.Forms.TextBox()
        Me.CodDeduccionTextBox = New System.Windows.Forms.TextBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.CodCedulaAjusteTextBox = New System.Windows.Forms.TextBox()
        Me.TOTALAJUSTARTextBox = New System.Windows.Forms.TextBox()
        Me.SALARIO_UNICOTextBox = New System.Windows.Forms.TextBox()
        Me.SumaTotalTextBox = New System.Windows.Forms.TextBox()
        Me.NoCEDULATextBox = New System.Windows.Forms.TextBox()
        Me.NOMBRE_COMPLETOTextBox = New System.Windows.Forms.TextBox()
        Me.CODPUESTOTextBox = New System.Windows.Forms.TextBox()
        Me.FECHA_INGRESODateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.N__CONYUGESCheckBox = New System.Windows.Forms.CheckBox()
        Me.N__HIJOSTextBox = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtImpuestoSobreSalario = New System.Windows.Forms.TextBox()
        Me.FechaPlanMensualDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.__ANUALTextBox = New System.Windows.Forms.TextBox()
        Me.ANUALIDADTextBox = New System.Windows.Forms.TextBox()
        Me.SALARIO_DIARIOTextBox = New System.Windows.Forms.TextBox()
        Me.AsientoTextBox = New System.Windows.Forms.TextBox()
        Me.IMP_RENTATextBox = New System.Windows.Forms.TextBox()
        Me.CCSSTextBox = New System.Windows.Forms.TextBox()
        Me.CESANTIATextBox = New System.Windows.Forms.TextBox()
        Me.LIQUIDO_A_PAGARTextBox = New System.Windows.Forms.TextBox()
        Me.EXTRASTextBox = New System.Windows.Forms.TextBox()
        Me.SUBSIDIOTextBox = New System.Windows.Forms.TextBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.InstitucionIncapacidadTextBox = New System.Windows.Forms.ComboBox()
        Me.TOTAL_DIAS_INCAPTextBox = New System.Windows.Forms.TextBox()
        Me.FechaINCAPINIDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.FechaINCAPFINDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.DIAS_TRABAJTextBox = New System.Windows.Forms.ComboBox()
        Me.HORAS_EXTRAS_DOBLESTextBox = New System.Windows.Forms.ComboBox()
        Me.HORAS_EXTRAS_REGULARTextBox = New System.Windows.Forms.ComboBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.CodImpuestoTextBox = New System.Windows.Forms.TextBox()
        Me.ProcentajeImpuesto1TextBox1 = New System.Windows.Forms.TextBox()
        Me.ProcentajeImpuesto2TextBox1 = New System.Windows.Forms.TextBox()
        Me.CreditosFiscalesHijoTextBox1 = New System.Windows.Forms.TextBox()
        Me.CreditosFiscalesConyugeTextBox1 = New System.Windows.Forms.TextBox()
        Me.CreditosFiscalesConyugeTextBox = New System.Windows.Forms.TextBox()
        Me.ArchivoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RegresarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.AccionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GuardarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BuscarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ToolStripButton4 = New System.Windows.Forms.ToolStripButton()
        Me.CodImpuestoTextBox1 = New System.Windows.Forms.TextBox()
        Me.ProcentajeImpuesto1TextBox2 = New System.Windows.Forms.TextBox()
        Me.ProcentajeImpuesto2TextBox2 = New System.Windows.Forms.TextBox()
        Me.CreditosFiscalesHijoTextBox2 = New System.Windows.Forms.TextBox()
        Me.CreditosFiscalesConyugeTextBox2 = New System.Windows.Forms.TextBox()
        Me.PlanillaBisemanalDataGridView = New System.Windows.Forms.DataGridView()
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn20 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn21 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn22 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn23 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PlanillaBisemanalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Planilla2DataSet1 = New PlanillasProyecto3.Planilla2DataSet1()
        Me.ImpuestosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PlanillaMensualBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.AjustesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PuestosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DeduccionesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DatosPersonalesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DatosPersonalesTableAdapter = New PlanillasProyecto3.Planilla2DataSet1TableAdapters.DatosPersonalesTableAdapter()
        Me.TableAdapterManager = New PlanillasProyecto3.Planilla2DataSet1TableAdapters.TableAdapterManager()
        Me.DeduccionesTableAdapter = New PlanillasProyecto3.Planilla2DataSet1TableAdapters.DeduccionesTableAdapter()
        Me.ImpuestosTableAdapter = New PlanillasProyecto3.Planilla2DataSet1TableAdapters.ImpuestosTableAdapter()
        Me.PuestosTableAdapter = New PlanillasProyecto3.Planilla2DataSet1TableAdapters.PuestosTableAdapter()
        Me.AjustesTableAdapter = New PlanillasProyecto3.Planilla2DataSet1TableAdapters.AjustesTableAdapter()
        Me.PlanillaMensualTableAdapter = New PlanillasProyecto3.Planilla2DataSet1TableAdapters.PlanillaMensualTableAdapter()
        Me.PlanillaBisemanalTableAdapter = New PlanillasProyecto3.Planilla2DataSet1TableAdapters.PlanillaBisemanalTableAdapter()
        Me.PlanillaBisemanalDataGridView1 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn24 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn25 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn26 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn27 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn28 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn29 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn30 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn31 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn32 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn33 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn34 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn35 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn36 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn37 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn38 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn39 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn40 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn41 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn42 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn43 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn44 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn45 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn46 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PlanillaMensualDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn47 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn48 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn49 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn50 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn51 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn52 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn53 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn54 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn55 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn56 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn57 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn58 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn59 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn60 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn61 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn62 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn63 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn64 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn65 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn66 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn67 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn68 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn69 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        ProcentajeImpuesto1Label = New System.Windows.Forms.Label()
        ProcentajeImpuesto2Label = New System.Windows.Forms.Label()
        CodCedulaAjusteLabel = New System.Windows.Forms.Label()
        TOTALAJUSTARLabel = New System.Windows.Forms.Label()
        SALARIO_UNICOLabel = New System.Windows.Forms.Label()
        CodDeduccionLabel = New System.Windows.Forms.Label()
        SumaTotalLabel = New System.Windows.Forms.Label()
        NoCEDULALabel = New System.Windows.Forms.Label()
        NOMBRE_COMPLETOLabel = New System.Windows.Forms.Label()
        CODPUESTOLabel = New System.Windows.Forms.Label()
        FECHA_INGRESOLabel = New System.Windows.Forms.Label()
        AÑOSSERVLabel = New System.Windows.Forms.Label()
        N__CONYUGESLabel = New System.Windows.Forms.Label()
        N__HIJOSLabel = New System.Windows.Forms.Label()
        FechaPlanMensualLabel = New System.Windows.Forms.Label()
        __ANUALLabel = New System.Windows.Forms.Label()
        ANUALIDADLabel = New System.Windows.Forms.Label()
        SALARIO_DIARIOLabel = New System.Windows.Forms.Label()
        LIQUIDO_A_PAGARLabel = New System.Windows.Forms.Label()
        DIAS_TRABAJLabel = New System.Windows.Forms.Label()
        InstitucionIncapacidadLabel = New System.Windows.Forms.Label()
        HORAS_EXTRAS_REGULARLabel = New System.Windows.Forms.Label()
        HORAS_EXTRAS_DOBLESLabel = New System.Windows.Forms.Label()
        AsientoLabel = New System.Windows.Forms.Label()
        IMP_RENTALabel = New System.Windows.Forms.Label()
        EXTRASLabel = New System.Windows.Forms.Label()
        CESANTIALabel = New System.Windows.Forms.Label()
        SUBSIDIOLabel = New System.Windows.Forms.Label()
        CCSSLabel = New System.Windows.Forms.Label()
        TOTAL_DIAS_INCAPLabel = New System.Windows.Forms.Label()
        FechaINCAPINILabel = New System.Windows.Forms.Label()
        FechaINCAPFINLabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        ProcentajeImpuesto1Label1 = New System.Windows.Forms.Label()
        ProcentajeImpuesto2Label1 = New System.Windows.Forms.Label()
        CreditosFiscalesHijoLabel1 = New System.Windows.Forms.Label()
        CreditosFiscalesConyugeLabel1 = New System.Windows.Forms.Label()
        CodImpuestoLabel = New System.Windows.Forms.Label()
        CreditosFiscalesConyugeLabel = New System.Windows.Forms.Label()
        CreditosFiscalesHijoLabel = New System.Windows.Forms.Label()
        CodImpuestoLabel1 = New System.Windows.Forms.Label()
        ProcentajeImpuesto1Label2 = New System.Windows.Forms.Label()
        ProcentajeImpuesto2Label2 = New System.Windows.Forms.Label()
        CreditosFiscalesHijoLabel2 = New System.Windows.Forms.Label()
        CreditosFiscalesConyugeLabel2 = New System.Windows.Forms.Label()
        Me.StatusStrip1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        CType(Me.PlanillaBisemanalDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PlanillaBisemanalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Planilla2DataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImpuestosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PlanillaMensualBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AjustesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PuestosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DeduccionesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DatosPersonalesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PlanillaBisemanalDataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PlanillaMensualDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ProcentajeImpuesto1Label
        '
        ProcentajeImpuesto1Label.AutoSize = True
        ProcentajeImpuesto1Label.Location = New System.Drawing.Point(-30, -22)
        ProcentajeImpuesto1Label.Name = "ProcentajeImpuesto1Label"
        ProcentajeImpuesto1Label.Size = New System.Drawing.Size(112, 13)
        ProcentajeImpuesto1Label.TabIndex = 63
        ProcentajeImpuesto1Label.Text = "procentaje Impuesto1:"
        ProcentajeImpuesto1Label.Visible = False
        '
        'ProcentajeImpuesto2Label
        '
        ProcentajeImpuesto2Label.AutoSize = True
        ProcentajeImpuesto2Label.Location = New System.Drawing.Point(126, -50)
        ProcentajeImpuesto2Label.Name = "ProcentajeImpuesto2Label"
        ProcentajeImpuesto2Label.Size = New System.Drawing.Size(112, 13)
        ProcentajeImpuesto2Label.TabIndex = 65
        ProcentajeImpuesto2Label.Text = "procentaje Impuesto2:"
        ProcentajeImpuesto2Label.Visible = False
        '
        'CodCedulaAjusteLabel
        '
        CodCedulaAjusteLabel.AutoSize = True
        CodCedulaAjusteLabel.Location = New System.Drawing.Point(9, 289)
        CodCedulaAjusteLabel.Name = "CodCedulaAjusteLabel"
        CodCedulaAjusteLabel.Size = New System.Drawing.Size(97, 13)
        CodCedulaAjusteLabel.TabIndex = 127
        CodCedulaAjusteLabel.Text = "Cod Cedula Ajuste:"
        '
        'TOTALAJUSTARLabel
        '
        TOTALAJUSTARLabel.AutoSize = True
        TOTALAJUSTARLabel.Location = New System.Drawing.Point(12, 312)
        TOTALAJUSTARLabel.Name = "TOTALAJUSTARLabel"
        TOTALAJUSTARLabel.Size = New System.Drawing.Size(94, 13)
        TOTALAJUSTARLabel.TabIndex = 129
        TOTALAJUSTARLabel.Text = "TOTALAJUSTAR:"
        '
        'SALARIO_UNICOLabel
        '
        SALARIO_UNICOLabel.AutoSize = True
        SALARIO_UNICOLabel.Location = New System.Drawing.Point(9, 266)
        SALARIO_UNICOLabel.Name = "SALARIO_UNICOLabel"
        SALARIO_UNICOLabel.Size = New System.Drawing.Size(93, 13)
        SALARIO_UNICOLabel.TabIndex = 125
        SALARIO_UNICOLabel.Text = "SALARIO UNICO:"
        '
        'CodDeduccionLabel
        '
        CodDeduccionLabel.AutoSize = True
        CodDeduccionLabel.Location = New System.Drawing.Point(22, 216)
        CodDeduccionLabel.Name = "CodDeduccionLabel"
        CodDeduccionLabel.Size = New System.Drawing.Size(83, 13)
        CodDeduccionLabel.TabIndex = 117
        CodDeduccionLabel.Text = "cod Deduccion:"
        '
        'SumaTotalLabel
        '
        SumaTotalLabel.AutoSize = True
        SumaTotalLabel.Location = New System.Drawing.Point(22, 242)
        SumaTotalLabel.Name = "SumaTotalLabel"
        SumaTotalLabel.Size = New System.Drawing.Size(64, 13)
        SumaTotalLabel.TabIndex = 119
        SumaTotalLabel.Text = "Suma Total:"
        '
        'NoCEDULALabel
        '
        NoCEDULALabel.AutoSize = True
        NoCEDULALabel.Location = New System.Drawing.Point(20, 37)
        NoCEDULALabel.Name = "NoCEDULALabel"
        NoCEDULALabel.Size = New System.Drawing.Size(70, 13)
        NoCEDULALabel.TabIndex = 103
        NoCEDULALabel.Text = "No CEDULA:"
        '
        'NOMBRE_COMPLETOLabel
        '
        NOMBRE_COMPLETOLabel.AutoSize = True
        NOMBRE_COMPLETOLabel.Location = New System.Drawing.Point(20, 60)
        NOMBRE_COMPLETOLabel.Name = "NOMBRE_COMPLETOLabel"
        NOMBRE_COMPLETOLabel.Size = New System.Drawing.Size(119, 13)
        NOMBRE_COMPLETOLabel.TabIndex = 105
        NOMBRE_COMPLETOLabel.Text = "NOMBRE COMPLETO:"
        '
        'CODPUESTOLabel
        '
        CODPUESTOLabel.AutoSize = True
        CODPUESTOLabel.Location = New System.Drawing.Point(20, 86)
        CODPUESTOLabel.Name = "CODPUESTOLabel"
        CODPUESTOLabel.Size = New System.Drawing.Size(77, 13)
        CODPUESTOLabel.TabIndex = 107
        CODPUESTOLabel.Text = "CODPUESTO:"
        '
        'FECHA_INGRESOLabel
        '
        FECHA_INGRESOLabel.AutoSize = True
        FECHA_INGRESOLabel.Location = New System.Drawing.Point(20, 113)
        FECHA_INGRESOLabel.Name = "FECHA_INGRESOLabel"
        FECHA_INGRESOLabel.Size = New System.Drawing.Size(97, 13)
        FECHA_INGRESOLabel.TabIndex = 109
        FECHA_INGRESOLabel.Text = "FECHA INGRESO:"
        '
        'AÑOSSERVLabel
        '
        AÑOSSERVLabel.AutoSize = True
        AÑOSSERVLabel.Location = New System.Drawing.Point(20, 138)
        AÑOSSERVLabel.Name = "AÑOSSERVLabel"
        AÑOSSERVLabel.Size = New System.Drawing.Size(69, 13)
        AÑOSSERVLabel.TabIndex = 111
        AÑOSSERVLabel.Text = "AÑOSSERV:"
        '
        'N__CONYUGESLabel
        '
        N__CONYUGESLabel.AutoSize = True
        N__CONYUGESLabel.Location = New System.Drawing.Point(20, 166)
        N__CONYUGESLabel.Name = "N__CONYUGESLabel"
        N__CONYUGESLabel.Size = New System.Drawing.Size(85, 13)
        N__CONYUGESLabel.TabIndex = 113
        N__CONYUGESLabel.Text = "N° CONYUGES:"
        '
        'N__HIJOSLabel
        '
        N__HIJOSLabel.AutoSize = True
        N__HIJOSLabel.Location = New System.Drawing.Point(20, 190)
        N__HIJOSLabel.Name = "N__HIJOSLabel"
        N__HIJOSLabel.Size = New System.Drawing.Size(56, 13)
        N__HIJOSLabel.TabIndex = 115
        N__HIJOSLabel.Text = "N° HIJOS:"
        '
        'FechaPlanMensualLabel
        '
        FechaPlanMensualLabel.AutoSize = True
        FechaPlanMensualLabel.Location = New System.Drawing.Point(187, -27)
        FechaPlanMensualLabel.Name = "FechaPlanMensualLabel"
        FechaPlanMensualLabel.Size = New System.Drawing.Size(107, 13)
        FechaPlanMensualLabel.TabIndex = 0
        FechaPlanMensualLabel.Text = "Fecha Plan Mensual:"
        '
        '__ANUALLabel
        '
        __ANUALLabel.AutoSize = True
        __ANUALLabel.Location = New System.Drawing.Point(31, 56)
        __ANUALLabel.Name = "__ANUALLabel"
        __ANUALLabel.Size = New System.Drawing.Size(57, 13)
        __ANUALLabel.TabIndex = 6
        __ANUALLabel.Text = "% ANUAL:"
        '
        'ANUALIDADLabel
        '
        ANUALIDADLabel.AutoSize = True
        ANUALIDADLabel.Location = New System.Drawing.Point(31, 82)
        ANUALIDADLabel.Name = "ANUALIDADLabel"
        ANUALIDADLabel.Size = New System.Drawing.Size(72, 13)
        ANUALIDADLabel.TabIndex = 8
        ANUALIDADLabel.Text = "ANUALIDAD:"
        '
        'SALARIO_DIARIOLabel
        '
        SALARIO_DIARIOLabel.AutoSize = True
        SALARIO_DIARIOLabel.Location = New System.Drawing.Point(31, 108)
        SALARIO_DIARIOLabel.Name = "SALARIO_DIARIOLabel"
        SALARIO_DIARIOLabel.Size = New System.Drawing.Size(96, 13)
        SALARIO_DIARIOLabel.TabIndex = 12
        SALARIO_DIARIOLabel.Text = "SALARIO DIARIO:"
        '
        'LIQUIDO_A_PAGARLabel
        '
        LIQUIDO_A_PAGARLabel.AutoSize = True
        LIQUIDO_A_PAGARLabel.Location = New System.Drawing.Point(12, 140)
        LIQUIDO_A_PAGARLabel.Name = "LIQUIDO_A_PAGARLabel"
        LIQUIDO_A_PAGARLabel.Size = New System.Drawing.Size(104, 13)
        LIQUIDO_A_PAGARLabel.TabIndex = 14
        LIQUIDO_A_PAGARLabel.Text = "LIQUIDO A PAGAR:"
        '
        'DIAS_TRABAJLabel
        '
        DIAS_TRABAJLabel.AutoSize = True
        DIAS_TRABAJLabel.Location = New System.Drawing.Point(9, 22)
        DIAS_TRABAJLabel.Name = "DIAS_TRABAJLabel"
        DIAS_TRABAJLabel.Size = New System.Drawing.Size(79, 13)
        DIAS_TRABAJLabel.TabIndex = 16
        DIAS_TRABAJLabel.Text = "DIAS TRABAJ:"
        '
        'InstitucionIncapacidadLabel
        '
        InstitucionIncapacidadLabel.AutoSize = True
        InstitucionIncapacidadLabel.Location = New System.Drawing.Point(10, 30)
        InstitucionIncapacidadLabel.Name = "InstitucionIncapacidadLabel"
        InstitucionIncapacidadLabel.Size = New System.Drawing.Size(120, 13)
        InstitucionIncapacidadLabel.TabIndex = 20
        InstitucionIncapacidadLabel.Text = "Institucion Incapacidad:"
        '
        'HORAS_EXTRAS_REGULARLabel
        '
        HORAS_EXTRAS_REGULARLabel.AutoSize = True
        HORAS_EXTRAS_REGULARLabel.Location = New System.Drawing.Point(12, 62)
        HORAS_EXTRAS_REGULARLabel.Name = "HORAS_EXTRAS_REGULARLabel"
        HORAS_EXTRAS_REGULARLabel.Size = New System.Drawing.Size(149, 13)
        HORAS_EXTRAS_REGULARLabel.TabIndex = 22
        HORAS_EXTRAS_REGULARLabel.Text = "HORAS EXTRAS REGULAR:"
        '
        'HORAS_EXTRAS_DOBLESLabel
        '
        HORAS_EXTRAS_DOBLESLabel.AutoSize = True
        HORAS_EXTRAS_DOBLESLabel.Location = New System.Drawing.Point(12, 88)
        HORAS_EXTRAS_DOBLESLabel.Name = "HORAS_EXTRAS_DOBLESLabel"
        HORAS_EXTRAS_DOBLESLabel.Size = New System.Drawing.Size(140, 13)
        HORAS_EXTRAS_DOBLESLabel.TabIndex = 24
        HORAS_EXTRAS_DOBLESLabel.Text = "HORAS EXTRAS DOBLES:"
        '
        'AsientoLabel
        '
        AsientoLabel.AutoSize = True
        AsientoLabel.Location = New System.Drawing.Point(33, 212)
        AsientoLabel.Name = "AsientoLabel"
        AsientoLabel.Size = New System.Drawing.Size(45, 13)
        AsientoLabel.TabIndex = 32
        AsientoLabel.Text = "Asiento:"
        '
        'IMP_RENTALabel
        '
        IMP_RENTALabel.AutoSize = True
        IMP_RENTALabel.Location = New System.Drawing.Point(33, 159)
        IMP_RENTALabel.Name = "IMP_RENTALabel"
        IMP_RENTALabel.Size = New System.Drawing.Size(69, 13)
        IMP_RENTALabel.TabIndex = 34
        IMP_RENTALabel.Text = "IMP RENTA:"
        '
        'EXTRASLabel
        '
        EXTRASLabel.AutoSize = True
        EXTRASLabel.Location = New System.Drawing.Point(12, 114)
        EXTRASLabel.Name = "EXTRASLabel"
        EXTRASLabel.Size = New System.Drawing.Size(53, 13)
        EXTRASLabel.TabIndex = 36
        EXTRASLabel.Text = "EXTRAS:"
        '
        'CESANTIALabel
        '
        CESANTIALabel.AutoSize = True
        CESANTIALabel.Location = New System.Drawing.Point(12, 40)
        CESANTIALabel.Name = "CESANTIALabel"
        CESANTIALabel.Size = New System.Drawing.Size(63, 13)
        CESANTIALabel.TabIndex = 38
        CESANTIALabel.Text = "CESANTIA:"
        '
        'SUBSIDIOLabel
        '
        SUBSIDIOLabel.AutoSize = True
        SUBSIDIOLabel.Location = New System.Drawing.Point(4, 132)
        SUBSIDIOLabel.Name = "SUBSIDIOLabel"
        SUBSIDIOLabel.Size = New System.Drawing.Size(61, 13)
        SUBSIDIOLabel.TabIndex = 40
        SUBSIDIOLabel.Text = "SUBSIDIO:"
        '
        'CCSSLabel
        '
        CCSSLabel.AutoSize = True
        CCSSLabel.Location = New System.Drawing.Point(33, 186)
        CCSSLabel.Name = "CCSSLabel"
        CCSSLabel.Size = New System.Drawing.Size(38, 13)
        CCSSLabel.TabIndex = 44
        CCSSLabel.Text = "CCSS:"
        '
        'TOTAL_DIAS_INCAPLabel
        '
        TOTAL_DIAS_INCAPLabel.AutoSize = True
        TOTAL_DIAS_INCAPLabel.Location = New System.Drawing.Point(4, 110)
        TOTAL_DIAS_INCAPLabel.Name = "TOTAL_DIAS_INCAPLabel"
        TOTAL_DIAS_INCAPLabel.Size = New System.Drawing.Size(108, 13)
        TOTAL_DIAS_INCAPLabel.TabIndex = 32
        TOTAL_DIAS_INCAPLabel.Text = "TOTAL DIAS INCAP:"
        '
        'FechaINCAPINILabel
        '
        FechaINCAPINILabel.AutoSize = True
        FechaINCAPINILabel.Location = New System.Drawing.Point(6, 60)
        FechaINCAPINILabel.Name = "FechaINCAPINILabel"
        FechaINCAPINILabel.Size = New System.Drawing.Size(89, 13)
        FechaINCAPINILabel.TabIndex = 34
        FechaINCAPINILabel.Text = "Fecha INCAPINI:"
        '
        'FechaINCAPFINLabel
        '
        FechaINCAPFINLabel.AutoSize = True
        FechaINCAPFINLabel.Location = New System.Drawing.Point(6, 86)
        FechaINCAPFINLabel.Name = "FechaINCAPFINLabel"
        FechaINCAPFINLabel.Size = New System.Drawing.Size(92, 13)
        FechaINCAPFINLabel.TabIndex = 36
        FechaINCAPFINLabel.Text = "Fecha INCAPFIN:"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Location = New System.Drawing.Point(31, 29)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(73, 13)
        Label1.TabIndex = 65
        Label1.Text = "Fecha Planilla"
        '
        'ProcentajeImpuesto1Label1
        '
        ProcentajeImpuesto1Label1.AutoSize = True
        ProcentajeImpuesto1Label1.Location = New System.Drawing.Point(845, 131)
        ProcentajeImpuesto1Label1.Name = "ProcentajeImpuesto1Label1"
        ProcentajeImpuesto1Label1.Size = New System.Drawing.Size(112, 13)
        ProcentajeImpuesto1Label1.TabIndex = 118
        ProcentajeImpuesto1Label1.Text = "procentaje Impuesto1:"
        '
        'ProcentajeImpuesto2Label1
        '
        ProcentajeImpuesto2Label1.AutoSize = True
        ProcentajeImpuesto2Label1.Location = New System.Drawing.Point(845, 157)
        ProcentajeImpuesto2Label1.Name = "ProcentajeImpuesto2Label1"
        ProcentajeImpuesto2Label1.Size = New System.Drawing.Size(112, 13)
        ProcentajeImpuesto2Label1.TabIndex = 120
        ProcentajeImpuesto2Label1.Text = "procentaje Impuesto2:"
        '
        'CreditosFiscalesHijoLabel1
        '
        CreditosFiscalesHijoLabel1.AutoSize = True
        CreditosFiscalesHijoLabel1.Location = New System.Drawing.Point(845, 183)
        CreditosFiscalesHijoLabel1.Name = "CreditosFiscalesHijoLabel1"
        CreditosFiscalesHijoLabel1.Size = New System.Drawing.Size(109, 13)
        CreditosFiscalesHijoLabel1.TabIndex = 122
        CreditosFiscalesHijoLabel1.Text = "creditos Fiscales Hijo:"
        '
        'CreditosFiscalesConyugeLabel1
        '
        CreditosFiscalesConyugeLabel1.AutoSize = True
        CreditosFiscalesConyugeLabel1.Location = New System.Drawing.Point(845, 209)
        CreditosFiscalesConyugeLabel1.Name = "CreditosFiscalesConyugeLabel1"
        CreditosFiscalesConyugeLabel1.Size = New System.Drawing.Size(133, 13)
        CreditosFiscalesConyugeLabel1.TabIndex = 124
        CreditosFiscalesConyugeLabel1.Text = "creditos Fiscales Conyuge:"
        '
        'CodImpuestoLabel
        '
        CodImpuestoLabel.AutoSize = True
        CodImpuestoLabel.Location = New System.Drawing.Point(845, 105)
        CodImpuestoLabel.Name = "CodImpuestoLabel"
        CodImpuestoLabel.Size = New System.Drawing.Size(74, 13)
        CodImpuestoLabel.TabIndex = 116
        CodImpuestoLabel.Text = "cod Impuesto:"
        '
        'CreditosFiscalesConyugeLabel
        '
        CreditosFiscalesConyugeLabel.AutoSize = True
        CreditosFiscalesConyugeLabel.Location = New System.Drawing.Point(101, 32)
        CreditosFiscalesConyugeLabel.Name = "CreditosFiscalesConyugeLabel"
        CreditosFiscalesConyugeLabel.Size = New System.Drawing.Size(133, 13)
        CreditosFiscalesConyugeLabel.TabIndex = 69
        CreditosFiscalesConyugeLabel.Text = "creditos Fiscales Conyuge:"
        CreditosFiscalesConyugeLabel.Visible = False
        '
        'CreditosFiscalesHijoLabel
        '
        CreditosFiscalesHijoLabel.AutoSize = True
        CreditosFiscalesHijoLabel.Location = New System.Drawing.Point(192, 35)
        CreditosFiscalesHijoLabel.Name = "CreditosFiscalesHijoLabel"
        CreditosFiscalesHijoLabel.Size = New System.Drawing.Size(109, 13)
        CreditosFiscalesHijoLabel.TabIndex = 67
        CreditosFiscalesHijoLabel.Text = "creditos Fiscales Hijo:"
        CreditosFiscalesHijoLabel.Visible = False
        '
        'CodImpuestoLabel1
        '
        CodImpuestoLabel1.AutoSize = True
        CodImpuestoLabel1.Location = New System.Drawing.Point(820, 352)
        CodImpuestoLabel1.Name = "CodImpuestoLabel1"
        CodImpuestoLabel1.Size = New System.Drawing.Size(74, 13)
        CodImpuestoLabel1.TabIndex = 125
        CodImpuestoLabel1.Text = "cod Impuesto:"
        '
        'ProcentajeImpuesto1Label2
        '
        ProcentajeImpuesto1Label2.AutoSize = True
        ProcentajeImpuesto1Label2.Location = New System.Drawing.Point(820, 378)
        ProcentajeImpuesto1Label2.Name = "ProcentajeImpuesto1Label2"
        ProcentajeImpuesto1Label2.Size = New System.Drawing.Size(112, 13)
        ProcentajeImpuesto1Label2.TabIndex = 127
        ProcentajeImpuesto1Label2.Text = "procentaje Impuesto1:"
        '
        'ProcentajeImpuesto2Label2
        '
        ProcentajeImpuesto2Label2.AutoSize = True
        ProcentajeImpuesto2Label2.Location = New System.Drawing.Point(820, 404)
        ProcentajeImpuesto2Label2.Name = "ProcentajeImpuesto2Label2"
        ProcentajeImpuesto2Label2.Size = New System.Drawing.Size(112, 13)
        ProcentajeImpuesto2Label2.TabIndex = 129
        ProcentajeImpuesto2Label2.Text = "procentaje Impuesto2:"
        '
        'CreditosFiscalesHijoLabel2
        '
        CreditosFiscalesHijoLabel2.AutoSize = True
        CreditosFiscalesHijoLabel2.Location = New System.Drawing.Point(820, 430)
        CreditosFiscalesHijoLabel2.Name = "CreditosFiscalesHijoLabel2"
        CreditosFiscalesHijoLabel2.Size = New System.Drawing.Size(109, 13)
        CreditosFiscalesHijoLabel2.TabIndex = 131
        CreditosFiscalesHijoLabel2.Text = "creditos Fiscales Hijo:"
        '
        'CreditosFiscalesConyugeLabel2
        '
        CreditosFiscalesConyugeLabel2.AutoSize = True
        CreditosFiscalesConyugeLabel2.Location = New System.Drawing.Point(820, 456)
        CreditosFiscalesConyugeLabel2.Name = "CreditosFiscalesConyugeLabel2"
        CreditosFiscalesConyugeLabel2.Size = New System.Drawing.Size(133, 13)
        CreditosFiscalesConyugeLabel2.TabIndex = 133
        CreditosFiscalesConyugeLabel2.Text = "creditos Fiscales Conyuge:"
        '
        'ProcentajeImpuesto1TextBox
        '
        Me.ProcentajeImpuesto1TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ImpuestosBindingSource, "procentajeImpuesto1", True))
        Me.ProcentajeImpuesto1TextBox.Location = New System.Drawing.Point(109, -25)
        Me.ProcentajeImpuesto1TextBox.Name = "ProcentajeImpuesto1TextBox"
        Me.ProcentajeImpuesto1TextBox.Size = New System.Drawing.Size(100, 20)
        Me.ProcentajeImpuesto1TextBox.TabIndex = 64
        Me.ProcentajeImpuesto1TextBox.Visible = False
        '
        'ProcentajeImpuesto2TextBox
        '
        Me.ProcentajeImpuesto2TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ImpuestosBindingSource, "procentajeImpuesto2", True))
        Me.ProcentajeImpuesto2TextBox.Location = New System.Drawing.Point(265, -53)
        Me.ProcentajeImpuesto2TextBox.Name = "ProcentajeImpuesto2TextBox"
        Me.ProcentajeImpuesto2TextBox.Size = New System.Drawing.Size(100, 20)
        Me.ProcentajeImpuesto2TextBox.TabIndex = 66
        Me.ProcentajeImpuesto2TextBox.Visible = False
        '
        'CreditosFiscalesHijoTextBox
        '
        Me.CreditosFiscalesHijoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ImpuestosBindingSource, "creditosFiscalesHijo", True))
        Me.CreditosFiscalesHijoTextBox.Location = New System.Drawing.Point(265, -27)
        Me.CreditosFiscalesHijoTextBox.Name = "CreditosFiscalesHijoTextBox"
        Me.CreditosFiscalesHijoTextBox.Size = New System.Drawing.Size(100, 20)
        Me.CreditosFiscalesHijoTextBox.TabIndex = 68
        Me.CreditosFiscalesHijoTextBox.Visible = False
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 589)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(1084, 22)
        Me.StatusStrip1.TabIndex = 104
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(39, 17)
        Me.ToolStripStatusLabel1.Text = "Status"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtAños)
        Me.GroupBox1.Controls.Add(Me.CodDeduccionTextBox)
        Me.GroupBox1.Controls.Add(Me.Button5)
        Me.GroupBox1.Controls.Add(FechaPlanMensualLabel)
        Me.GroupBox1.Controls.Add(CodCedulaAjusteLabel)
        Me.GroupBox1.Controls.Add(Me.CodCedulaAjusteTextBox)
        Me.GroupBox1.Controls.Add(TOTALAJUSTARLabel)
        Me.GroupBox1.Controls.Add(Me.TOTALAJUSTARTextBox)
        Me.GroupBox1.Controls.Add(SALARIO_UNICOLabel)
        Me.GroupBox1.Controls.Add(Me.SALARIO_UNICOTextBox)
        Me.GroupBox1.Controls.Add(CodDeduccionLabel)
        Me.GroupBox1.Controls.Add(SumaTotalLabel)
        Me.GroupBox1.Controls.Add(Me.SumaTotalTextBox)
        Me.GroupBox1.Controls.Add(ProcentajeImpuesto2Label)
        Me.GroupBox1.Controls.Add(NoCEDULALabel)
        Me.GroupBox1.Controls.Add(Me.NoCEDULATextBox)
        Me.GroupBox1.Controls.Add(Me.ProcentajeImpuesto2TextBox)
        Me.GroupBox1.Controls.Add(NOMBRE_COMPLETOLabel)
        Me.GroupBox1.Controls.Add(Me.NOMBRE_COMPLETOTextBox)
        Me.GroupBox1.Controls.Add(CODPUESTOLabel)
        Me.GroupBox1.Controls.Add(Me.CODPUESTOTextBox)
        Me.GroupBox1.Controls.Add(Me.CreditosFiscalesHijoTextBox)
        Me.GroupBox1.Controls.Add(FECHA_INGRESOLabel)
        Me.GroupBox1.Controls.Add(Me.FECHA_INGRESODateTimePicker)
        Me.GroupBox1.Controls.Add(AÑOSSERVLabel)
        Me.GroupBox1.Controls.Add(N__CONYUGESLabel)
        Me.GroupBox1.Controls.Add(Me.N__CONYUGESCheckBox)
        Me.GroupBox1.Controls.Add(N__HIJOSLabel)
        Me.GroupBox1.Controls.Add(Me.N__HIJOSTextBox)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 52)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(359, 350)
        Me.GroupBox1.TabIndex = 106
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "DatosEmpleado"
        '
        'txtAños
        '
        Me.txtAños.Location = New System.Drawing.Point(148, 135)
        Me.txtAños.Name = "txtAños"
        Me.txtAños.ReadOnly = True
        Me.txtAños.Size = New System.Drawing.Size(200, 20)
        Me.txtAños.TabIndex = 133
        '
        'CodDeduccionTextBox
        '
        Me.CodDeduccionTextBox.Location = New System.Drawing.Point(148, 213)
        Me.CodDeduccionTextBox.Name = "CodDeduccionTextBox"
        Me.CodDeduccionTextBox.ReadOnly = True
        Me.CodDeduccionTextBox.Size = New System.Drawing.Size(200, 20)
        Me.CodDeduccionTextBox.TabIndex = 132
        '
        'Button5
        '
        Me.Button5.Image = Global.PlanillasProyecto3.My.Resources.Resources.iconoBuscarAutor
        Me.Button5.Location = New System.Drawing.Point(307, 24)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(41, 23)
        Me.Button5.TabIndex = 131
        Me.Button5.Text = "Button5"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'CodCedulaAjusteTextBox
        '
        Me.CodCedulaAjusteTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.AjustesBindingSource, "CodCedulaAjuste", True))
        Me.CodCedulaAjusteTextBox.Location = New System.Drawing.Point(146, 286)
        Me.CodCedulaAjusteTextBox.Name = "CodCedulaAjusteTextBox"
        Me.CodCedulaAjusteTextBox.ReadOnly = True
        Me.CodCedulaAjusteTextBox.Size = New System.Drawing.Size(202, 20)
        Me.CodCedulaAjusteTextBox.TabIndex = 128
        '
        'TOTALAJUSTARTextBox
        '
        Me.TOTALAJUSTARTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.AjustesBindingSource, "TOTALAJUSTAR", True))
        Me.TOTALAJUSTARTextBox.Location = New System.Drawing.Point(146, 312)
        Me.TOTALAJUSTARTextBox.Name = "TOTALAJUSTARTextBox"
        Me.TOTALAJUSTARTextBox.ReadOnly = True
        Me.TOTALAJUSTARTextBox.Size = New System.Drawing.Size(202, 20)
        Me.TOTALAJUSTARTextBox.TabIndex = 130
        '
        'SALARIO_UNICOTextBox
        '
        Me.SALARIO_UNICOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PuestosBindingSource, "SALARIO UNICO", True))
        Me.SALARIO_UNICOTextBox.Location = New System.Drawing.Point(146, 263)
        Me.SALARIO_UNICOTextBox.Name = "SALARIO_UNICOTextBox"
        Me.SALARIO_UNICOTextBox.ReadOnly = True
        Me.SALARIO_UNICOTextBox.Size = New System.Drawing.Size(202, 20)
        Me.SALARIO_UNICOTextBox.TabIndex = 126
        '
        'SumaTotalTextBox
        '
        Me.SumaTotalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DeduccionesBindingSource, "SumaTotal", True))
        Me.SumaTotalTextBox.Location = New System.Drawing.Point(148, 239)
        Me.SumaTotalTextBox.Name = "SumaTotalTextBox"
        Me.SumaTotalTextBox.ReadOnly = True
        Me.SumaTotalTextBox.Size = New System.Drawing.Size(200, 20)
        Me.SumaTotalTextBox.TabIndex = 120
        '
        'NoCEDULATextBox
        '
        Me.NoCEDULATextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "NoCEDULA", True))
        Me.NoCEDULATextBox.Location = New System.Drawing.Point(150, 26)
        Me.NoCEDULATextBox.Name = "NoCEDULATextBox"
        Me.NoCEDULATextBox.ReadOnly = True
        Me.NoCEDULATextBox.Size = New System.Drawing.Size(151, 20)
        Me.NoCEDULATextBox.TabIndex = 104
        '
        'NOMBRE_COMPLETOTextBox
        '
        Me.NOMBRE_COMPLETOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "NOMBRE COMPLETO", True))
        Me.NOMBRE_COMPLETOTextBox.Location = New System.Drawing.Point(148, 57)
        Me.NOMBRE_COMPLETOTextBox.Name = "NOMBRE_COMPLETOTextBox"
        Me.NOMBRE_COMPLETOTextBox.ReadOnly = True
        Me.NOMBRE_COMPLETOTextBox.Size = New System.Drawing.Size(200, 20)
        Me.NOMBRE_COMPLETOTextBox.TabIndex = 106
        '
        'CODPUESTOTextBox
        '
        Me.CODPUESTOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "CODPUESTO", True))
        Me.CODPUESTOTextBox.Location = New System.Drawing.Point(148, 83)
        Me.CODPUESTOTextBox.Name = "CODPUESTOTextBox"
        Me.CODPUESTOTextBox.ReadOnly = True
        Me.CODPUESTOTextBox.Size = New System.Drawing.Size(200, 20)
        Me.CODPUESTOTextBox.TabIndex = 108
        '
        'FECHA_INGRESODateTimePicker
        '
        Me.FECHA_INGRESODateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.DatosPersonalesBindingSource, "FECHA INGRESO", True))
        Me.FECHA_INGRESODateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FECHA_INGRESODateTimePicker.Location = New System.Drawing.Point(148, 109)
        Me.FECHA_INGRESODateTimePicker.Name = "FECHA_INGRESODateTimePicker"
        Me.FECHA_INGRESODateTimePicker.Size = New System.Drawing.Size(200, 20)
        Me.FECHA_INGRESODateTimePicker.TabIndex = 110
        '
        'N__CONYUGESCheckBox
        '
        Me.N__CONYUGESCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.DatosPersonalesBindingSource, "N° CONYUGES", True))
        Me.N__CONYUGESCheckBox.Enabled = False
        Me.N__CONYUGESCheckBox.Location = New System.Drawing.Point(148, 161)
        Me.N__CONYUGESCheckBox.Name = "N__CONYUGESCheckBox"
        Me.N__CONYUGESCheckBox.Size = New System.Drawing.Size(200, 24)
        Me.N__CONYUGESCheckBox.TabIndex = 114
        Me.N__CONYUGESCheckBox.Text = "Casado"
        Me.N__CONYUGESCheckBox.UseVisualStyleBackColor = True
        '
        'N__HIJOSTextBox
        '
        Me.N__HIJOSTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "N° HIJOS", True))
        Me.N__HIJOSTextBox.Location = New System.Drawing.Point(148, 187)
        Me.N__HIJOSTextBox.Name = "N__HIJOSTextBox"
        Me.N__HIJOSTextBox.ReadOnly = True
        Me.N__HIJOSTextBox.Size = New System.Drawing.Size(200, 20)
        Me.N__HIJOSTextBox.TabIndex = 116
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.txtImpuestoSobreSalario)
        Me.GroupBox2.Controls.Add(Label1)
        Me.GroupBox2.Controls.Add(Me.FechaPlanMensualDateTimePicker)
        Me.GroupBox2.Controls.Add(__ANUALLabel)
        Me.GroupBox2.Controls.Add(Me.__ANUALTextBox)
        Me.GroupBox2.Controls.Add(ANUALIDADLabel)
        Me.GroupBox2.Controls.Add(Me.ANUALIDADTextBox)
        Me.GroupBox2.Controls.Add(SALARIO_DIARIOLabel)
        Me.GroupBox2.Controls.Add(Me.SALARIO_DIARIOTextBox)
        Me.GroupBox2.Controls.Add(AsientoLabel)
        Me.GroupBox2.Controls.Add(Me.AsientoTextBox)
        Me.GroupBox2.Controls.Add(ProcentajeImpuesto1Label)
        Me.GroupBox2.Controls.Add(IMP_RENTALabel)
        Me.GroupBox2.Controls.Add(Me.ProcentajeImpuesto1TextBox)
        Me.GroupBox2.Controls.Add(Me.IMP_RENTATextBox)
        Me.GroupBox2.Controls.Add(CCSSLabel)
        Me.GroupBox2.Controls.Add(Me.CCSSTextBox)
        Me.GroupBox2.Location = New System.Drawing.Point(383, 52)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(412, 243)
        Me.GroupBox2.TabIndex = 107
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "CalculosLaborales"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(33, 133)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(134, 13)
        Me.Label2.TabIndex = 132
        Me.Label2.Text = "Año Impuesto sobre salario"
        '
        'txtImpuestoSobreSalario
        '
        Me.txtImpuestoSobreSalario.Location = New System.Drawing.Point(186, 130)
        Me.txtImpuestoSobreSalario.Name = "txtImpuestoSobreSalario"
        Me.txtImpuestoSobreSalario.ReadOnly = True
        Me.txtImpuestoSobreSalario.Size = New System.Drawing.Size(200, 20)
        Me.txtImpuestoSobreSalario.TabIndex = 133
        '
        'FechaPlanMensualDateTimePicker
        '
        Me.FechaPlanMensualDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.PlanillaMensualBindingSource, "FechaPlanMensual", True))
        Me.FechaPlanMensualDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.FechaPlanMensualDateTimePicker.Location = New System.Drawing.Point(186, 23)
        Me.FechaPlanMensualDateTimePicker.Name = "FechaPlanMensualDateTimePicker"
        Me.FechaPlanMensualDateTimePicker.Size = New System.Drawing.Size(200, 20)
        Me.FechaPlanMensualDateTimePicker.TabIndex = 1
        '
        '__ANUALTextBox
        '
        Me.__ANUALTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "% ANUAL", True))
        Me.__ANUALTextBox.Location = New System.Drawing.Point(186, 53)
        Me.__ANUALTextBox.Name = "__ANUALTextBox"
        Me.__ANUALTextBox.ReadOnly = True
        Me.__ANUALTextBox.Size = New System.Drawing.Size(200, 20)
        Me.__ANUALTextBox.TabIndex = 7
        '
        'ANUALIDADTextBox
        '
        Me.ANUALIDADTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "ANUALIDAD", True))
        Me.ANUALIDADTextBox.Location = New System.Drawing.Point(186, 79)
        Me.ANUALIDADTextBox.Name = "ANUALIDADTextBox"
        Me.ANUALIDADTextBox.ReadOnly = True
        Me.ANUALIDADTextBox.Size = New System.Drawing.Size(200, 20)
        Me.ANUALIDADTextBox.TabIndex = 9
        '
        'SALARIO_DIARIOTextBox
        '
        Me.SALARIO_DIARIOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "SALARIO DIARIO", True))
        Me.SALARIO_DIARIOTextBox.Location = New System.Drawing.Point(186, 105)
        Me.SALARIO_DIARIOTextBox.Name = "SALARIO_DIARIOTextBox"
        Me.SALARIO_DIARIOTextBox.ReadOnly = True
        Me.SALARIO_DIARIOTextBox.Size = New System.Drawing.Size(200, 20)
        Me.SALARIO_DIARIOTextBox.TabIndex = 13
        '
        'AsientoTextBox
        '
        Me.AsientoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "Asiento", True))
        Me.AsientoTextBox.Location = New System.Drawing.Point(186, 209)
        Me.AsientoTextBox.Name = "AsientoTextBox"
        Me.AsientoTextBox.Size = New System.Drawing.Size(200, 20)
        Me.AsientoTextBox.TabIndex = 33
        '
        'IMP_RENTATextBox
        '
        Me.IMP_RENTATextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "IMP RENTA", True))
        Me.IMP_RENTATextBox.Location = New System.Drawing.Point(188, 156)
        Me.IMP_RENTATextBox.Name = "IMP_RENTATextBox"
        Me.IMP_RENTATextBox.ReadOnly = True
        Me.IMP_RENTATextBox.Size = New System.Drawing.Size(200, 20)
        Me.IMP_RENTATextBox.TabIndex = 35
        '
        'CCSSTextBox
        '
        Me.CCSSTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "CCSS", True))
        Me.CCSSTextBox.Location = New System.Drawing.Point(188, 183)
        Me.CCSSTextBox.Name = "CCSSTextBox"
        Me.CCSSTextBox.ReadOnly = True
        Me.CCSSTextBox.Size = New System.Drawing.Size(200, 20)
        Me.CCSSTextBox.TabIndex = 45
        '
        'CESANTIATextBox
        '
        Me.CESANTIATextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "CESANTIA", True))
        Me.CESANTIATextBox.Location = New System.Drawing.Point(167, 37)
        Me.CESANTIATextBox.Name = "CESANTIATextBox"
        Me.CESANTIATextBox.ReadOnly = True
        Me.CESANTIATextBox.Size = New System.Drawing.Size(200, 20)
        Me.CESANTIATextBox.TabIndex = 39
        '
        'LIQUIDO_A_PAGARTextBox
        '
        Me.LIQUIDO_A_PAGARTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "LIQUIDO A PAGAR", True))
        Me.LIQUIDO_A_PAGARTextBox.Location = New System.Drawing.Point(167, 137)
        Me.LIQUIDO_A_PAGARTextBox.Name = "LIQUIDO_A_PAGARTextBox"
        Me.LIQUIDO_A_PAGARTextBox.ReadOnly = True
        Me.LIQUIDO_A_PAGARTextBox.Size = New System.Drawing.Size(200, 20)
        Me.LIQUIDO_A_PAGARTextBox.TabIndex = 15
        '
        'EXTRASTextBox
        '
        Me.EXTRASTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "EXTRAS", True))
        Me.EXTRASTextBox.Location = New System.Drawing.Point(167, 111)
        Me.EXTRASTextBox.Name = "EXTRASTextBox"
        Me.EXTRASTextBox.ReadOnly = True
        Me.EXTRASTextBox.Size = New System.Drawing.Size(200, 20)
        Me.EXTRASTextBox.TabIndex = 37
        '
        'SUBSIDIOTextBox
        '
        Me.SUBSIDIOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "SUBSIDIO", True))
        Me.SUBSIDIOTextBox.Location = New System.Drawing.Point(159, 129)
        Me.SUBSIDIOTextBox.Name = "SUBSIDIOTextBox"
        Me.SUBSIDIOTextBox.ReadOnly = True
        Me.SUBSIDIOTextBox.Size = New System.Drawing.Size(200, 20)
        Me.SUBSIDIOTextBox.TabIndex = 41
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.InstitucionIncapacidadTextBox)
        Me.GroupBox3.Controls.Add(TOTAL_DIAS_INCAPLabel)
        Me.GroupBox3.Controls.Add(Me.TOTAL_DIAS_INCAPTextBox)
        Me.GroupBox3.Controls.Add(FechaINCAPINILabel)
        Me.GroupBox3.Controls.Add(Me.FechaINCAPINIDateTimePicker)
        Me.GroupBox3.Controls.Add(FechaINCAPFINLabel)
        Me.GroupBox3.Controls.Add(Me.FechaINCAPFINDateTimePicker)
        Me.GroupBox3.Controls.Add(InstitucionIncapacidadLabel)
        Me.GroupBox3.Controls.Add(Me.SUBSIDIOTextBox)
        Me.GroupBox3.Controls.Add(SUBSIDIOLabel)
        Me.GroupBox3.Location = New System.Drawing.Point(6, 408)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(365, 180)
        Me.GroupBox3.TabIndex = 108
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Incapacidad"
        '
        'InstitucionIncapacidadTextBox
        '
        Me.InstitucionIncapacidadTextBox.FormattingEnabled = True
        Me.InstitucionIncapacidadTextBox.Items.AddRange(New Object() {"INS", "CCSS"})
        Me.InstitucionIncapacidadTextBox.Location = New System.Drawing.Point(159, 28)
        Me.InstitucionIncapacidadTextBox.Name = "InstitucionIncapacidadTextBox"
        Me.InstitucionIncapacidadTextBox.Size = New System.Drawing.Size(200, 21)
        Me.InstitucionIncapacidadTextBox.TabIndex = 42
        '
        'TOTAL_DIAS_INCAPTextBox
        '
        Me.TOTAL_DIAS_INCAPTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "TOTAL DIAS INCAP", True))
        Me.TOTAL_DIAS_INCAPTextBox.Location = New System.Drawing.Point(159, 107)
        Me.TOTAL_DIAS_INCAPTextBox.Name = "TOTAL_DIAS_INCAPTextBox"
        Me.TOTAL_DIAS_INCAPTextBox.ReadOnly = True
        Me.TOTAL_DIAS_INCAPTextBox.Size = New System.Drawing.Size(200, 20)
        Me.TOTAL_DIAS_INCAPTextBox.TabIndex = 33
        '
        'FechaINCAPINIDateTimePicker
        '
        Me.FechaINCAPINIDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.PlanillaMensualBindingSource, "FechaINCAPINI", True))
        Me.FechaINCAPINIDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.FechaINCAPINIDateTimePicker.Location = New System.Drawing.Point(159, 56)
        Me.FechaINCAPINIDateTimePicker.Name = "FechaINCAPINIDateTimePicker"
        Me.FechaINCAPINIDateTimePicker.Size = New System.Drawing.Size(200, 20)
        Me.FechaINCAPINIDateTimePicker.TabIndex = 35
        '
        'FechaINCAPFINDateTimePicker
        '
        Me.FechaINCAPFINDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.PlanillaMensualBindingSource, "FechaINCAPFIN", True))
        Me.FechaINCAPFINDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.FechaINCAPFINDateTimePicker.Location = New System.Drawing.Point(161, 82)
        Me.FechaINCAPFINDateTimePicker.Name = "FechaINCAPFINDateTimePicker"
        Me.FechaINCAPFINDateTimePicker.Size = New System.Drawing.Size(200, 20)
        Me.FechaINCAPFINDateTimePicker.TabIndex = 37
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.DIAS_TRABAJTextBox)
        Me.GroupBox4.Controls.Add(Me.HORAS_EXTRAS_DOBLESTextBox)
        Me.GroupBox4.Controls.Add(Me.HORAS_EXTRAS_REGULARTextBox)
        Me.GroupBox4.Controls.Add(Me.EXTRASTextBox)
        Me.GroupBox4.Controls.Add(EXTRASLabel)
        Me.GroupBox4.Controls.Add(HORAS_EXTRAS_DOBLESLabel)
        Me.GroupBox4.Controls.Add(HORAS_EXTRAS_REGULARLabel)
        Me.GroupBox4.Controls.Add(DIAS_TRABAJLabel)
        Me.GroupBox4.Controls.Add(Me.LIQUIDO_A_PAGARTextBox)
        Me.GroupBox4.Controls.Add(LIQUIDO_A_PAGARLabel)
        Me.GroupBox4.Controls.Add(Me.CESANTIATextBox)
        Me.GroupBox4.Controls.Add(CESANTIALabel)
        Me.GroupBox4.Location = New System.Drawing.Point(382, 301)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(413, 180)
        Me.GroupBox4.TabIndex = 46
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "DatoLaborales"
        '
        'DIAS_TRABAJTextBox
        '
        Me.DIAS_TRABAJTextBox.FormattingEnabled = True
        Me.DIAS_TRABAJTextBox.Items.AddRange(New Object() {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"})
        Me.DIAS_TRABAJTextBox.Location = New System.Drawing.Point(167, 10)
        Me.DIAS_TRABAJTextBox.Name = "DIAS_TRABAJTextBox"
        Me.DIAS_TRABAJTextBox.Size = New System.Drawing.Size(200, 21)
        Me.DIAS_TRABAJTextBox.TabIndex = 43
        '
        'HORAS_EXTRAS_DOBLESTextBox
        '
        Me.HORAS_EXTRAS_DOBLESTextBox.FormattingEnabled = True
        Me.HORAS_EXTRAS_DOBLESTextBox.Items.AddRange(New Object() {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16"})
        Me.HORAS_EXTRAS_DOBLESTextBox.Location = New System.Drawing.Point(167, 84)
        Me.HORAS_EXTRAS_DOBLESTextBox.Name = "HORAS_EXTRAS_DOBLESTextBox"
        Me.HORAS_EXTRAS_DOBLESTextBox.Size = New System.Drawing.Size(200, 21)
        Me.HORAS_EXTRAS_DOBLESTextBox.TabIndex = 42
        '
        'HORAS_EXTRAS_REGULARTextBox
        '
        Me.HORAS_EXTRAS_REGULARTextBox.FormattingEnabled = True
        Me.HORAS_EXTRAS_REGULARTextBox.Items.AddRange(New Object() {"0", "1", "2", "3", "4", "5", "6", "7", "8"})
        Me.HORAS_EXTRAS_REGULARTextBox.Location = New System.Drawing.Point(167, 59)
        Me.HORAS_EXTRAS_REGULARTextBox.Name = "HORAS_EXTRAS_REGULARTextBox"
        Me.HORAS_EXTRAS_REGULARTextBox.Size = New System.Drawing.Size(200, 21)
        Me.HORAS_EXTRAS_REGULARTextBox.TabIndex = 41
        '
        'Button1
        '
        Me.Button1.Image = Global.PlanillasProyecto3.My.Resources.Resources.guardar_documento_icono_7840_48
        Me.Button1.Location = New System.Drawing.Point(417, 497)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(84, 68)
        Me.Button1.TabIndex = 113
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Image = Global.PlanillasProyecto3.My.Resources.Resources.icono_retroceso
        Me.Button4.Location = New System.Drawing.Point(599, 497)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 71)
        Me.Button4.TabIndex = 116
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Image = Global.PlanillasProyecto3.My.Resources.Resources.iconoBuscarAutor
        Me.Button2.Location = New System.Drawing.Point(512, 497)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(81, 68)
        Me.Button2.TabIndex = 114
        Me.Button2.UseVisualStyleBackColor = True
        '
        'CodImpuestoTextBox
        '
        Me.CodImpuestoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ImpuestosBindingSource, "codImpuesto", True))
        Me.CodImpuestoTextBox.Location = New System.Drawing.Point(984, 102)
        Me.CodImpuestoTextBox.Name = "CodImpuestoTextBox"
        Me.CodImpuestoTextBox.Size = New System.Drawing.Size(100, 20)
        Me.CodImpuestoTextBox.TabIndex = 117
        '
        'ProcentajeImpuesto1TextBox1
        '
        Me.ProcentajeImpuesto1TextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ImpuestosBindingSource, "procentajeImpuesto1", True))
        Me.ProcentajeImpuesto1TextBox1.Location = New System.Drawing.Point(984, 128)
        Me.ProcentajeImpuesto1TextBox1.Name = "ProcentajeImpuesto1TextBox1"
        Me.ProcentajeImpuesto1TextBox1.Size = New System.Drawing.Size(100, 20)
        Me.ProcentajeImpuesto1TextBox1.TabIndex = 119
        '
        'ProcentajeImpuesto2TextBox1
        '
        Me.ProcentajeImpuesto2TextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ImpuestosBindingSource, "procentajeImpuesto2", True))
        Me.ProcentajeImpuesto2TextBox1.Location = New System.Drawing.Point(984, 154)
        Me.ProcentajeImpuesto2TextBox1.Name = "ProcentajeImpuesto2TextBox1"
        Me.ProcentajeImpuesto2TextBox1.Size = New System.Drawing.Size(100, 20)
        Me.ProcentajeImpuesto2TextBox1.TabIndex = 121
        '
        'CreditosFiscalesHijoTextBox1
        '
        Me.CreditosFiscalesHijoTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ImpuestosBindingSource, "creditosFiscalesHijo", True))
        Me.CreditosFiscalesHijoTextBox1.Location = New System.Drawing.Point(984, 180)
        Me.CreditosFiscalesHijoTextBox1.Name = "CreditosFiscalesHijoTextBox1"
        Me.CreditosFiscalesHijoTextBox1.Size = New System.Drawing.Size(100, 20)
        Me.CreditosFiscalesHijoTextBox1.TabIndex = 123
        '
        'CreditosFiscalesConyugeTextBox1
        '
        Me.CreditosFiscalesConyugeTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ImpuestosBindingSource, "creditosFiscalesConyuge", True))
        Me.CreditosFiscalesConyugeTextBox1.Location = New System.Drawing.Point(984, 206)
        Me.CreditosFiscalesConyugeTextBox1.Name = "CreditosFiscalesConyugeTextBox1"
        Me.CreditosFiscalesConyugeTextBox1.Size = New System.Drawing.Size(100, 20)
        Me.CreditosFiscalesConyugeTextBox1.TabIndex = 125
        '
        'CreditosFiscalesConyugeTextBox
        '
        Me.CreditosFiscalesConyugeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ImpuestosBindingSource, "creditosFiscalesConyuge", True))
        Me.CreditosFiscalesConyugeTextBox.Location = New System.Drawing.Point(240, 29)
        Me.CreditosFiscalesConyugeTextBox.Name = "CreditosFiscalesConyugeTextBox"
        Me.CreditosFiscalesConyugeTextBox.Size = New System.Drawing.Size(100, 20)
        Me.CreditosFiscalesConyugeTextBox.TabIndex = 70
        Me.CreditosFiscalesConyugeTextBox.Visible = False
        '
        'ArchivoToolStripMenuItem
        '
        Me.ArchivoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RegresarToolStripMenuItem})
        Me.ArchivoToolStripMenuItem.Name = "ArchivoToolStripMenuItem"
        Me.ArchivoToolStripMenuItem.Size = New System.Drawing.Size(60, 20)
        Me.ArchivoToolStripMenuItem.Text = "&Archivo"
        '
        'RegresarToolStripMenuItem
        '
        Me.RegresarToolStripMenuItem.Name = "RegresarToolStripMenuItem"
        Me.RegresarToolStripMenuItem.Size = New System.Drawing.Size(119, 22)
        Me.RegresarToolStripMenuItem.Text = "&Regresar"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ArchivoToolStripMenuItem, Me.AccionesToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1084, 24)
        Me.MenuStrip1.TabIndex = 105
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'AccionesToolStripMenuItem
        '
        Me.AccionesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GuardarToolStripMenuItem, Me.BuscarToolStripMenuItem})
        Me.AccionesToolStripMenuItem.Name = "AccionesToolStripMenuItem"
        Me.AccionesToolStripMenuItem.Size = New System.Drawing.Size(67, 20)
        Me.AccionesToolStripMenuItem.Text = "&Acciones"
        '
        'GuardarToolStripMenuItem
        '
        Me.GuardarToolStripMenuItem.Name = "GuardarToolStripMenuItem"
        Me.GuardarToolStripMenuItem.Size = New System.Drawing.Size(116, 22)
        Me.GuardarToolStripMenuItem.Text = "&Guardar"
        '
        'BuscarToolStripMenuItem
        '
        Me.BuscarToolStripMenuItem.Name = "BuscarToolStripMenuItem"
        Me.BuscarToolStripMenuItem.Size = New System.Drawing.Size(116, 22)
        Me.BuscarToolStripMenuItem.Text = "&Buscar"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton1.Image = Global.PlanillasProyecto3.My.Resources.Resources.guardar_documento_icono_7840_48
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton1.Text = "ToolStripButton1"
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton2.Image = Global.PlanillasProyecto3.My.Resources.Resources.iconoBuscarAutor
        Me.ToolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton2.Text = "ToolStripButton2"
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.ToolStripButton2, Me.ToolStripButton4})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 24)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(1084, 25)
        Me.ToolStrip1.TabIndex = 103
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripButton4
        '
        Me.ToolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton4.Image = Global.PlanillasProyecto3.My.Resources.Resources.icono_retroceso
        Me.ToolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton4.Name = "ToolStripButton4"
        Me.ToolStripButton4.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton4.Text = "ToolStripButton4"
        '
        'CodImpuestoTextBox1
        '
        Me.CodImpuestoTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ImpuestosBindingSource, "codImpuesto", True))
        Me.CodImpuestoTextBox1.Location = New System.Drawing.Point(959, 349)
        Me.CodImpuestoTextBox1.Name = "CodImpuestoTextBox1"
        Me.CodImpuestoTextBox1.Size = New System.Drawing.Size(100, 20)
        Me.CodImpuestoTextBox1.TabIndex = 126
        '
        'ProcentajeImpuesto1TextBox2
        '
        Me.ProcentajeImpuesto1TextBox2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ImpuestosBindingSource, "procentajeImpuesto1", True))
        Me.ProcentajeImpuesto1TextBox2.Location = New System.Drawing.Point(959, 375)
        Me.ProcentajeImpuesto1TextBox2.Name = "ProcentajeImpuesto1TextBox2"
        Me.ProcentajeImpuesto1TextBox2.Size = New System.Drawing.Size(100, 20)
        Me.ProcentajeImpuesto1TextBox2.TabIndex = 128
        '
        'ProcentajeImpuesto2TextBox2
        '
        Me.ProcentajeImpuesto2TextBox2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ImpuestosBindingSource, "procentajeImpuesto2", True))
        Me.ProcentajeImpuesto2TextBox2.Location = New System.Drawing.Point(959, 401)
        Me.ProcentajeImpuesto2TextBox2.Name = "ProcentajeImpuesto2TextBox2"
        Me.ProcentajeImpuesto2TextBox2.Size = New System.Drawing.Size(100, 20)
        Me.ProcentajeImpuesto2TextBox2.TabIndex = 130
        '
        'CreditosFiscalesHijoTextBox2
        '
        Me.CreditosFiscalesHijoTextBox2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ImpuestosBindingSource, "creditosFiscalesHijo", True))
        Me.CreditosFiscalesHijoTextBox2.Location = New System.Drawing.Point(959, 427)
        Me.CreditosFiscalesHijoTextBox2.Name = "CreditosFiscalesHijoTextBox2"
        Me.CreditosFiscalesHijoTextBox2.Size = New System.Drawing.Size(100, 20)
        Me.CreditosFiscalesHijoTextBox2.TabIndex = 132
        '
        'CreditosFiscalesConyugeTextBox2
        '
        Me.CreditosFiscalesConyugeTextBox2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ImpuestosBindingSource, "creditosFiscalesConyuge", True))
        Me.CreditosFiscalesConyugeTextBox2.Location = New System.Drawing.Point(959, 453)
        Me.CreditosFiscalesConyugeTextBox2.Name = "CreditosFiscalesConyugeTextBox2"
        Me.CreditosFiscalesConyugeTextBox2.Size = New System.Drawing.Size(100, 20)
        Me.CreditosFiscalesConyugeTextBox2.TabIndex = 134
        '
        'PlanillaBisemanalDataGridView
        '
        Me.PlanillaBisemanalDataGridView.AutoGenerateColumns = False
        Me.PlanillaBisemanalDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.PlanillaBisemanalDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9, Me.DataGridViewTextBoxColumn10, Me.DataGridViewTextBoxColumn11, Me.DataGridViewTextBoxColumn12, Me.DataGridViewTextBoxColumn13, Me.DataGridViewTextBoxColumn14, Me.DataGridViewTextBoxColumn15, Me.DataGridViewTextBoxColumn16, Me.DataGridViewTextBoxColumn17, Me.DataGridViewTextBoxColumn18, Me.DataGridViewTextBoxColumn19, Me.DataGridViewTextBoxColumn20, Me.DataGridViewTextBoxColumn21, Me.DataGridViewTextBoxColumn22, Me.DataGridViewTextBoxColumn23})
        Me.PlanillaBisemanalDataGridView.DataSource = Me.PlanillaBisemanalBindingSource
        Me.PlanillaBisemanalDataGridView.Location = New System.Drawing.Point(801, 86)
        Me.PlanillaBisemanalDataGridView.Name = "PlanillaBisemanalDataGridView"
        Me.PlanillaBisemanalDataGridView.Size = New System.Drawing.Size(258, 173)
        Me.PlanillaBisemanalDataGridView.TabIndex = 134
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "FechaPlanMensual"
        Me.DataGridViewTextBoxColumn1.HeaderText = "FechaPlanMensual"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "NoCEDULA"
        Me.DataGridViewTextBoxColumn2.HeaderText = "NoCEDULA"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Nombre"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Nombre"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "% ANUAL"
        Me.DataGridViewTextBoxColumn4.HeaderText = "% ANUAL"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "ANUALIDAD"
        Me.DataGridViewTextBoxColumn5.HeaderText = "ANUALIDAD"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "SALARIO BRUTO"
        Me.DataGridViewTextBoxColumn6.HeaderText = "SALARIO BRUTO"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "SALARIO DIARIO"
        Me.DataGridViewTextBoxColumn7.HeaderText = "SALARIO DIARIO"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "LIQUIDO A PAGAR"
        Me.DataGridViewTextBoxColumn8.HeaderText = "LIQUIDO A PAGAR"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "DIAS TRABAJ"
        Me.DataGridViewTextBoxColumn9.HeaderText = "DIAS TRABAJ"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "DiasIncap"
        Me.DataGridViewTextBoxColumn10.HeaderText = "DiasIncap"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.DataPropertyName = "InstitucionIncapacidad"
        Me.DataGridViewTextBoxColumn11.HeaderText = "InstitucionIncapacidad"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.DataPropertyName = "HORAS EXTRAS REGULAR"
        Me.DataGridViewTextBoxColumn12.HeaderText = "HORAS EXTRAS REGULAR"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.DataPropertyName = "HORAS EXTRAS DOBLES"
        Me.DataGridViewTextBoxColumn13.HeaderText = "HORAS EXTRAS DOBLES"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.DataPropertyName = "TOTAL DIAS INCAP"
        Me.DataGridViewTextBoxColumn14.HeaderText = "TOTAL DIAS INCAP"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.DataPropertyName = "FechaINCAPINI"
        Me.DataGridViewTextBoxColumn15.HeaderText = "FechaINCAPINI"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.DataPropertyName = "FechaINCAPFIN"
        Me.DataGridViewTextBoxColumn16.HeaderText = "FechaINCAPFIN"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.DataPropertyName = "Asiento"
        Me.DataGridViewTextBoxColumn17.HeaderText = "Asiento"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.DataPropertyName = "IMP RENTA"
        Me.DataGridViewTextBoxColumn18.HeaderText = "IMP RENTA"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.DataPropertyName = "EXTRAS"
        Me.DataGridViewTextBoxColumn19.HeaderText = "EXTRAS"
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        '
        'DataGridViewTextBoxColumn20
        '
        Me.DataGridViewTextBoxColumn20.DataPropertyName = "CESANTIA"
        Me.DataGridViewTextBoxColumn20.HeaderText = "CESANTIA"
        Me.DataGridViewTextBoxColumn20.Name = "DataGridViewTextBoxColumn20"
        '
        'DataGridViewTextBoxColumn21
        '
        Me.DataGridViewTextBoxColumn21.DataPropertyName = "SUBSIDIO"
        Me.DataGridViewTextBoxColumn21.HeaderText = "SUBSIDIO"
        Me.DataGridViewTextBoxColumn21.Name = "DataGridViewTextBoxColumn21"
        '
        'DataGridViewTextBoxColumn22
        '
        Me.DataGridViewTextBoxColumn22.DataPropertyName = "CCSS"
        Me.DataGridViewTextBoxColumn22.HeaderText = "CCSS"
        Me.DataGridViewTextBoxColumn22.Name = "DataGridViewTextBoxColumn22"
        '
        'DataGridViewTextBoxColumn23
        '
        Me.DataGridViewTextBoxColumn23.DataPropertyName = "codDeduc"
        Me.DataGridViewTextBoxColumn23.HeaderText = "codDeduc"
        Me.DataGridViewTextBoxColumn23.Name = "DataGridViewTextBoxColumn23"
        '
        'PlanillaBisemanalBindingSource
        '
        Me.PlanillaBisemanalBindingSource.DataMember = "PlanillaBisemanal"
        Me.PlanillaBisemanalBindingSource.DataSource = Me.Planilla2DataSet1
        '
        'Planilla2DataSet1
        '
        Me.Planilla2DataSet1.DataSetName = "Planilla2DataSet1"
        Me.Planilla2DataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ImpuestosBindingSource
        '
        Me.ImpuestosBindingSource.DataMember = "Impuestos"
        Me.ImpuestosBindingSource.DataSource = Me.Planilla2DataSet1
        '
        'PlanillaMensualBindingSource
        '
        Me.PlanillaMensualBindingSource.DataMember = "PlanillaMensual"
        Me.PlanillaMensualBindingSource.DataSource = Me.Planilla2DataSet1
        '
        'AjustesBindingSource
        '
        Me.AjustesBindingSource.DataMember = "Ajustes"
        Me.AjustesBindingSource.DataSource = Me.Planilla2DataSet1
        '
        'PuestosBindingSource
        '
        Me.PuestosBindingSource.DataMember = "Puestos"
        Me.PuestosBindingSource.DataSource = Me.Planilla2DataSet1
        '
        'DeduccionesBindingSource
        '
        Me.DeduccionesBindingSource.DataMember = "Deducciones"
        Me.DeduccionesBindingSource.DataSource = Me.Planilla2DataSet1
        '
        'DatosPersonalesBindingSource
        '
        Me.DatosPersonalesBindingSource.DataMember = "DatosPersonales"
        Me.DatosPersonalesBindingSource.DataSource = Me.Planilla2DataSet1
        '
        'DatosPersonalesTableAdapter
        '
        Me.DatosPersonalesTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.AguinaldoTableAdapter = Nothing
        Me.TableAdapterManager.AjustesTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.DatosPersonalesTableAdapter = Me.DatosPersonalesTableAdapter
        Me.TableAdapterManager.DeduccionesTableAdapter = Nothing
        Me.TableAdapterManager.ImpuestosTableAdapter = Nothing
        Me.TableAdapterManager.PlanillaBisemanalTableAdapter = Nothing
        Me.TableAdapterManager.PlanillaMensualTableAdapter = Nothing
        Me.TableAdapterManager.PuestosTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = PlanillasProyecto3.Planilla2DataSet1TableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.UsuarioRegistroTableAdapter = Nothing
        Me.TableAdapterManager.VacacionesTableAdapter = Nothing
        '
        'DeduccionesTableAdapter
        '
        Me.DeduccionesTableAdapter.ClearBeforeFill = True
        '
        'ImpuestosTableAdapter
        '
        Me.ImpuestosTableAdapter.ClearBeforeFill = True
        '
        'PuestosTableAdapter
        '
        Me.PuestosTableAdapter.ClearBeforeFill = True
        '
        'AjustesTableAdapter
        '
        Me.AjustesTableAdapter.ClearBeforeFill = True
        '
        'PlanillaMensualTableAdapter
        '
        Me.PlanillaMensualTableAdapter.ClearBeforeFill = True
        '
        'PlanillaBisemanalTableAdapter
        '
        Me.PlanillaBisemanalTableAdapter.ClearBeforeFill = True
        '
        'PlanillaBisemanalDataGridView1
        '
        Me.PlanillaBisemanalDataGridView1.AutoGenerateColumns = False
        Me.PlanillaBisemanalDataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.PlanillaBisemanalDataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn24, Me.DataGridViewTextBoxColumn25, Me.DataGridViewTextBoxColumn26, Me.DataGridViewTextBoxColumn27, Me.DataGridViewTextBoxColumn28, Me.DataGridViewTextBoxColumn29, Me.DataGridViewTextBoxColumn30, Me.DataGridViewTextBoxColumn31, Me.DataGridViewTextBoxColumn32, Me.DataGridViewTextBoxColumn33, Me.DataGridViewTextBoxColumn34, Me.DataGridViewTextBoxColumn35, Me.DataGridViewTextBoxColumn36, Me.DataGridViewTextBoxColumn37, Me.DataGridViewTextBoxColumn38, Me.DataGridViewTextBoxColumn39, Me.DataGridViewTextBoxColumn40, Me.DataGridViewTextBoxColumn41, Me.DataGridViewTextBoxColumn42, Me.DataGridViewTextBoxColumn43, Me.DataGridViewTextBoxColumn44, Me.DataGridViewTextBoxColumn45, Me.DataGridViewTextBoxColumn46})
        Me.PlanillaBisemanalDataGridView1.DataSource = Me.PlanillaBisemanalBindingSource
        Me.PlanillaBisemanalDataGridView1.Location = New System.Drawing.Point(801, 268)
        Me.PlanillaBisemanalDataGridView1.Name = "PlanillaBisemanalDataGridView1"
        Me.PlanillaBisemanalDataGridView1.Size = New System.Drawing.Size(300, 220)
        Me.PlanillaBisemanalDataGridView1.TabIndex = 134
        '
        'DataGridViewTextBoxColumn24
        '
        Me.DataGridViewTextBoxColumn24.DataPropertyName = "FechaPlanMensual"
        Me.DataGridViewTextBoxColumn24.HeaderText = "FechaPlanMensual"
        Me.DataGridViewTextBoxColumn24.Name = "DataGridViewTextBoxColumn24"
        '
        'DataGridViewTextBoxColumn25
        '
        Me.DataGridViewTextBoxColumn25.DataPropertyName = "NoCEDULA"
        Me.DataGridViewTextBoxColumn25.HeaderText = "NoCEDULA"
        Me.DataGridViewTextBoxColumn25.Name = "DataGridViewTextBoxColumn25"
        '
        'DataGridViewTextBoxColumn26
        '
        Me.DataGridViewTextBoxColumn26.DataPropertyName = "Nombre"
        Me.DataGridViewTextBoxColumn26.HeaderText = "Nombre"
        Me.DataGridViewTextBoxColumn26.Name = "DataGridViewTextBoxColumn26"
        '
        'DataGridViewTextBoxColumn27
        '
        Me.DataGridViewTextBoxColumn27.DataPropertyName = "% ANUAL"
        Me.DataGridViewTextBoxColumn27.HeaderText = "% ANUAL"
        Me.DataGridViewTextBoxColumn27.Name = "DataGridViewTextBoxColumn27"
        '
        'DataGridViewTextBoxColumn28
        '
        Me.DataGridViewTextBoxColumn28.DataPropertyName = "ANUALIDAD"
        Me.DataGridViewTextBoxColumn28.HeaderText = "ANUALIDAD"
        Me.DataGridViewTextBoxColumn28.Name = "DataGridViewTextBoxColumn28"
        '
        'DataGridViewTextBoxColumn29
        '
        Me.DataGridViewTextBoxColumn29.DataPropertyName = "SALARIO BRUTO"
        Me.DataGridViewTextBoxColumn29.HeaderText = "SALARIO BRUTO"
        Me.DataGridViewTextBoxColumn29.Name = "DataGridViewTextBoxColumn29"
        '
        'DataGridViewTextBoxColumn30
        '
        Me.DataGridViewTextBoxColumn30.DataPropertyName = "SALARIO DIARIO"
        Me.DataGridViewTextBoxColumn30.HeaderText = "SALARIO DIARIO"
        Me.DataGridViewTextBoxColumn30.Name = "DataGridViewTextBoxColumn30"
        '
        'DataGridViewTextBoxColumn31
        '
        Me.DataGridViewTextBoxColumn31.DataPropertyName = "LIQUIDO A PAGAR"
        Me.DataGridViewTextBoxColumn31.HeaderText = "LIQUIDO A PAGAR"
        Me.DataGridViewTextBoxColumn31.Name = "DataGridViewTextBoxColumn31"
        '
        'DataGridViewTextBoxColumn32
        '
        Me.DataGridViewTextBoxColumn32.DataPropertyName = "DIAS TRABAJ"
        Me.DataGridViewTextBoxColumn32.HeaderText = "DIAS TRABAJ"
        Me.DataGridViewTextBoxColumn32.Name = "DataGridViewTextBoxColumn32"
        '
        'DataGridViewTextBoxColumn33
        '
        Me.DataGridViewTextBoxColumn33.DataPropertyName = "DiasIncap"
        Me.DataGridViewTextBoxColumn33.HeaderText = "DiasIncap"
        Me.DataGridViewTextBoxColumn33.Name = "DataGridViewTextBoxColumn33"
        '
        'DataGridViewTextBoxColumn34
        '
        Me.DataGridViewTextBoxColumn34.DataPropertyName = "InstitucionIncapacidad"
        Me.DataGridViewTextBoxColumn34.HeaderText = "InstitucionIncapacidad"
        Me.DataGridViewTextBoxColumn34.Name = "DataGridViewTextBoxColumn34"
        '
        'DataGridViewTextBoxColumn35
        '
        Me.DataGridViewTextBoxColumn35.DataPropertyName = "HORAS EXTRAS REGULAR"
        Me.DataGridViewTextBoxColumn35.HeaderText = "HORAS EXTRAS REGULAR"
        Me.DataGridViewTextBoxColumn35.Name = "DataGridViewTextBoxColumn35"
        '
        'DataGridViewTextBoxColumn36
        '
        Me.DataGridViewTextBoxColumn36.DataPropertyName = "HORAS EXTRAS DOBLES"
        Me.DataGridViewTextBoxColumn36.HeaderText = "HORAS EXTRAS DOBLES"
        Me.DataGridViewTextBoxColumn36.Name = "DataGridViewTextBoxColumn36"
        '
        'DataGridViewTextBoxColumn37
        '
        Me.DataGridViewTextBoxColumn37.DataPropertyName = "TOTAL DIAS INCAP"
        Me.DataGridViewTextBoxColumn37.HeaderText = "TOTAL DIAS INCAP"
        Me.DataGridViewTextBoxColumn37.Name = "DataGridViewTextBoxColumn37"
        '
        'DataGridViewTextBoxColumn38
        '
        Me.DataGridViewTextBoxColumn38.DataPropertyName = "FechaINCAPINI"
        Me.DataGridViewTextBoxColumn38.HeaderText = "FechaINCAPINI"
        Me.DataGridViewTextBoxColumn38.Name = "DataGridViewTextBoxColumn38"
        '
        'DataGridViewTextBoxColumn39
        '
        Me.DataGridViewTextBoxColumn39.DataPropertyName = "FechaINCAPFIN"
        Me.DataGridViewTextBoxColumn39.HeaderText = "FechaINCAPFIN"
        Me.DataGridViewTextBoxColumn39.Name = "DataGridViewTextBoxColumn39"
        '
        'DataGridViewTextBoxColumn40
        '
        Me.DataGridViewTextBoxColumn40.DataPropertyName = "Asiento"
        Me.DataGridViewTextBoxColumn40.HeaderText = "Asiento"
        Me.DataGridViewTextBoxColumn40.Name = "DataGridViewTextBoxColumn40"
        '
        'DataGridViewTextBoxColumn41
        '
        Me.DataGridViewTextBoxColumn41.DataPropertyName = "IMP RENTA"
        Me.DataGridViewTextBoxColumn41.HeaderText = "IMP RENTA"
        Me.DataGridViewTextBoxColumn41.Name = "DataGridViewTextBoxColumn41"
        '
        'DataGridViewTextBoxColumn42
        '
        Me.DataGridViewTextBoxColumn42.DataPropertyName = "EXTRAS"
        Me.DataGridViewTextBoxColumn42.HeaderText = "EXTRAS"
        Me.DataGridViewTextBoxColumn42.Name = "DataGridViewTextBoxColumn42"
        '
        'DataGridViewTextBoxColumn43
        '
        Me.DataGridViewTextBoxColumn43.DataPropertyName = "CESANTIA"
        Me.DataGridViewTextBoxColumn43.HeaderText = "CESANTIA"
        Me.DataGridViewTextBoxColumn43.Name = "DataGridViewTextBoxColumn43"
        '
        'DataGridViewTextBoxColumn44
        '
        Me.DataGridViewTextBoxColumn44.DataPropertyName = "SUBSIDIO"
        Me.DataGridViewTextBoxColumn44.HeaderText = "SUBSIDIO"
        Me.DataGridViewTextBoxColumn44.Name = "DataGridViewTextBoxColumn44"
        '
        'DataGridViewTextBoxColumn45
        '
        Me.DataGridViewTextBoxColumn45.DataPropertyName = "CCSS"
        Me.DataGridViewTextBoxColumn45.HeaderText = "CCSS"
        Me.DataGridViewTextBoxColumn45.Name = "DataGridViewTextBoxColumn45"
        '
        'DataGridViewTextBoxColumn46
        '
        Me.DataGridViewTextBoxColumn46.DataPropertyName = "codDeduc"
        Me.DataGridViewTextBoxColumn46.HeaderText = "codDeduc"
        Me.DataGridViewTextBoxColumn46.Name = "DataGridViewTextBoxColumn46"
        '
        'PlanillaMensualDataGridView
        '
        Me.PlanillaMensualDataGridView.AutoGenerateColumns = False
        Me.PlanillaMensualDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.PlanillaMensualDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn47, Me.DataGridViewTextBoxColumn48, Me.DataGridViewTextBoxColumn49, Me.DataGridViewTextBoxColumn50, Me.DataGridViewTextBoxColumn51, Me.DataGridViewTextBoxColumn52, Me.DataGridViewTextBoxColumn53, Me.DataGridViewTextBoxColumn54, Me.DataGridViewTextBoxColumn55, Me.DataGridViewTextBoxColumn56, Me.DataGridViewTextBoxColumn57, Me.DataGridViewTextBoxColumn58, Me.DataGridViewTextBoxColumn59, Me.DataGridViewTextBoxColumn60, Me.DataGridViewTextBoxColumn61, Me.DataGridViewTextBoxColumn62, Me.DataGridViewTextBoxColumn63, Me.DataGridViewTextBoxColumn64, Me.DataGridViewTextBoxColumn65, Me.DataGridViewTextBoxColumn66, Me.DataGridViewTextBoxColumn67, Me.DataGridViewTextBoxColumn68, Me.DataGridViewTextBoxColumn69})
        Me.PlanillaMensualDataGridView.DataSource = Me.PlanillaMensualBindingSource
        Me.PlanillaMensualDataGridView.Location = New System.Drawing.Point(784, 378)
        Me.PlanillaMensualDataGridView.Name = "PlanillaMensualDataGridView"
        Me.PlanillaMensualDataGridView.Size = New System.Drawing.Size(300, 220)
        Me.PlanillaMensualDataGridView.TabIndex = 134
        '
        'DataGridViewTextBoxColumn47
        '
        Me.DataGridViewTextBoxColumn47.DataPropertyName = "FechaPlanMensual"
        Me.DataGridViewTextBoxColumn47.HeaderText = "FechaPlanMensual"
        Me.DataGridViewTextBoxColumn47.Name = "DataGridViewTextBoxColumn47"
        '
        'DataGridViewTextBoxColumn48
        '
        Me.DataGridViewTextBoxColumn48.DataPropertyName = "NoCEDULA"
        Me.DataGridViewTextBoxColumn48.HeaderText = "NoCEDULA"
        Me.DataGridViewTextBoxColumn48.Name = "DataGridViewTextBoxColumn48"
        '
        'DataGridViewTextBoxColumn49
        '
        Me.DataGridViewTextBoxColumn49.DataPropertyName = "Nombre"
        Me.DataGridViewTextBoxColumn49.HeaderText = "Nombre"
        Me.DataGridViewTextBoxColumn49.Name = "DataGridViewTextBoxColumn49"
        '
        'DataGridViewTextBoxColumn50
        '
        Me.DataGridViewTextBoxColumn50.DataPropertyName = "% ANUAL"
        Me.DataGridViewTextBoxColumn50.HeaderText = "% ANUAL"
        Me.DataGridViewTextBoxColumn50.Name = "DataGridViewTextBoxColumn50"
        '
        'DataGridViewTextBoxColumn51
        '
        Me.DataGridViewTextBoxColumn51.DataPropertyName = "ANUALIDAD"
        Me.DataGridViewTextBoxColumn51.HeaderText = "ANUALIDAD"
        Me.DataGridViewTextBoxColumn51.Name = "DataGridViewTextBoxColumn51"
        '
        'DataGridViewTextBoxColumn52
        '
        Me.DataGridViewTextBoxColumn52.DataPropertyName = "SALARIO BRUTO"
        Me.DataGridViewTextBoxColumn52.HeaderText = "SALARIO BRUTO"
        Me.DataGridViewTextBoxColumn52.Name = "DataGridViewTextBoxColumn52"
        '
        'DataGridViewTextBoxColumn53
        '
        Me.DataGridViewTextBoxColumn53.DataPropertyName = "SALARIO DIARIO"
        Me.DataGridViewTextBoxColumn53.HeaderText = "SALARIO DIARIO"
        Me.DataGridViewTextBoxColumn53.Name = "DataGridViewTextBoxColumn53"
        '
        'DataGridViewTextBoxColumn54
        '
        Me.DataGridViewTextBoxColumn54.DataPropertyName = "LIQUIDO A PAGAR"
        Me.DataGridViewTextBoxColumn54.HeaderText = "LIQUIDO A PAGAR"
        Me.DataGridViewTextBoxColumn54.Name = "DataGridViewTextBoxColumn54"
        '
        'DataGridViewTextBoxColumn55
        '
        Me.DataGridViewTextBoxColumn55.DataPropertyName = "DIAS TRABAJ"
        Me.DataGridViewTextBoxColumn55.HeaderText = "DIAS TRABAJ"
        Me.DataGridViewTextBoxColumn55.Name = "DataGridViewTextBoxColumn55"
        '
        'DataGridViewTextBoxColumn56
        '
        Me.DataGridViewTextBoxColumn56.DataPropertyName = "DiasIncap"
        Me.DataGridViewTextBoxColumn56.HeaderText = "DiasIncap"
        Me.DataGridViewTextBoxColumn56.Name = "DataGridViewTextBoxColumn56"
        '
        'DataGridViewTextBoxColumn57
        '
        Me.DataGridViewTextBoxColumn57.DataPropertyName = "InstitucionIncapacidad"
        Me.DataGridViewTextBoxColumn57.HeaderText = "InstitucionIncapacidad"
        Me.DataGridViewTextBoxColumn57.Name = "DataGridViewTextBoxColumn57"
        '
        'DataGridViewTextBoxColumn58
        '
        Me.DataGridViewTextBoxColumn58.DataPropertyName = "HORAS EXTRAS REGULAR"
        Me.DataGridViewTextBoxColumn58.HeaderText = "HORAS EXTRAS REGULAR"
        Me.DataGridViewTextBoxColumn58.Name = "DataGridViewTextBoxColumn58"
        '
        'DataGridViewTextBoxColumn59
        '
        Me.DataGridViewTextBoxColumn59.DataPropertyName = "HORAS EXTRAS DOBLES"
        Me.DataGridViewTextBoxColumn59.HeaderText = "HORAS EXTRAS DOBLES"
        Me.DataGridViewTextBoxColumn59.Name = "DataGridViewTextBoxColumn59"
        '
        'DataGridViewTextBoxColumn60
        '
        Me.DataGridViewTextBoxColumn60.DataPropertyName = "TOTAL DIAS INCAP"
        Me.DataGridViewTextBoxColumn60.HeaderText = "TOTAL DIAS INCAP"
        Me.DataGridViewTextBoxColumn60.Name = "DataGridViewTextBoxColumn60"
        '
        'DataGridViewTextBoxColumn61
        '
        Me.DataGridViewTextBoxColumn61.DataPropertyName = "FechaINCAPINI"
        Me.DataGridViewTextBoxColumn61.HeaderText = "FechaINCAPINI"
        Me.DataGridViewTextBoxColumn61.Name = "DataGridViewTextBoxColumn61"
        '
        'DataGridViewTextBoxColumn62
        '
        Me.DataGridViewTextBoxColumn62.DataPropertyName = "FechaINCAPFIN"
        Me.DataGridViewTextBoxColumn62.HeaderText = "FechaINCAPFIN"
        Me.DataGridViewTextBoxColumn62.Name = "DataGridViewTextBoxColumn62"
        '
        'DataGridViewTextBoxColumn63
        '
        Me.DataGridViewTextBoxColumn63.DataPropertyName = "Asiento"
        Me.DataGridViewTextBoxColumn63.HeaderText = "Asiento"
        Me.DataGridViewTextBoxColumn63.Name = "DataGridViewTextBoxColumn63"
        '
        'DataGridViewTextBoxColumn64
        '
        Me.DataGridViewTextBoxColumn64.DataPropertyName = "IMP RENTA"
        Me.DataGridViewTextBoxColumn64.HeaderText = "IMP RENTA"
        Me.DataGridViewTextBoxColumn64.Name = "DataGridViewTextBoxColumn64"
        '
        'DataGridViewTextBoxColumn65
        '
        Me.DataGridViewTextBoxColumn65.DataPropertyName = "EXTRAS"
        Me.DataGridViewTextBoxColumn65.HeaderText = "EXTRAS"
        Me.DataGridViewTextBoxColumn65.Name = "DataGridViewTextBoxColumn65"
        '
        'DataGridViewTextBoxColumn66
        '
        Me.DataGridViewTextBoxColumn66.DataPropertyName = "CESANTIA"
        Me.DataGridViewTextBoxColumn66.HeaderText = "CESANTIA"
        Me.DataGridViewTextBoxColumn66.Name = "DataGridViewTextBoxColumn66"
        '
        'DataGridViewTextBoxColumn67
        '
        Me.DataGridViewTextBoxColumn67.DataPropertyName = "SUBSIDIO"
        Me.DataGridViewTextBoxColumn67.HeaderText = "SUBSIDIO"
        Me.DataGridViewTextBoxColumn67.Name = "DataGridViewTextBoxColumn67"
        '
        'DataGridViewTextBoxColumn68
        '
        Me.DataGridViewTextBoxColumn68.DataPropertyName = "codDed"
        Me.DataGridViewTextBoxColumn68.HeaderText = "codDed"
        Me.DataGridViewTextBoxColumn68.Name = "DataGridViewTextBoxColumn68"
        '
        'DataGridViewTextBoxColumn69
        '
        Me.DataGridViewTextBoxColumn69.DataPropertyName = "CCSS"
        Me.DataGridViewTextBoxColumn69.HeaderText = "CCSS"
        Me.DataGridViewTextBoxColumn69.Name = "DataGridViewTextBoxColumn69"
        '
        'frmPlanillaMensuals
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1323, 750)
        Me.Controls.Add(Me.PlanillaMensualDataGridView)
        Me.Controls.Add(Me.PlanillaBisemanalDataGridView1)
        Me.Controls.Add(Me.PlanillaBisemanalDataGridView)
        Me.Controls.Add(CodImpuestoLabel1)
        Me.Controls.Add(Me.CodImpuestoTextBox1)
        Me.Controls.Add(ProcentajeImpuesto1Label2)
        Me.Controls.Add(Me.ProcentajeImpuesto1TextBox2)
        Me.Controls.Add(ProcentajeImpuesto2Label2)
        Me.Controls.Add(Me.ProcentajeImpuesto2TextBox2)
        Me.Controls.Add(CreditosFiscalesHijoLabel2)
        Me.Controls.Add(Me.CreditosFiscalesHijoTextBox2)
        Me.Controls.Add(CreditosFiscalesConyugeLabel2)
        Me.Controls.Add(Me.CreditosFiscalesConyugeTextBox2)
        Me.Controls.Add(CodImpuestoLabel)
        Me.Controls.Add(Me.CodImpuestoTextBox)
        Me.Controls.Add(ProcentajeImpuesto1Label1)
        Me.Controls.Add(Me.ProcentajeImpuesto1TextBox1)
        Me.Controls.Add(ProcentajeImpuesto2Label1)
        Me.Controls.Add(Me.ProcentajeImpuesto2TextBox1)
        Me.Controls.Add(CreditosFiscalesHijoLabel1)
        Me.Controls.Add(Me.CreditosFiscalesHijoTextBox1)
        Me.Controls.Add(CreditosFiscalesConyugeLabel1)
        Me.Controls.Add(Me.CreditosFiscalesConyugeTextBox1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(CreditosFiscalesHijoLabel)
        Me.Controls.Add(Me.CreditosFiscalesConyugeTextBox)
        Me.Controls.Add(CreditosFiscalesConyugeLabel)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmPlanillaMensuals"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmPlanillaMensuals"
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        CType(Me.PlanillaBisemanalDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PlanillaBisemanalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Planilla2DataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImpuestosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PlanillaMensualBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AjustesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PuestosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DeduccionesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DatosPersonalesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PlanillaBisemanalDataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PlanillaMensualDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Planilla2DataSet1 As PlanillasProyecto3.Planilla2DataSet1
    Friend WithEvents DatosPersonalesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DatosPersonalesTableAdapter As PlanillasProyecto3.Planilla2DataSet1TableAdapters.DatosPersonalesTableAdapter
    Friend WithEvents TableAdapterManager As PlanillasProyecto3.Planilla2DataSet1TableAdapters.TableAdapterManager
    Friend WithEvents DeduccionesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DeduccionesTableAdapter As PlanillasProyecto3.Planilla2DataSet1TableAdapters.DeduccionesTableAdapter
    Friend WithEvents ImpuestosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ImpuestosTableAdapter As PlanillasProyecto3.Planilla2DataSet1TableAdapters.ImpuestosTableAdapter
    Friend WithEvents ProcentajeImpuesto1TextBox As System.Windows.Forms.TextBox
    Friend WithEvents ProcentajeImpuesto2TextBox As System.Windows.Forms.TextBox
    Friend WithEvents CreditosFiscalesHijoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PuestosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PuestosTableAdapter As PlanillasProyecto3.Planilla2DataSet1TableAdapters.PuestosTableAdapter
    Friend WithEvents AjustesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents AjustesTableAdapter As PlanillasProyecto3.Planilla2DataSet1TableAdapters.AjustesTableAdapter
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents CodCedulaAjusteTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TOTALAJUSTARTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SALARIO_UNICOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SumaTotalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NoCEDULATextBox As System.Windows.Forms.TextBox
    Friend WithEvents NOMBRE_COMPLETOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CODPUESTOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents FECHA_INGRESODateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents N__CONYUGESCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents N__HIJOSTextBox As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents PlanillaMensualBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PlanillaMensualTableAdapter As PlanillasProyecto3.Planilla2DataSet1TableAdapters.PlanillaMensualTableAdapter
    Friend WithEvents FechaPlanMensualDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents __ANUALTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ANUALIDADTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SALARIO_DIARIOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents LIQUIDO_A_PAGARTextBox As System.Windows.Forms.TextBox
    Friend WithEvents AsientoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents IMP_RENTATextBox As System.Windows.Forms.TextBox
    Friend WithEvents EXTRASTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CESANTIATextBox As System.Windows.Forms.TextBox
    Friend WithEvents SUBSIDIOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CCSSTextBox As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents TOTAL_DIAS_INCAPTextBox As System.Windows.Forms.TextBox
    Friend WithEvents FechaINCAPINIDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents FechaINCAPFINDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents InstitucionIncapacidadTextBox As System.Windows.Forms.ComboBox
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtImpuestoSobreSalario As System.Windows.Forms.TextBox
    Friend WithEvents CodDeduccionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents txtAños As System.Windows.Forms.TextBox
    Friend WithEvents CodImpuestoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ProcentajeImpuesto1TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents ProcentajeImpuesto2TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents CreditosFiscalesHijoTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents CreditosFiscalesConyugeTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents CreditosFiscalesConyugeTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ArchivoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RegresarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents CodImpuestoTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents ProcentajeImpuesto1TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents ProcentajeImpuesto2TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents CreditosFiscalesHijoTextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents CreditosFiscalesConyugeTextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents HORAS_EXTRAS_DOBLESTextBox As System.Windows.Forms.ComboBox
    Friend WithEvents HORAS_EXTRAS_REGULARTextBox As System.Windows.Forms.ComboBox
    Friend WithEvents DIAS_TRABAJTextBox As System.Windows.Forms.ComboBox
    Friend WithEvents PlanillaBisemanalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PlanillaBisemanalTableAdapter As PlanillasProyecto3.Planilla2DataSet1TableAdapters.PlanillaBisemanalTableAdapter
    Friend WithEvents PlanillaBisemanalDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn20 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn21 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn22 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn23 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ToolStripButton4 As System.Windows.Forms.ToolStripButton
    Friend WithEvents AccionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GuardarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BuscarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
    Friend WithEvents PlanillaMensualDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn47 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn48 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn49 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn50 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn51 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn52 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn53 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn54 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn55 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn56 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn57 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn58 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn59 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn60 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn61 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn62 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn63 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn64 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn65 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn66 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn67 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn68 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn69 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PlanillaBisemanalDataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn24 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn25 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn26 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn27 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn28 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn29 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn30 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn31 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn32 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn33 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn34 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn35 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn36 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn37 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn38 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn39 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn40 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn41 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn42 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn43 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn44 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn45 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn46 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

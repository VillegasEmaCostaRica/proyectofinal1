﻿Public Class frmPlanillaMensualConsultaS

    Private Sub PlanillaMensualBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.PlanillaMensualBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet1)

    End Sub

    Private Sub frmPlanillaMensualConsultaS_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Planilla2DataSet1.PlanillaMensual' table. You can move, or remove it, as needed.
        Me.PlanillaMensualTableAdapter.Fill(Me.Planilla2DataSet1.PlanillaMensual)
        'TODO: This line of code loads data into the 'Planilla2DataSet.PlanillaMensual' table. You can move, or remove it, as needed.
        
    End Sub

    Private Sub ToolStripButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton3.Click
        Me.Close()
        frmPlanillaMensuals.Show()
    End Sub

    Private Sub RegresarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegresarToolStripMenuItem.Click
        Me.Close()
        frmPlanillaMensuals.Show()

    End Sub

    Private Sub GuardarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub BuscarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BuscarToolStripMenuItem.Click
        opciones()
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        opciones()
    End Sub

    Sub opciones()

      
        Try



            If My.Forms.frmPrincipal.status = True Then

                If ComboBox1.SelectedIndex = 0 Then
                    PlanillaMensualTableAdapter.FillByFechaPlanillaMensual(Me.Planilla2DataSet1.PlanillaMensual, Date.Parse(TextBox1.Text))
                End If

                If ComboBox1.SelectedIndex = 1 Then
                    PlanillaMensualTableAdapter.FillByPlanillaMensualCedula(Me.Planilla2DataSet1.PlanillaMensual, Integer.Parse(TextBox1.Text))
                End If

                If ComboBox1.SelectedIndex = 2 Then

                    If MsgBox("Seguro desea eliminar el registro", vbYesNo) = vbYes Then
                        PlanillaMensualTableAdapter.DeleteQueryFecha(Date.Parse(TextBox1.Text))
                        Me.PlanillaMensualTableAdapter.Fill(Me.Planilla2DataSet1.PlanillaMensual)
                        MsgBox("Registro eliminado")
                    End If

                End If
                If ComboBox1.SelectedIndex = 3 Then
                    If MsgBox("Seguro desea eliminar el registro", vbYesNo) = vbYes Then
                        PlanillaMensualTableAdapter.DeleteQueryCedula(Integer.Parse(TextBox1.Text))
                        Me.PlanillaMensualTableAdapter.Fill(Me.Planilla2DataSet1.PlanillaMensual)

                        MsgBox("Registro eliminado")


                    End If
                End If

            End If

            
        Catch ex As FormatException
            MsgBox(ex.Message)
        Catch x As Exception
            MsgBox(x.Message)

        End Try
        
    End Sub
    Private Sub MostarLosRegistrosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MostarLosRegistrosToolStripMenuItem.Click
        Me.PlanillaMensualTableAdapter.Fill(Me.Planilla2DataSet1.PlanillaMensual)

    End Sub

    Private Sub ToolStripButton4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton4.Click
        Me.PlanillaMensualTableAdapter.Fill(Me.Planilla2DataSet1.PlanillaMensual)
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        opciones()
    End Sub

    Private Sub ToolStrip1_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles ToolStrip1.ItemClicked

    End Sub

    Private Sub PlanillaMensualBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.PlanillaMensualBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet1)

    End Sub

    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click
        opciones()
    End Sub
End Class
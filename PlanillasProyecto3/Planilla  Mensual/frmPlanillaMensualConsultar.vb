﻿Public Class frmPlanillaMensualConsultar

    Private Sub PlanillaMensualBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.PlanillaMensualBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet1)

    End Sub

    Private Sub frmPlanillaMensualConsultar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Planilla2DataSet1.PlanillaMensual' table. You can move, or remove it, as needed.
        Me.PlanillaMensualTableAdapter.Fill(Me.Planilla2DataSet1.PlanillaMensual)

    End Sub

    Private Sub RegresarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegresarToolStripMenuItem.Click
        Me.Close()
        frmPlanillaMensual.Show()
    End Sub

    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click
        Me.Close()
        frmPlanillaMensual.Show()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try

            PlanillaMensualTableAdapter.FillByConsultaPlanillaMensualFecha(Me.Planilla2DataSet1.PlanillaMensual, TextBox1.Text)
            PlanillaMensualTableAdapter.FillBy(Me.Planilla2DataSet1.PlanillaMensual, Integer.Parse(TextBox1.Text))


        Catch ex As Exception

        End Try
    End Sub

    Private Sub ToolStripButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton3.Click
        Me.PlanillaMensualTableAdapter.Fill(Me.Planilla2DataSet1.PlanillaMensual)

    End Sub
End Class
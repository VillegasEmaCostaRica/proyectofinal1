﻿Public Class frmEliminaMensual

    Private Sub PlanillaMensualBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.PlanillaMensualBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)

    End Sub

    Private Sub frmConsulataMensual_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Planilla2DataSet1.PlanillaMensual' table. You can move, or remove it, as needed.
        Me.PlanillaMensualTableAdapter1.Fill(Me.Planilla2DataSet1.PlanillaMensual)
        'TODO: This line of code loads data into the 'Planilla2DataSet.PlanillaMensual' table. You can move, or remove it, as needed.
        Me.PlanillaMensualTableAdapter.Fill(Me.Planilla2DataSet.PlanillaMensual)

    End Sub

    Sub eliminarRegistro()
        Try
            If MsgBox("Seguro desea eliminar el registro", vbYesNo) = vbYes Then
                PlanillaMensualTableAdapter1.DeleteQuery(Integer.Parse(TextBox1.Text))
                Me.PlanillaMensualTableAdapter1.Fill(Me.Planilla2DataSet1.PlanillaMensual)
                MsgBox("Registro eliminado")
            End If
        Catch ex As Exception

            MsgBox("Verifique los datos" & ex.Message)
        End Try
      End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        eliminarRegistro()

    End Sub

    Private Sub RegresarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegresarToolStripMenuItem.Click
        Me.Close()
        frmPlanillaMensual.Show()
    End Sub

    Private Sub EliminarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EliminarToolStripMenuItem.Click
        eliminarRegistro()
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        eliminarRegistro()
    End Sub

    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click
        Me.Close()
        frmPlanillaMensual.Show()
    End Sub
End Class
﻿
Public Class frmPlanillaMensuals

    Dim numeroCedula As String
    Dim numeroHijos As String
    Dim numeroConyugues As String
    Public datoBusqueda As String
    Public datoBusqueda2 As Boolean
    Dim opcion As String
    Dim impuesto As Double
    Dim sueldo As Double
    Dim salarioUnico As Double
    Dim cadena As String
    Dim SALARIOBRUTO As Double
    Dim SALARIODIARIO As Double
    Dim LIQUIDOAPAGAR As Double
    Dim DIASTRABAJ As Integer
    Dim DiasIncap As Integer
    Dim institucionIncapacidad As Integer
    Dim HORASEXTRASREGULAR As Integer
    Dim horasExtrasDobles As Integer
    Dim TOTALDIASINCAP As Integer
    Dim FechaINCAPINI As String
    Dim FechaINCAPFIN As String
    Dim Asiento As String
    Dim IMPRENTA As Double
    Dim EXTRAS As Double
    Dim CESANTIA As Double
    Dim SUBSIDIO As Double
    Dim codDed As Double
    Dim FechaPlanMensual As String
    Dim NoCEDULA As Integer
    Dim Nombre As String
    Dim ANUALIDAD As Double
    Dim ccss As Double

    Private Sub DatosPersonalesBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.DatosPersonalesBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet1)

    End Sub

    Private Sub frmPlanillaMensuals_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FECHA_INGRESODateTimePicker.Enabled = False
        'TODO: This line of code loads data into the 'Planilla2DataSet1.PlanillaBisemanal' table. You can move, or remove it, as needed.
        Me.PlanillaBisemanalTableAdapter.Fill(Me.Planilla2DataSet1.PlanillaBisemanal)
        'TODO: This line of code loads data into the 'Planilla2DataSet1.PlanillaMensual' table. You can move, or remove it, as needed.
        Me.PlanillaMensualTableAdapter.Fill(Me.Planilla2DataSet1.PlanillaMensual)
        'TODO: This line of code loads data into the 'Planilla2DataSet1.Ajustes' table. You can move, or remove it, as needed.
        '  Me.AjustesTableAdapter.Fill(Me.Planilla2DataSet1.Ajustes)
        'TODO: This line of code loads data into the 'Planilla2DataSet1.Puestos' table. You can move, or remove it, as needed.
        'Me.PuestosTableAdapter.Fill(Me.Planilla2DataSet1.Puestos)
        'TODO: This line of code loads data into the 'Planilla2DataSet1.Impuestos' table. You can move, or remove it, as needed.Me.ImpuestosTableAdapter.Fill(Me.Planilla2DataSet1.Impuestos)
        'TODO: This line of code loads data into the 'Planilla2DataSet1.Deducciones' table. You can move, or remove it, as needed.
        '  Me.DeduccionesTableAdapter.Fill(Me.Planilla2DataSet1.Deducciones)
        'TODO: This line of code loads data into the 'Planilla2DataSet1.DatosPersonales' table. You can move, or remove it, as needed.
        'Me.DatosPersonalesTableAdapter.Fill(Me.Planilla2DataSet1.DatosPersonales)
        ImpuestosTableAdapter.FillByImpuestos(Me.Planilla2DataSet1.Impuestos, Integer.Parse(Now.Year))
        ImpuestosTableAdapter.FillByCodImpuesto(Me.Planilla2DataSet1.Impuestos, Integer.Parse(txtImpuestoSobreSalario.Text))
        My.Forms.frmConsultaDatosPersonales.opcion = datoBusqueda2
    End Sub



    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        
        abilitarGroupBoxs()
        Me.Close()
        My.Forms.frmPrincipal.Hide()

        My.Forms.frmConsultaDatosPersonales.opcion = My.Forms.frmPrincipal.status
      
        frmConsultaDatosPersonales.Show()

    End Sub
    Private Sub NoCEDULATextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NoCEDULATextBox.TextChanged
        buscarRegistro()
        txtImpuestoSobreSalario.Text = Year(Now) & vbTab
        CodDeduccionTextBox.Text = Year(Now)
        CESANTIATextBox.Text = "0"
        CodCedulaAjusteTextBox.Text = NoCEDULATextBox.Text

    End Sub
    Sub buscarRegistro()
        Try
            Dim numeroCedula As String
            numeroCedula = NoCEDULATextBox.Text
            DatosPersonalesTableAdapter.FillByEmployee(Me.Planilla2DataSet1.DatosPersonales, Integer.Parse(numeroCedula))
            PuestosTableAdapter.FillByPuesto(Me.Planilla2DataSet1.Puestos, Integer.Parse(CODPUESTOTextBox.Text))
            Math.Abs(AjustesTableAdapter.FillByDatosAjuste(Me.Planilla2DataSet1.Ajustes, Integer.Parse(numeroCedula)))


        Catch ex As Exception

        End Try

    End Sub

    Private Sub CodDeduccionTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CodDeduccionTextBox.TextChanged
        Try
            DeduccionesTableAdapter.FillBy(Me.Planilla2DataSet1.Deducciones, Integer.Parse(CodDeduccionTextBox.Text))
        Catch ex As Exception

        End Try

    End Sub

    Private Sub CODPUESTOTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CODPUESTOTextBox.TextChanged
        Try
            PuestosTableAdapter.FillByPuesto(Me.Planilla2DataSet1.Puestos, Integer.Parse(CODPUESTOTextBox.Text))

        Catch ex As Exception

        End Try

    End Sub

    Private Sub CodCedulaAjusteTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CodCedulaAjusteTextBox.TextChanged
        Try
            AjustesTableAdapter.FillByDatosAjuste(Me.Planilla2DataSet1.Ajustes, Integer.Parse(CodCedulaAjusteTextBox.Text))

        Catch ex As Exception

        End Try

    End Sub

    Private Sub FECHA_INGRESODateTimePicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FECHA_INGRESODateTimePicker.ValueChanged
        Dim data As Date
        data = FECHA_INGRESODateTimePicker.Text
        txtAños.Text = DateDiff(DateInterval.Year, data, Date.Now())

    End Sub

    Private Sub txtAños_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAños.TextChanged
        Try

            Dim resultado As Double

            Select Case CODPUESTOTextBox.Text
                Case 36

                    resultado = Integer.Parse(txtAños.Text) * 0.015
                Case 21
                    resultado = Integer.Parse(txtAños.Text) * 0.0175
                Case 31
                    resultado = Integer.Parse(txtAños.Text) * 0.0175
                Case Else
                    resultado = Integer.Parse(txtAños.Text) * 0.02
            End Select
            __ANUALTextBox.Text = resultado
        Catch ex As Exception

        End Try

    End Sub

    Private Sub __ANUALTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles __ANUALTextBox.TextChanged
        ANUALIDADTextBox.Text = "0"
        Dim ccss As Double

        Try
            Me.sueldo = Integer.Parse(SALARIO_UNICOTextBox.Text) & vbTab
            ANUALIDADTextBox.Text = sueldo * Double.Parse(__ANUALTextBox.Text) & vbTab

            ANUALIDADTextBox.Text = sueldo * Double.Parse(__ANUALTextBox.Text) & vbTab
            sueldo = Double.Parse(SALARIO_UNICOTextBox.Text) & vbTab

            SALARIO_DIARIOTextBox.Text = Fix(Integer.Parse(SALARIO_UNICOTextBox.Text) / 30) & vbTab
            ccss = Double.Parse(SALARIO_UNICOTextBox.Text) * 0.9
            CCSSTextBox.Text = (Double.Parse(SALARIO_UNICOTextBox.Text)) - ccss


        Catch ex As Exception

        End Try

    End Sub


    Function calculoImpuesto(ByVal sueldo As Double) As Double

        Try

            If sueldo > 714000 Then
                If sueldo < 1071000 Then
                    impuesto = (sueldo * 10) / 100
                End If
            End If

            If sueldo > 1071000 Then
                impuesto = (sueldo * 15) / 100

            End If

        Catch ex As Exception

        End Try

        Return impuesto
    End Function


    Private Sub txtImpuestoSobreSalario_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtImpuestoSobreSalario.TextChanged
        Try

            Dim salarioBruto As Double
            salarioBruto = Double.Parse(SALARIO_UNICOTextBox.Text)

            ImpuestosTableAdapter.FillByImpuestos(Me.Planilla2DataSet1.Impuestos, Integer.Parse(txtImpuestoSobreSalario.Text))
            ImpuestosTableAdapter.FillByCodImpuesto(Me.Planilla2DataSet1.Impuestos, Integer.Parse(txtImpuestoSobreSalario.Text))

        Catch ex As Exception

        End Try
    End Sub


    Private Sub SALARIO_UNICOTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SALARIO_UNICOTextBox.TextChanged
        Try
            IMP_RENTATextBox.Text = calculoImpuesto(SALARIO_UNICOTextBox.Text)
            Me.salarioUnico = Double.Parse(SALARIO_UNICOTextBox.Text)

        Catch ex As Exception

        End Try
     End Sub

    Private Sub DIAS_TRABAJTextBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Try
            CESANTIATextBox.Text = Fix((Double.Parse(SALARIO_UNICOTextBox.Text) * Integer.Parse(DIAS_TRABAJTextBox.Text)) / 360)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub HORAS_EXTRAS_DOBLESTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles HORAS_EXTRAS_REGULARTextBox.TextChanged, HORAS_EXTRAS_DOBLESTextBox.TextChanged

        Try

            EXTRASTextBox.Text = Math.Abs((Double.Parse(SALARIO_DIARIOTextBox.Text) * Integer.Parse(HORAS_EXTRAS_REGULARTextBox.SelectedIndex) + (Double.Parse(SALARIO_DIARIOTextBox.Text) * Integer.Parse(HORAS_EXTRAS_DOBLESTextBox.SelectedIndex) * 2)))



        Catch ex As Exception

        End Try
    End Sub

    Sub CalcularDeducion(ByVal SalarioTotal As Integer, ByVal CanHijos As Integer, ByVal Casado As Boolean)



        Select Case SalarioTotal

            Case SalarioTotal > 714000 Or SalarioTotal < 1071000

                IMP_RENTATextBox.Text = (SalarioTotal * 10) / 100

                If Casado = True Then

                    IMP_RENTATextBox.Text -= 2000

                End If

                If CanHijos > 0 Then
                    CanHijos *= 1340
                    IMP_RENTATextBox.Text -= CanHijos
                End If


            Case SalarioTotal > 1071000

                IMP_RENTATextBox.Text = (SalarioTotal * 15) / 100

                If Casado = True Then

                    IMP_RENTATextBox.Text -= 2000

                End If


                If CanHijos > 0 Then
                    CanHijos *= 1340
                    IMP_RENTATextBox.Text -= CanHijos
                End If

            Case Else

        End Select

    End Sub

    Private Sub TOTAL_DIAS_INCAPTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TOTAL_DIAS_INCAPTextBox.TextChanged

        Try
            If Integer.Parse(TOTAL_DIAS_INCAPTextBox.Text) <= 3 Then

                SUBSIDIOTextBox.Text = (Double.Parse(SALARIO_UNICOTextBox.Text) * 100) / 100

            End If


            If Integer.Parse(TOTAL_DIAS_INCAPTextBox.Text) > 3 Then


                If InstitucionIncapacidadTextBox.SelectedIndex = 0 Then

                    SUBSIDIOTextBox.Text = (Double.Parse(SALARIO_UNICOTextBox.Text) * 75) / 100

                End If

            End If

            If Integer.Parse(TOTAL_DIAS_INCAPTextBox.Text) > 3 Then

                If InstitucionIncapacidadTextBox.SelectedIndex = 1 Then
                    SUBSIDIOTextBox.Text = (Double.Parse(SALARIO_UNICOTextBox.Text) * 40) / 100
                End If

            End If

        Catch ex As Exception

        End Try
        
       
    End Sub

    Private Sub FechaINCAPFINDateTimePicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FechaINCAPINIDateTimePicker.ValueChanged, FechaINCAPFINDateTimePicker.ValueChanged
        Try
            Dim data As Date
            data = FechaINCAPINIDateTimePicker.Text
            Dim diasFechas As String
            Dim fechaFinal As Date
            fechaFinal = FechaINCAPFINDateTimePicker.Text
            diasFechas = DateDiff(DateInterval.Day, data, fechaFinal)

            If diasFechas > 0 Then
                TOTAL_DIAS_INCAPTextBox.Text = DateDiff(DateInterval.Day, data, fechaFinal)

            Else
                MsgBox("No exitan dias negativos, verifique las fechas")
            End If
         
        Catch ex As Exception

        End Try

    End Sub

    Sub guardar()


        Try


            FechaPlanMensual = FechaPlanMensualDateTimePicker.Text


            NoCEDULA = Integer.Parse(NoCEDULATextBox.Text)


            Nombre = NOMBRE_COMPLETOTextBox.Text


            cadena = Replace(__ANUALTextBox.Text, ".", ",")

            ANUALIDAD = Double.Parse(ANUALIDADTextBox.Text)

            SALARIOBRUTO = Double.Parse(SALARIO_UNICOTextBox.Text)

            SALARIODIARIO = Double.Parse(SALARIO_DIARIOTextBox.Text)

            LIQUIDOAPAGAR = Double.Parse(LIQUIDO_A_PAGARTextBox.Text)

            DIASTRABAJ = Integer.Parse(DIAS_TRABAJTextBox.Text)

            DiasIncap = Integer.Parse(TOTAL_DIAS_INCAPTextBox.Text)

            institucionIncapacidad = Integer.Parse(InstitucionIncapacidadTextBox.SelectedIndex)

            HORASEXTRASREGULAR = Integer.Parse(HORAS_EXTRAS_REGULARTextBox.Text)

            horasExtrasDobles = Integer.Parse(HORAS_EXTRAS_DOBLESTextBox.Text)

            TOTALDIASINCAP = Integer.Parse(TOTAL_DIAS_INCAPTextBox.Text)

            FechaINCAPINI = FechaINCAPINIDateTimePicker.Text

            FechaINCAPFIN = FechaINCAPFINDateTimePicker.Text

            Asiento = AsientoTextBox.Text

            IMPRENTA = Double.Parse(IMP_RENTATextBox.Text)

            EXTRAS = Double.Parse(EXTRASTextBox.Text)

            CESANTIA = Double.Parse(CESANTIATextBox.Text)

            SUBSIDIO = Double.Parse(SUBSIDIOTextBox.Text)

            codDed = Double.Parse(SumaTotalTextBox.Text)

            ccss = (Double.Parse(CCSSTextBox.Text))

            If Me.datoBusqueda2 = True Then

                PlanillaMensualTableAdapter.InsertQuerys(FechaPlanMensual, NoCEDULA, Nombre, cadena, ANUALIDAD, SALARIOBRUTO, SALARIODIARIO, LIQUIDOAPAGAR, DIASTRABAJ, DiasIncap, institucionIncapacidad, HORASEXTRASREGULAR, horasExtrasDobles, TOTALDIASINCAP, FechaINCAPINI, FechaINCAPFIN, Asiento, IMPRENTA, EXTRAS, CESANTIA, SUBSIDIO, codDed, ccss)
                MsgBox("Planilla Mensual almacenada exitosamente")
                txtImpuestoSobreSalario.Text = 0
                LIQUIDO_A_PAGARTextBox.Text = 0
                limpiarCampos()

              End If

            If Me.datoBusqueda2 = False Then

                    PlanillaBisemanalTableAdapter.InsertQueryPlanillaBisemanal(FechaPlanMensual, NoCEDULA, Nombre, cadena, ANUALIDAD, SALARIOBRUTO, SALARIODIARIO, LIQUIDOAPAGAR, DIASTRABAJ, DiasIncap, institucionIncapacidad, HORASEXTRASREGULAR, horasExtrasDobles, TOTALDIASINCAP, FechaINCAPINI, FechaINCAPFIN, Asiento, IMPRENTA, EXTRAS, CESANTIA, SUBSIDIO, codDed, ccss)
                    MsgBox("Planilla Bisemanal almacenada exitosamente")
                    txtImpuestoSobreSalario.Text = 0
                    LIQUIDO_A_PAGARTextBox.Text = 0
                    limpiarCampos()
                End If
            

        Catch ex As Exception
            MsgBox("Verifique los datos" & ex.Message)
        End Try

    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        If My.Forms.frmPrincipal.modificarPlanillas = True Then
            modificarPlanillas()
        Else
            guardar()
        End If

    End Sub


    Sub modificarPlanillas()
        Try


            FechaPlanMensual = FechaPlanMensualDateTimePicker.Text


            NoCEDULA = Integer.Parse(NoCEDULATextBox.Text)


            Nombre = NOMBRE_COMPLETOTextBox.Text


            cadena = Replace(__ANUALTextBox.Text, ".", ",")

            ANUALIDAD = Double.Parse(ANUALIDADTextBox.Text)

            SALARIOBRUTO = Double.Parse(SALARIO_UNICOTextBox.Text)

            SALARIODIARIO = Double.Parse(SALARIO_DIARIOTextBox.Text)

            LIQUIDOAPAGAR = Double.Parse(LIQUIDO_A_PAGARTextBox.Text)

            DIASTRABAJ = Integer.Parse(DIAS_TRABAJTextBox.Text)

            DiasIncap = Integer.Parse(TOTAL_DIAS_INCAPTextBox.Text)

            institucionIncapacidad = Integer.Parse(InstitucionIncapacidadTextBox.SelectedIndex)

            HORASEXTRASREGULAR = Integer.Parse(HORAS_EXTRAS_REGULARTextBox.Text)

            horasExtrasDobles = Integer.Parse(HORAS_EXTRAS_DOBLESTextBox.Text)

            TOTALDIASINCAP = Integer.Parse(TOTAL_DIAS_INCAPTextBox.Text)

            FechaINCAPINI = FechaINCAPINIDateTimePicker.Text

            FechaINCAPFIN = FechaINCAPFINDateTimePicker.Text

            Asiento = AsientoTextBox.Text

            IMPRENTA = Double.Parse(IMP_RENTATextBox.Text)

            EXTRAS = Double.Parse(EXTRASTextBox.Text)

            CESANTIA = Double.Parse(CESANTIATextBox.Text)

            SUBSIDIO = Double.Parse(SUBSIDIOTextBox.Text)

            codDed = Double.Parse(SumaTotalTextBox.Text)

            ccss = (Double.Parse(CCSSTextBox.Text))

            If Me.datoBusqueda2 = True Then

                PlanillaMensualTableAdapter.UpdateQueryMensual(FechaPlanMensual, NoCEDULA, Nombre, cadena, ANUALIDAD, SALARIOBRUTO, SALARIODIARIO, LIQUIDOAPAGAR, DIASTRABAJ, DiasIncap, institucionIncapacidad, HORASEXTRASREGULAR, horasExtrasDobles, TOTALDIASINCAP, FechaINCAPINI, FechaINCAPFIN, Asiento, IMPRENTA, EXTRAS, CESANTIA, SUBSIDIO, codDed, ccss, NoCEDULA)
                MsgBox("Planilla Mensual Modificados exitosamente")
                txtImpuestoSobreSalario.Text = 0
                LIQUIDO_A_PAGARTextBox.Text = 0
                limpiarCampos()

            End If

            If Me.datoBusqueda2 = False Then

                PlanillaBisemanalTableAdapter.UpdateQueryBisemanal(FechaPlanMensual, NoCEDULA, Nombre, cadena, ANUALIDAD, SALARIOBRUTO, SALARIODIARIO, LIQUIDOAPAGAR, DIASTRABAJ, DiasIncap, institucionIncapacidad, HORASEXTRASREGULAR, horasExtrasDobles, TOTALDIASINCAP, FechaINCAPINI, FechaINCAPFIN, Asiento, IMPRENTA, EXTRAS, CESANTIA, SUBSIDIO, codDed, ccss, NoCEDULA)

                MsgBox("Planilla Bisemanal  Modificados exitosamente")
                txtImpuestoSobreSalario.Text = 0
                LIQUIDO_A_PAGARTextBox.Text = 0
                limpiarCampos()
            End If


        Catch ex As Exception
            MsgBox("Verifique los datos" & ex.Message)
        End Try
    End Sub

    Sub limpiarCampos()
        __ANUALTextBox.Text = 0
        LIQUIDO_A_PAGARTextBox.Text = 0
        NOMBRE_COMPLETOTextBox.Text = 0
        ANUALIDADTextBox.Text = 0
        SALARIO_UNICOTextBox.Text = 0
        SALARIO_DIARIOTextBox.Text = 0
        LIQUIDO_A_PAGARTextBox.Text = 0
        DIAS_TRABAJTextBox.Text = 0
        TOTAL_DIAS_INCAPTextBox.Text = 0
        InstitucionIncapacidadTextBox.SelectionStart = 0
        HORAS_EXTRAS_REGULARTextBox.Text = 0
        HORAS_EXTRAS_DOBLESTextBox.Text = 0
        TOTAL_DIAS_INCAPTextBox.Text = 0
        AsientoTextBox.Text = 0
        IMP_RENTATextBox.Text = 0
        EXTRASTextBox.Text = 0
        CESANTIATextBox.Text = 0
        SUBSIDIOTextBox.Text = 0
        SumaTotalTextBox.Text = 0
        CCSSTextBox.Text = 0


    End Sub
    Private Sub InstitucionIncapacidadTextBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InstitucionIncapacidadTextBox.SelectedIndexChanged

        If MsgBox("Seguro que quiera aplicar un incapacidad al empleado", vbYesNo) = vbYes Then
            DIAS_TRABAJTextBox.Enabled = False
            HORAS_EXTRAS_REGULARTextBox.Enabled = False
            HORAS_EXTRAS_DOBLESTextBox.Enabled = False
            CodCedulaAjusteTextBox.Text = 0
            ANUALIDADTextBox.Text = 0
            __ANUALTextBox.Text = 0
            SALARIO_DIARIOTextBox.Text = 0
            IMP_RENTATextBox.Text = 0
            txtImpuestoSobreSalario.Text = 0
            IMP_RENTATextBox.Text = 0
            DIAS_TRABAJTextBox.Text = 0
            CESANTIATextBox.Text = 0
            HORAS_EXTRAS_REGULARTextBox.Text = 0
            HORAS_EXTRAS_DOBLESTextBox.Text = 0
            EXTRASTextBox.Text = 0
            CCSSTextBox.Text = 0
            DIAS_TRABAJTextBox.Text = 0
            CESANTIATextBox.Text = 0
            HORAS_EXTRAS_DOBLESTextBox.Text = 0
            HORAS_EXTRAS_REGULARTextBox.Text = 0
            EXTRASTextBox.Text = 0
            codDed = 0
            LIQUIDO_A_PAGARTextBox.Text = 0

        Else
            abilitarGroupBoxs()
        End If

    End Sub


    Sub abilitarGroupBoxs()

        Dim c As New Control

        For Each c In Me.GroupBox2.Controls

            If TypeOf c Is TextBox Then

                CType(c, TextBox).Enabled = True
            End If

            If TypeOf c Is ComboBox Then

                CType(c, ComboBox).Enabled = True

            End If


        Next

        For Each c In Me.GroupBox4.Controls

            If TypeOf c Is TextBox Then

                CType(c, TextBox).Enabled = True

            End If
            If TypeOf c Is ComboBox Then

                CType(c, ComboBox).Enabled = True

            End If

        Next

    End Sub
    Sub limpiarGroupBoxs()

        Dim c As New Control

        For Each c In Me.GroupBox2.Controls

            If TypeOf c Is TextBox Then

                CType(c, TextBox).Clear()

            End If

        Next

        For Each c In Me.GroupBox4.Controls

            If TypeOf c Is TextBox Then

                CType(c, TextBox).Clear()

            End If
        Next
        For Each c In Me.GroupBox1.Controls

            If TypeOf c Is TextBox Then

                CType(c, TextBox).Clear()

            End If
        Next

    End Sub
    Sub bloquearGroupBoxs()

        Dim c As New Control

        For Each c In Me.GroupBox2.Controls

            If TypeOf c Is TextBox Then

                CType(c, TextBox).Enabled = False
            End If
            If TypeOf c Is ComboBox Then

                CType(c, ComboBox).Enabled = False

            End If


        Next
        For Each c In Me.GroupBox4.Controls

            If TypeOf c Is TextBox Then

                CType(c, TextBox).Enabled = False

            End If
            If TypeOf c Is ComboBox Then

                CType(c, ComboBox).Enabled = False

            End If

        Next

    End Sub

    Sub iniciarCampos()

        Dim c As New Control

        For Each c In Me.GroupBox2.Controls

            If TypeOf c Is TextBox Then

                CType(c, TextBox).Text = "0"
            End If

        Next
        For Each c In Me.GroupBox4.Controls
            If TypeOf c Is TextBox Then

                CType(c, TextBox).Text = "0"
            End If

        Next

    End Sub
  
    Private Sub EXTRASTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TOTALAJUSTARTextBox.TextChanged, SumaTotalTextBox.TextChanged, MyBase.TextChanged, IMP_RENTATextBox.TextChanged, EXTRASTextBox.TextChanged, CESANTIATextBox.TextChanged, CCSSTextBox.TextChanged, ANUALIDADTextBox.TextChanged
        Try
            Dim impuestoSobreSalario As Double
            impuestoSobreSalario = Double.Parse(IMP_RENTATextBox.Text)
            If N__CONYUGESCheckBox.Checked = True Then
                impuestoSobreSalario -= Integer.Parse(CreditosFiscalesConyugeTextBox2.Text)
            End If
            If Integer.Parse(N__HIJOSTextBox.Text) > 0 Then
                impuestoSobreSalario -= Double.Parse(CreditosFiscalesHijoTextBox1.Text) * Integer.Parse(N__HIJOSTextBox.Text)
            End If

            LIQUIDO_A_PAGARTextBox.Text = Fix(Double.Parse(TOTALAJUSTARTextBox.Text) - Double.Parse(SumaTotalTextBox.Text) + impuestoSobreSalario + Double.Parse(EXTRASTextBox.Text) + Double.Parse(CESANTIATextBox.Text) - Double.Parse(CCSSTextBox.Text) + Double.Parse(ANUALIDADTextBox.Text))
        Catch ex As Exception

        End Try

    End Sub

    Private Sub DIAS_TRABAJTextBox_SelectedIndexChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DIAS_TRABAJTextBox.SelectedIndexChanged
        Try
            CESANTIATextBox.Text = Fix(Double.Parse(SALARIO_UNICOTextBox.Text) * Double.Parse(DIAS_TRABAJTextBox.SelectedIndex) / 360)

        Catch ex As Exception

        End Try
        End Sub

    Private Sub NOMBRE_COMPLETOTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NOMBRE_COMPLETOTextBox.TextChanged
        AsientoTextBox.Text = "0"
        SUBSIDIOTextBox.Text = "0"
        InstitucionIncapacidadTextBox.Text = "Opción"
        TOTAL_DIAS_INCAPTextBox.Text = 0

    End Sub

    Private Sub GroupBox3_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox3.Enter

    End Sub

    Private Sub GroupBox1_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox1.Enter

    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        guardar()

    End Sub

    Private Sub ToolStripButton4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton4.Click
        Me.Close()
        frmPrincipal.Show()

    End Sub

    Private Sub SUBSIDIOTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SUBSIDIOTextBox.TextChanged
        LIQUIDO_A_PAGARTextBox.Enabled = True


        LIQUIDO_A_PAGARTextBox.Text = SUBSIDIOTextBox.Text
    End Sub

    Private Sub GroupBox2_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox2.Enter

    End Sub

    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click
        Me.Hide()

        If My.Forms.frmPrincipal.status = True Then
            frmPlanillaMensualConsultaS.Show()
            MsgBox("Bienvenido a la  planilla Mensual")
        Else
            frmPlanillaBisemanalConsulta.Show()
            MsgBox("Bienvenido a la  planilla Bisemanal")
        End If

        
    End Sub

    Private Sub GuardarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GuardarToolStripMenuItem.Click
        guardar()

    End Sub

    Private Sub BuscarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BuscarToolStripMenuItem.Click
        Me.Hide()

        If My.Forms.frmPrincipal.status = True Then
            frmPlanillaMensualConsultaS.Show()
            MsgBox("Bienvenido a la  planilla Mensual")
        Else
            frmPlanillaBisemanalConsulta.Show()
            MsgBox("Bienvenido a la  planilla Bisemanal")
        End If

    End Sub

    Private Sub RegresarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegresarToolStripMenuItem.Click
        Me.Close()
        frmPrincipal.Show()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Hide()
        If My.Forms.frmPrincipal.status = True Then
            frmPlanillaMensualConsultaS.Show()
            MsgBox("Bienvenido a la  planilla Mensual")
        Else
            frmPlanillaBisemanalConsulta.Show()
            MsgBox("Bienvenido a la  planilla Bisemanal")
        End If

    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Close()
        frmPrincipal.Show()

    End Sub

    Private Sub frmPlanillaMensuals_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        frmPrincipal.Show()

    End Sub


    Private Sub validarTelefono_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles AsientoTextBox.KeyPress

        Dim objeto As TextBox
        objeto = CType(sender, TextBox)

        If (Char.IsLetter(e.KeyChar) And e.KeyChar <> Microsoft.VisualBasic.ChrW(0)) Then
            e.Handled = True

            ToolStripStatusLabel1.Text = "Sólo se ingresa numero en el campo"
            ErrorProvider1.SetError(objeto, "Sólo se ingresa numero en el campo")
            Beep()
        Else
            ErrorProvider1.Clear()
            ToolStripStatusLabel1.Text = "Status"

        End If
    End Sub

    Private Sub AsientoTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AsientoTextBox.TextChanged

    End Sub

    Private Sub HORAS_EXTRAS_DOBLESTextBox_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles HORAS_EXTRAS_REGULARTextBox.KeyPress, HORAS_EXTRAS_DOBLESTextBox.KeyPress, DIAS_TRABAJTextBox.KeyPress

        HORAS_EXTRAS_REGULARTextBox.MaxLength = 2
        HORAS_EXTRAS_DOBLESTextBox.MaxLength = 2
        DIAS_TRABAJTextBox.MaxLength = 2
    End Sub
End Class

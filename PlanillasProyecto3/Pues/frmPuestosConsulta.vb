﻿Public Class frmPuestosConsulta

    Public Shared codPuesto As Integer

    Private Sub PuestosBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.PuestosBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)

    End Sub

    Private Sub frmPuestosConsulta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Planilla2DataSet.Puestos' table. You can move, or remove it, as needed.
        Me.PuestosTableAdapter.Fill(Me.Planilla2DataSet.Puestos)
        txtDatoConsulta.Select()
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        buscar()
    End Sub


    Sub buscar()

        Try
            Dim departamento As String = txtDatoConsulta.Text


            If Me.PuestosTableAdapter.FillBy2buscarPuesto(Me.Planilla2DataSet.Puestos, departamento) = "0" Then
                ''Me.PuestosTableAdapter.FillBy2buscarPuesto(Me.Planilla2DataSet.Puestos, departamento)
                ''Else
                MsgBox("No se Encontro Ningun Registro con este Nombre de Departamento")
                txtDatoConsulta.Clear()
                txtDatoConsulta.Focus()
                Me.PuestosTableAdapter.Fill(Me.Planilla2DataSet.Puestos)

            End If



        Catch ex As Exception
            MsgBox("No se Encontro Ningun Registro con este Nombre de Departamento")
            txtDatoConsulta.Clear()
            txtDatoConsulta.Focus()
            Me.PuestosTableAdapter.Fill(Me.Planilla2DataSet.Puestos)

        End Try

    End Sub

    
    Private Sub PuestosDataGridView_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PuestosDataGridView.DoubleClick

        codPuesto = Integer.Parse(PuestosDataGridView.Item(0, PuestosDataGridView.CurrentRow.Index).Value)


        Select Case frmPuestosActualizar.tipoFormularioPuestoValidar
            Case "Actualizar"
                frmPuestosActualizar.PuestosTableAdapter.FillByBuscarPorCodigo(frmPuestosActualizar.Planilla2DataSet.Puestos, codPuesto)
                Me.Close()
                frmPuestosActualizar.Show()
            Case "Eliminar"

                frmPuestoEliminar.PuestosTableAdapter.FillByBuscarPorCodigo(frmPuestoEliminar.Planilla2DataSet.Puestos, codPuesto)
                frmPuestoEliminar.txtDatoConsulta.Text = codPuesto
                Me.Close()
                frmPuestoEliminar.Show()

            Case "Ajuste"
                frmAjustes.PuestosTableAdapter1.FillByPuesto(frmAjustes.Planilla2DataSet1.Puestos, codPuesto)
                'frmAjustes.PuestosTableAdapter.FillByBuscarPorCodigo(frmAjustes.Planilla2DataSet.Puestos, codPuesto)
                If frmAjustes.CodPUESTO_NUEVOTextBox.Text <> "" Then
                    frmAjustes.ComboBox2.Enabled = True
                Else
                    frmAjustes.ComboBox2.Enabled = False
                End If
                Me.Close()
                frmAjustes.Show()
            Case Else

        End Select

    End Sub



    Private Sub ConsultarDatosToolStripMenuItem_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultarDatosToolStripMenuItem.MouseHover
        ToolStripStatusLabel1.Text = "Consultar Los Datos que estan Guardados"
    End Sub

    Private Sub btnBuscar_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.MouseHover, btnBuscar.MouseHover
        ToolStripStatusLabel1.Text = "Consultar Los Datos que estan Guardados"
    End Sub

    Private Sub RegesarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegesarToolStripMenuItem.Click
        Me.Hide()
        frmPrincipal.Show()
    End Sub

    Private Sub ToolStripButton6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton6.Click
        Me.Hide()
        frmPrincipal.Show()
    End Sub

    Private Sub RegesarToolStripMenuItem_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegesarToolStripMenuItem.MouseHover
        ToolStripStatusLabel1.Text = "Regresar A la Pagina Principal"
    End Sub

    
    Private Sub ToolStripButton6_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton6.MouseHover
        ToolStripStatusLabel1.Text = "Regresar A la Pagina Principal"
    End Sub

    Private Sub btnBuscar_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDatoConsulta.MouseLeave, ToolStripButton6.MouseLeave, ToolStripButton2.MouseLeave, btnBuscar.MouseLeave, ArchivoToolStripMenuItem.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    Private Sub RegesarToolStripMenuItem_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegesarToolStripMenuItem.MouseLeave, MyBase.MouseLeave, AccionesToolStripMenuItem.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    

    Private Sub ConsultarDatosToolStripMenuItem_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultarDatosToolStripMenuItem.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    Private Sub txtDatoConsulta_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDatoConsulta.MouseHover
        ToolStripStatusLabel1.Text = "Digitar el Nombre Del Departamento"
    End Sub


    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click
        buscar()
    End Sub
End Class
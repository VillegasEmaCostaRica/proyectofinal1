﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPuestosActualizar
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CODIGO_DE_PUESTOLabel As System.Windows.Forms.Label
        Dim DESCRIPCIONLabel As System.Windows.Forms.Label
        Dim DEPARTAMENTOLabel As System.Windows.Forms.Label
        Dim SALARIO_UNICOLabel As System.Windows.Forms.Label
        Dim EMPLEADO_CONFIANZALabel As System.Windows.Forms.Label
        Dim GRADO_ACADEMICOLabel As System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnGaurdar = New System.Windows.Forms.Button()
        Me.CODIGO_DE_PUESTOTextBox = New System.Windows.Forms.TextBox()
        Me.PuestosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Planilla2DataSet = New PlanillasProyecto3.Planilla2DataSet()
        Me.DESCRIPCIONTextBox = New System.Windows.Forms.TextBox()
        Me.DEPARTAMENTOTextBox = New System.Windows.Forms.TextBox()
        Me.SALARIO_UNICOTextBox = New System.Windows.Forms.TextBox()
        Me.EMPLEADO_CONFIANZACheckBox = New System.Windows.Forms.CheckBox()
        Me.GRADO_ACADEMICOTextBox = New System.Windows.Forms.TextBox()
        Me.PuestosTableAdapter = New PlanillasProyecto3.Planilla2DataSetTableAdapters.PuestosTableAdapter()
        Me.TableAdapterManager = New PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnConsultar = New System.Windows.Forms.Button()
        Me.txtDatoConsulta = New System.Windows.Forms.TextBox()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton5 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton6 = New System.Windows.Forms.ToolStripButton()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ArchivoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RegesarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AccionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GuardarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ActulizarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolTip2 = New System.Windows.Forms.ToolTip(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        CODIGO_DE_PUESTOLabel = New System.Windows.Forms.Label()
        DESCRIPCIONLabel = New System.Windows.Forms.Label()
        DEPARTAMENTOLabel = New System.Windows.Forms.Label()
        SALARIO_UNICOLabel = New System.Windows.Forms.Label()
        EMPLEADO_CONFIANZALabel = New System.Windows.Forms.Label()
        GRADO_ACADEMICOLabel = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        CType(Me.PuestosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Planilla2DataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStrip1.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CODIGO_DE_PUESTOLabel
        '
        CODIGO_DE_PUESTOLabel.AutoSize = True
        CODIGO_DE_PUESTOLabel.Location = New System.Drawing.Point(88, 43)
        CODIGO_DE_PUESTOLabel.Name = "CODIGO_DE_PUESTOLabel"
        CODIGO_DE_PUESTOLabel.Size = New System.Drawing.Size(52, 13)
        CODIGO_DE_PUESTOLabel.TabIndex = 0
        CODIGO_DE_PUESTOLabel.Text = "CODIGO:"
        '
        'DESCRIPCIONLabel
        '
        DESCRIPCIONLabel.AutoSize = True
        DESCRIPCIONLabel.Location = New System.Drawing.Point(57, 66)
        DESCRIPCIONLabel.Name = "DESCRIPCIONLabel"
        DESCRIPCIONLabel.Size = New System.Drawing.Size(83, 13)
        DESCRIPCIONLabel.TabIndex = 2
        DESCRIPCIONLabel.Text = "DESCRIPCION:"
        '
        'DEPARTAMENTOLabel
        '
        DEPARTAMENTOLabel.AutoSize = True
        DEPARTAMENTOLabel.Location = New System.Drawing.Point(40, 95)
        DEPARTAMENTOLabel.Name = "DEPARTAMENTOLabel"
        DEPARTAMENTOLabel.Size = New System.Drawing.Size(100, 13)
        DEPARTAMENTOLabel.TabIndex = 4
        DEPARTAMENTOLabel.Text = "DEPARTAMENTO:"
        '
        'SALARIO_UNICOLabel
        '
        SALARIO_UNICOLabel.AutoSize = True
        SALARIO_UNICOLabel.Location = New System.Drawing.Point(47, 121)
        SALARIO_UNICOLabel.Name = "SALARIO_UNICOLabel"
        SALARIO_UNICOLabel.Size = New System.Drawing.Size(93, 13)
        SALARIO_UNICOLabel.TabIndex = 6
        SALARIO_UNICOLabel.Text = "SALARIO UNICO:"
        '
        'EMPLEADO_CONFIANZALabel
        '
        EMPLEADO_CONFIANZALabel.AutoSize = True
        EMPLEADO_CONFIANZALabel.Location = New System.Drawing.Point(71, 147)
        EMPLEADO_CONFIANZALabel.Name = "EMPLEADO_CONFIANZALabel"
        EMPLEADO_CONFIANZALabel.Size = New System.Drawing.Size(69, 13)
        EMPLEADO_CONFIANZALabel.TabIndex = 8
        EMPLEADO_CONFIANZALabel.Text = "EMPLEADO:"
        '
        'GRADO_ACADEMICOLabel
        '
        GRADO_ACADEMICOLabel.AutoSize = True
        GRADO_ACADEMICOLabel.Location = New System.Drawing.Point(25, 180)
        GRADO_ACADEMICOLabel.Name = "GRADO_ACADEMICOLabel"
        GRADO_ACADEMICOLabel.Size = New System.Drawing.Size(115, 13)
        GRADO_ACADEMICOLabel.TabIndex = 10
        GRADO_ACADEMICOLabel.Text = "GRADO ACADEMICO:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnGaurdar)
        Me.GroupBox1.Controls.Add(CODIGO_DE_PUESTOLabel)
        Me.GroupBox1.Controls.Add(Me.CODIGO_DE_PUESTOTextBox)
        Me.GroupBox1.Controls.Add(DESCRIPCIONLabel)
        Me.GroupBox1.Controls.Add(Me.DESCRIPCIONTextBox)
        Me.GroupBox1.Controls.Add(DEPARTAMENTOLabel)
        Me.GroupBox1.Controls.Add(Me.DEPARTAMENTOTextBox)
        Me.GroupBox1.Controls.Add(SALARIO_UNICOLabel)
        Me.GroupBox1.Controls.Add(Me.SALARIO_UNICOTextBox)
        Me.GroupBox1.Controls.Add(EMPLEADO_CONFIANZALabel)
        Me.GroupBox1.Controls.Add(Me.EMPLEADO_CONFIANZACheckBox)
        Me.GroupBox1.Controls.Add(GRADO_ACADEMICOLabel)
        Me.GroupBox1.Controls.Add(Me.GRADO_ACADEMICOTextBox)
        Me.GroupBox1.Location = New System.Drawing.Point(85, 135)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(321, 274)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Puesto"
        '
        'btnGaurdar
        '
        Me.btnGaurdar.Image = Global.PlanillasProyecto3.My.Resources.Resources.guardar_documento_icono_7840_48
        Me.btnGaurdar.Location = New System.Drawing.Point(141, 214)
        Me.btnGaurdar.Name = "btnGaurdar"
        Me.btnGaurdar.Size = New System.Drawing.Size(75, 54)
        Me.btnGaurdar.TabIndex = 6
        Me.btnGaurdar.Text = "&Guardar"
        Me.ToolTip1.SetToolTip(Me.btnGaurdar, "Guadar Puesto o Actualizacion De Puesto")
        Me.btnGaurdar.UseVisualStyleBackColor = True
        '
        'CODIGO_DE_PUESTOTextBox
        '
        Me.CODIGO_DE_PUESTOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PuestosBindingSource, "CODIGO DE PUESTO", True))
        Me.CODIGO_DE_PUESTOTextBox.Location = New System.Drawing.Point(164, 43)
        Me.CODIGO_DE_PUESTOTextBox.Name = "CODIGO_DE_PUESTOTextBox"
        Me.CODIGO_DE_PUESTOTextBox.Size = New System.Drawing.Size(104, 20)
        Me.CODIGO_DE_PUESTOTextBox.TabIndex = 1
        Me.ToolTip1.SetToolTip(Me.CODIGO_DE_PUESTOTextBox, "Codigo Del Puesto ")
        '
        'PuestosBindingSource
        '
        Me.PuestosBindingSource.DataMember = "Puestos"
        Me.PuestosBindingSource.DataSource = Me.Planilla2DataSet
        '
        'Planilla2DataSet
        '
        Me.Planilla2DataSet.DataSetName = "Planilla2DataSet"
        Me.Planilla2DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DESCRIPCIONTextBox
        '
        Me.DESCRIPCIONTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PuestosBindingSource, "DESCRIPCION", True))
        Me.DESCRIPCIONTextBox.Location = New System.Drawing.Point(164, 69)
        Me.DESCRIPCIONTextBox.Name = "DESCRIPCIONTextBox"
        Me.DESCRIPCIONTextBox.Size = New System.Drawing.Size(104, 20)
        Me.DESCRIPCIONTextBox.TabIndex = 1
        Me.ToolTip1.SetToolTip(Me.DESCRIPCIONTextBox, "Descripcion del Puesto")
        '
        'DEPARTAMENTOTextBox
        '
        Me.DEPARTAMENTOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PuestosBindingSource, "DEPARTAMENTO", True))
        Me.DEPARTAMENTOTextBox.Location = New System.Drawing.Point(164, 95)
        Me.DEPARTAMENTOTextBox.Name = "DEPARTAMENTOTextBox"
        Me.DEPARTAMENTOTextBox.Size = New System.Drawing.Size(104, 20)
        Me.DEPARTAMENTOTextBox.TabIndex = 2
        Me.ToolTip1.SetToolTip(Me.DEPARTAMENTOTextBox, "Departamento Que le Pertenece el Puesto")
        '
        'SALARIO_UNICOTextBox
        '
        Me.SALARIO_UNICOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PuestosBindingSource, "SALARIO UNICO", True))
        Me.SALARIO_UNICOTextBox.Location = New System.Drawing.Point(164, 122)
        Me.SALARIO_UNICOTextBox.Name = "SALARIO_UNICOTextBox"
        Me.SALARIO_UNICOTextBox.Size = New System.Drawing.Size(104, 20)
        Me.SALARIO_UNICOTextBox.TabIndex = 3
        Me.ToolTip1.SetToolTip(Me.SALARIO_UNICOTextBox, "Salario Unico Del Puesto")
        '
        'EMPLEADO_CONFIANZACheckBox
        '
        Me.EMPLEADO_CONFIANZACheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.PuestosBindingSource, "EMPLEADO CONFIANZA", True))
        Me.EMPLEADO_CONFIANZACheckBox.Location = New System.Drawing.Point(164, 147)
        Me.EMPLEADO_CONFIANZACheckBox.Name = "EMPLEADO_CONFIANZACheckBox"
        Me.EMPLEADO_CONFIANZACheckBox.Size = New System.Drawing.Size(104, 24)
        Me.EMPLEADO_CONFIANZACheckBox.TabIndex = 4
        Me.EMPLEADO_CONFIANZACheckBox.Text = "Si"
        Me.ToolTip1.SetToolTip(Me.EMPLEADO_CONFIANZACheckBox, "Si es Empleado De Confianza")
        Me.EMPLEADO_CONFIANZACheckBox.UseVisualStyleBackColor = True
        '
        'GRADO_ACADEMICOTextBox
        '
        Me.GRADO_ACADEMICOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PuestosBindingSource, "GRADO ACADEMICO", True))
        Me.GRADO_ACADEMICOTextBox.Location = New System.Drawing.Point(164, 177)
        Me.GRADO_ACADEMICOTextBox.Name = "GRADO_ACADEMICOTextBox"
        Me.GRADO_ACADEMICOTextBox.Size = New System.Drawing.Size(104, 20)
        Me.GRADO_ACADEMICOTextBox.TabIndex = 5
        Me.ToolTip1.SetToolTip(Me.GRADO_ACADEMICOTextBox, "Grado Academico que Requiere el Puesto")
        '
        'PuestosTableAdapter
        '
        Me.PuestosTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.AguinaldoTableAdapter = Nothing
        Me.TableAdapterManager.AjustesTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.DatosPersonalesTableAdapter = Nothing
        Me.TableAdapterManager.DeduccionesTableAdapter = Nothing
        Me.TableAdapterManager.ImpuestosTableAdapter = Nothing
        Me.TableAdapterManager.PlanillaBisemanalTableAdapter = Nothing
        Me.TableAdapterManager.PlanillaMensualTableAdapter = Nothing
        Me.TableAdapterManager.PuestosTableAdapter = Me.PuestosTableAdapter
        Me.TableAdapterManager.UpdateOrder = PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.UsuarioRegistroTableAdapter = Nothing
        Me.TableAdapterManager.VacacionesTableAdapter = Nothing
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnConsultar)
        Me.GroupBox2.Controls.Add(Me.txtDatoConsulta)
        Me.GroupBox2.Controls.Add(Me.btnBuscar)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Location = New System.Drawing.Point(465, 167)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(260, 144)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Ingresar Codigo Puesto Para Actualizar"
        '
        'btnConsultar
        '
        Me.btnConsultar.Image = Global.PlanillasProyecto3.My.Resources.Resources._1355196609_old_edit_find
        Me.btnConsultar.Location = New System.Drawing.Point(145, 60)
        Me.btnConsultar.Name = "btnConsultar"
        Me.btnConsultar.Size = New System.Drawing.Size(75, 54)
        Me.btnConsultar.TabIndex = 2
        Me.btnConsultar.Text = "&Consultar Puetos"
        Me.ToolTip1.SetToolTip(Me.btnConsultar, "Consultar Los Puestos Que Estan Guardados")
        Me.btnConsultar.UseVisualStyleBackColor = True
        '
        'txtDatoConsulta
        '
        Me.txtDatoConsulta.Location = New System.Drawing.Point(120, 34)
        Me.txtDatoConsulta.Name = "txtDatoConsulta"
        Me.txtDatoConsulta.Size = New System.Drawing.Size(100, 20)
        Me.txtDatoConsulta.TabIndex = 0
        Me.ToolTip1.SetToolTip(Me.txtDatoConsulta, "Dato Por el Cual se va hacer la Busqueda")
        '
        'btnBuscar
        '
        Me.btnBuscar.Image = Global.PlanillasProyecto3.My.Resources.Resources.iconoBuscarAutor
        Me.btnBuscar.Location = New System.Drawing.Point(52, 60)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(75, 54)
        Me.btnBuscar.TabIndex = 1
        Me.btnBuscar.Text = "&Buscar"
        Me.ToolTip1.SetToolTip(Me.btnBuscar, "Buscar el Puesto Para Actualizar")
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(14, 35)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 16)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "Código Puesto:"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.PlanillasProyecto3.My.Resources.Resources.descarga__1_
        Me.PictureBox1.Location = New System.Drawing.Point(465, 167)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(219, 279)
        Me.PictureBox1.TabIndex = 52
        Me.PictureBox1.TabStop = False
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.ToolStripButton2, Me.ToolStripButton5, Me.ToolStripButton6})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 24)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(793, 25)
        Me.ToolStrip1.TabIndex = 49
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton1.Image = Global.PlanillasProyecto3.My.Resources.Resources.guardar_documento_icono_7840_48
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton1.Text = "Guardar"
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton2.Image = Global.PlanillasProyecto3.My.Resources.Resources.icono_modificar
        Me.ToolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton2.Text = "Modificar"
        '
        'ToolStripButton5
        '
        Me.ToolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton5.Image = Global.PlanillasProyecto3.My.Resources.Resources.RegisterIcon1
        Me.ToolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton5.Name = "ToolStripButton5"
        Me.ToolStripButton5.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton5.Text = "Nuevo registro"
        '
        'ToolStripButton6
        '
        Me.ToolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton6.Image = Global.PlanillasProyecto3.My.Resources.Resources.icono_retroceso
        Me.ToolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton6.Name = "ToolStripButton6"
        Me.ToolStripButton6.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton6.Text = "Regresar"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ArchivoToolStripMenuItem, Me.AccionesToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(793, 24)
        Me.MenuStrip1.TabIndex = 50
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ArchivoToolStripMenuItem
        '
        Me.ArchivoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RegesarToolStripMenuItem})
        Me.ArchivoToolStripMenuItem.Name = "ArchivoToolStripMenuItem"
        Me.ArchivoToolStripMenuItem.Size = New System.Drawing.Size(60, 20)
        Me.ArchivoToolStripMenuItem.Text = "&Archivo"
        '
        'RegesarToolStripMenuItem
        '
        Me.RegesarToolStripMenuItem.Name = "RegesarToolStripMenuItem"
        Me.RegesarToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.F), System.Windows.Forms.Keys)
        Me.RegesarToolStripMenuItem.Size = New System.Drawing.Size(155, 22)
        Me.RegesarToolStripMenuItem.Text = "&Regesar"
        Me.RegesarToolStripMenuItem.ToolTipText = "Regresar"
        '
        'AccionesToolStripMenuItem
        '
        Me.AccionesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GuardarToolStripMenuItem, Me.ActulizarToolStripMenuItem})
        Me.AccionesToolStripMenuItem.Name = "AccionesToolStripMenuItem"
        Me.AccionesToolStripMenuItem.Size = New System.Drawing.Size(67, 20)
        Me.AccionesToolStripMenuItem.Text = "&Acciones"
        '
        'GuardarToolStripMenuItem
        '
        Me.GuardarToolStripMenuItem.Name = "GuardarToolStripMenuItem"
        Me.GuardarToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.E), System.Windows.Forms.Keys)
        Me.GuardarToolStripMenuItem.Size = New System.Drawing.Size(196, 22)
        Me.GuardarToolStripMenuItem.Text = "&Guardar"
        Me.GuardarToolStripMenuItem.ToolTipText = "Guardar Registro"
        '
        'ActulizarToolStripMenuItem
        '
        Me.ActulizarToolStripMenuItem.Name = "ActulizarToolStripMenuItem"
        Me.ActulizarToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.F), System.Windows.Forms.Keys)
        Me.ActulizarToolStripMenuItem.Size = New System.Drawing.Size(160, 22)
        Me.ActulizarToolStripMenuItem.Text = "&Actulizar"
        Me.ActulizarToolStripMenuItem.ToolTipText = "Actualizar Un Registro"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 449)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(793, 22)
        Me.StatusStrip1.TabIndex = 51
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(121, 17)
        Me.ToolStripStatusLabel1.Text = "ToolStripStatusLabel1"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(296, 61)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 24)
        Me.Label2.TabIndex = 53
        Me.Label2.Text = "Label2"
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'frmPuestosActualizar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(793, 471)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmPuestosActualizar"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmPuestosActualizar"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.PuestosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Planilla2DataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Planilla2DataSet As PlanillasProyecto3.Planilla2DataSet
    Friend WithEvents PuestosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PuestosTableAdapter As PlanillasProyecto3.Planilla2DataSetTableAdapters.PuestosTableAdapter
    Friend WithEvents TableAdapterManager As PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager
    Friend WithEvents CODIGO_DE_PUESTOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DESCRIPCIONTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DEPARTAMENTOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SALARIO_UNICOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents EMPLEADO_CONFIANZACheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents GRADO_ACADEMICOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents btnGaurdar As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtDatoConsulta As System.Windows.Forms.TextBox
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnConsultar As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton5 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton6 As System.Windows.Forms.ToolStripButton
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents ArchivoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RegesarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AccionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GuardarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ActulizarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolTip2 As System.Windows.Forms.ToolTip
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
End Class

﻿Public Class frmPuestoEliminar

    Public Shared codPuesto As Integer

    Private Sub PuestosBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.PuestosBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)

    End Sub

    Private Sub frmPuestoEliminar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'Planilla2DataSet.Puestos' Puede moverla o quitarla según sea necesario.
        'Me.PuestosTableAdapter.Fill(Me.Planilla2DataSet.Puestos)
        txtDatoConsulta.Select()
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        eliminar()
    End Sub


    Sub eliminar()

        Try
            Me.PuestosTableAdapter.FillByBuscarPorCodigo(Me.Planilla2DataSet.Puestos, Integer.Parse(txtDatoConsulta.Text))

            If MsgBox("¿ESTÁ SEGURO DE ELIMINAR ESTE REGISTRO?", MsgBoxStyle.OkCancel, "CONFIRMAR") = MsgBoxResult.Ok Then

                Dim codPuesto As Integer = Integer.Parse(txtDatoConsulta.Text)
                Dim lleno As Integer = 0
                lleno = Me.PuestosTableAdapter.buscarPuesto(Me.Planilla2DataSet.Puestos, codPuesto)

                If lleno <> 0 Then
                    Me.PuestosTableAdapter.DeletePuesto(codPuesto)
                    MsgBox("Se Elimino satisfactoriamente La persona")
                    Me.PuestosTableAdapter.Fill(Me.Planilla2DataSet.Puestos)
                Else
                    MsgBox("No se Ecuentra Ninguana Cedula Guarda, Compruebe En el Boton De Consulta")
                End If
            End If
        Catch ex As Exception
            MsgBox(" Verifique Que la Cedula sea Númerica")
        End Try


    End Sub


    Private Sub btnConsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsultar.Click
        frmPuestosActualizar.tipoFormularioPuestoValidar = "Eliminar"
        frmPuestosConsulta.Show()
        Me.Hide()
    End Sub


    Private Sub ConsultarDatosToolStripMenuItem_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultarDatosToolStripMenuItem.MouseHover
        ToolStripStatusLabel1.Text = "Consultar Los Datos que estan Guardados"
    End Sub

    Private Sub ConsultarDatosToolStripMenuItem_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultarDatosToolStripMenuItem.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    Private Sub btnEliminar_MouseMove(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnEliminar.MouseMove
        ToolStripStatusLabel1.Text = "Eliminar Puesto Por el Numero de Codigo"
    End Sub

    Private Sub ToolStripButton1_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.MouseHover
        ToolStripStatusLabel1.Text = "Eliminar Puesto Por el Numero de Codigo"
    End Sub

    Private Sub EliminarToolStripMenuItem_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EliminarToolStripMenuItem.MouseHover
        ToolStripStatusLabel1.Text = "Eliminar Puesto Por el Numero de Codigo"
    End Sub

    Private Sub EliminarToolStripMenuItem_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EliminarToolStripMenuItem.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    Private Sub RegesarToolStripMenuItem_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegesarToolStripMenuItem.MouseHover
        ToolStripStatusLabel1.Text = "Regresar A la Pagina Principal"
    End Sub

    Private Sub txtDatoConsulta_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDatoConsulta.MouseHover
        ToolStripStatusLabel1.Text = "Dato Por el cual se va a Buscar el Puesto"
    End Sub

    Private Sub ToolStripButton6_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton6.MouseHover
        ToolStripStatusLabel1.Text = "Regresar A la Pagina Principal"
    End Sub

    Private Sub btnEliminar_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.MouseHover
        ToolStripStatusLabel1.Text = "Eliminar Puesto Por el Numero de Codigo"
    End Sub

    Private Sub btnEliminar_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    Private Sub btnConsultar_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsultar.MouseHover
        ToolStripStatusLabel1.Text = "Ver todo los Puestos Guardados"
    End Sub

    Private Sub ToolStripButton2_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.MouseHover
        ToolStripStatusLabel1.Text = "Consultar Los Datos que estan Guardados"
    End Sub

    Private Sub ToolStripButton2_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    Private Sub frmPuestoEliminar_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    Private Sub EliminarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EliminarToolStripMenuItem.Click
        eliminar()
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        eliminar()
    End Sub

    Private Sub ToolStripButton6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton6.Click
        Me.Hide()
        frmPrincipal.Show()
    End Sub

    Private Sub RegesarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegesarToolStripMenuItem.Click
        Me.Hide()
        frmPrincipal.Show()
    End Sub

    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click
        frmPuestosActualizar.tipoFormularioPuestoValidar = "Eliminar"
        frmPuestosConsulta.Show()
        Me.Hide()
    End Sub
End Class
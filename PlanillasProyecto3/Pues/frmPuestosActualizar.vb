﻿Public Class frmPuestosActualizar
    Public Shared tipoFormularioPuestoValidar As String
    Public Shared empleadoConfianza As Boolean

    Private Sub PuestosBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.PuestosBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)

    End Sub

    Private Sub frmPuestosActualizar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ToolStripStatusLabel1.Text = "Status"
        'TODO: esta línea de código carga datos en la tabla 'Planilla2DataSet.Puestos' Puede moverla o quitarla según sea necesario.
        'Me.PuestosTableAdapter.Fill(Me.Planilla2DataSet.Puestos)

    End Sub

    Private Sub EMPLEADO_CONFIANZACheckBox_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EMPLEADO_CONFIANZACheckBox.Enter
        ' EMPLEADO_CONFIANZACheckBox.Checked = True
    End Sub
    Sub Limpiar_Contenedores()

        Dim c As New Control

        For Each c In GroupBox1.Controls

            If TypeOf c Is TextBox Then

                CType(c, TextBox).Clear()

            End If

        Next

    End Sub
    Private Sub btnGaurdar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGaurdar.Click

        guardar()
       
    End Sub

    Sub guardar()
        If EMPLEADO_CONFIANZACheckBox.Checked = True Then
            empleadoConfianza = True
        Else
            empleadoConfianza = False

        End If

        Select Case frmPrincipal.nomForm
            Case "Guardar"
                Try
                    Me.PuestosTableAdapter.InsertPuesto(Integer.Parse(CODIGO_DE_PUESTOTextBox.Text), DESCRIPCIONTextBox.Text, DEPARTAMENTOTextBox.Text, Integer.Parse(SALARIO_UNICOTextBox.Text), empleadoConfianza, GRADO_ACADEMICOTextBox.Text)
                    Me.PuestosTableAdapter.Fill(Me.Planilla2DataSet.Puestos)
                    MsgBox("Se Guardo Correctamente los Datos del Puesto Nuevo ")
                Catch ex As Exception
                    MsgBox("No Se Guardo los Datos porque Coincide con un Codigo ya Guardado ")
                End Try
            Case "Actualizar"
                Try

                    Me.PuestosTableAdapter.UpdatePuesto(Integer.Parse(CODIGO_DE_PUESTOTextBox.Text), DESCRIPCIONTextBox.Text, DEPARTAMENTOTextBox.Text, Integer.Parse(SALARIO_UNICOTextBox.Text), empleadoConfianza, GRADO_ACADEMICOTextBox.Text, frmPuestosConsulta.codPuesto)
                    MsgBox("Se Guardo Correctamente La Actualizacion De los Datos ")
                Catch ex As Exception
                    MsgBox("Verifique El Codigo Que Sea Correcto")
                End Try

            Case Else

        End Select



    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click

        Try

            Dim codPuesto As Integer = Integer.Parse(txtDatoConsulta.Text)


            If Me.PuestosTableAdapter.FillByBuscarPorCodigo(Me.Planilla2DataSet.Puestos, codPuesto) = "0" Then
                MsgBox("No se Encuentra Ningun Dato Con este Codigo")
                txtDatoConsulta.SelectAll()
            End If

        Catch ex As Exception
            MsgBox("Verifique El Codigo Que Sea Correcto")
        End Try
    End Sub

    Private Sub btnConsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsultar.Click
        tipoDeFormularioPuesto("Actualizar")

    End Sub

    Sub tipoDeFormularioPuesto(ByVal NomFormulario As String)


        Select Case NomFormulario
            Case "Actualizar"
                Me.Hide()
                frmPuestosConsulta.Show()
                tipoFormularioPuestoValidar = "Actualizar"
            Case "Eliminar"
                frmPuestoEliminar.Hide()
                frmPuestosConsulta.Show()
                tipoFormularioPuestoValidar = "Eliminar"
            Case Else

        End Select


    End Sub

    Private Sub ToolStripButton1_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDatoConsulta.MouseLeave, ToolStripButton6.MouseLeave, ToolStripButton5.MouseLeave, ToolStripButton2.MouseLeave, ToolStripButton1.MouseLeave, SALARIO_UNICOTextBox.MouseLeave, GRADO_ACADEMICOTextBox.MouseLeave, EMPLEADO_CONFIANZACheckBox.MouseLeave, DESCRIPCIONTextBox.MouseLeave, DEPARTAMENTOTextBox.MouseLeave, CODIGO_DE_PUESTOTextBox.MouseLeave, btnGaurdar.MouseLeave, btnConsultar.MouseLeave, btnBuscar.MouseLeave, ArchivoToolStripMenuItem.MouseLeave, AccionesToolStripMenuItem.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    
    Private Sub AccionesToolStripMenuItem_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.MouseHover, GuardarToolStripMenuItem.MouseHover, btnGaurdar.MouseHover, AccionesToolStripMenuItem.MouseHover
        ToolStripStatusLabel1.Text = "Guardar el Nuevo Puesto"
    End Sub

    Private Sub CODIGO_DE_PUESTOTextBox_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CODIGO_DE_PUESTOTextBox.MouseHover
        ToolStripStatusLabel1.Text = "Codigo Del Puesto "
    End Sub

    Private Sub DESCRIPCIONTextBox_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DESCRIPCIONTextBox.MouseHover
        ToolStripStatusLabel1.Text = "Descripcion del Puesto"
    End Sub

    Private Sub DEPARTAMENTOTextBox_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DEPARTAMENTOTextBox.MouseHover
        ToolStripStatusLabel1.Text = "Departamento Que le Pertenece el Puesto"
    End Sub

    Private Sub SALARIO_UNICOTextBox_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SALARIO_UNICOTextBox.MouseHover
        ToolStripStatusLabel1.Text = "Salario Unico Del Puesto"
    End Sub

    Private Sub EMPLEADO_CONFIANZACheckBox_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EMPLEADO_CONFIANZACheckBox.MouseHover
        ToolStripStatusLabel1.Text = "Si es Empleado De Confianza"
    End Sub

    Private Sub GRADO_ACADEMICOTextBox_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GRADO_ACADEMICOTextBox.MouseHover
        ToolStripStatusLabel1.Text = "Grado Academico que Requiere el Puesto"
    End Sub

    Private Sub btnBuscar_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.MouseHover
        ToolStripStatusLabel1.Text = "Buscar el Puesto Para Actualizar"
    End Sub

    Private Sub btnConsultar_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsultar.MouseHover
        ToolStripStatusLabel1.Text = "Consultar Los Puestos Que Estan Guardados"
    End Sub

    Private Sub txtDatoConsulta_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDatoConsulta.MouseHover
        ToolStripStatusLabel1.Text = "Dato Por el Cual se va hacer la Busqueda"
    End Sub

    Private Sub ToolStripButton6_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton6.MouseHover
        ToolStripStatusLabel1.Text = "Regresar a la Pagina Principal"
    End Sub

    Private Sub RegesarToolStripMenuItem_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegesarToolStripMenuItem.MouseHover
        ToolStripStatusLabel1.Text = "Regresar a la Pagina Principal"
    End Sub

    Private Sub RegesarToolStripMenuItem_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegesarToolStripMenuItem.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    Private Sub ToolStripButton6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton6.Click
        Limpiar_Contenedores()
        Me.Hide()
        frmPrincipal.Show()
    End Sub

    Private Sub RegesarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegesarToolStripMenuItem.Click
        Limpiar_Contenedores()
        Me.Hide()
        frmPrincipal.Show()
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        guardar()
    End Sub

    Private Sub GuardarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GuardarToolStripMenuItem.Click
        guardar()
    End Sub

    Private Sub ActulizarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ActulizarToolStripMenuItem.Click
        guardar()
    End Sub

    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click
        guardar()
    End Sub

    Private Sub CODIGO_DE_PUESTOTextBox_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles SALARIO_UNICOTextBox.Validating, CODIGO_DE_PUESTOTextBox.Validating
        Dim textbox As TextBox = CType(sender, TextBox)
        Try
            Dim num As Integer = Integer.Parse(textbox.Text)
        Catch ex As Exception
            e.Cancel = True
            textbox.SelectAll()
            ErrorProvider1.SetError(textbox, "Tiene Que ser Numerico")
        End Try
    End Sub

    Private Sub DESCRIPCIONTextBox_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles GRADO_ACADEMICOTextBox.Validating, DESCRIPCIONTextBox.Validating, DEPARTAMENTOTextBox.Validating
        Dim textbox As TextBox = CType(sender, TextBox)

        If textbox.Text = "" Then
            e.Cancel = True
            textbox.SelectAll()
            ErrorProvider1.SetError(textbox, "Campo Requerido")
        End If


    End Sub

    Private Sub CODIGO_DE_PUESTOTextBox_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SALARIO_UNICOTextBox.Validated, GRADO_ACADEMICOTextBox.Validated, DESCRIPCIONTextBox.Validated, DEPARTAMENTOTextBox.Validated, CODIGO_DE_PUESTOTextBox.Validated
        ErrorProvider1.Clear()
    End Sub
End Class
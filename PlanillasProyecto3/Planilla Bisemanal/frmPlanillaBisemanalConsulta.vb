﻿Public Class frmPlanillaBisemanalConsulta

    Private Sub frmPlanillaBisemanalConsulta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Planilla2DataSet1.PlanillaBisemanal' table. You can move, or remove it, as needed.
        Me.PlanillaBisemanalTableAdapter1.Fill(Me.Planilla2DataSet1.PlanillaBisemanal)
        'TODO: This line of code loads data into the 'Planilla2DataSet.PlanillaBisemanal' table. You can move, or remove it, as needed.
    
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        opcions()


    End Sub

    Sub opcions()

        If ComboBox1.SelectedIndex = 0 Then
            PlanillaBisemanalTableAdapter1.FillByBisemanallFecha(Me.Planilla2DataSet1.PlanillaBisemanal, Date.Parse(TextBox1.Text))
        End If

            If ComboBox1.SelectedIndex = 1 Then
            PlanillaBisemanalTableAdapter1.FillByBisemanalCedula(Me.Planilla2DataSet1.PlanillaBisemanal, Integer.Parse(TextBox1.Text))
            End If

            If ComboBox1.SelectedIndex = 2 Then

                If MsgBox("Seguro desea eliminar el registro", vbYesNo) = vbYes Then
                PlanillaBisemanalTableAdapter1.DeleteQueryBisemanallFecha(Date.Parse(TextBox1.Text))
                Me.PlanillaBisemanalTableAdapter1.Fill(Me.Planilla2DataSet1.PlanillaBisemanal)
                MsgBox("Registro eliminado")
                End If

            End If
            If ComboBox1.SelectedIndex = 3 Then
                If MsgBox("Seguro desea eliminar el registro", vbYesNo) = vbYes Then
                    PlanillaBisemanalTableAdapter1.DeleteQueryBisemanalCedula(Integer.Parse(TextBox1.Text))
                Me.PlanillaBisemanalTableAdapter1.Fill(Me.Planilla2DataSet1.PlanillaBisemanal)
                MsgBox("Registro eliminado")
                End If
            End If
    End Sub

    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click
        opcions()

    End Sub

    Private Sub BuscarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BuscarToolStripMenuItem.Click
        opcions()

    End Sub

    Private Sub MostarLosRegistrosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MostarLosRegistrosToolStripMenuItem.Click
        Me.PlanillaBisemanalTableAdapter1.Fill(Me.Planilla2DataSet1.PlanillaBisemanal)
            
    End Sub

    Private Sub ToolStripButton4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton4.Click
        Me.PlanillaBisemanalTableAdapter1.Fill(Me.Planilla2DataSet1.PlanillaBisemanal)
        
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        opcions()

    End Sub

    Private Sub ToolStripButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton3.Click
        Me.Close()
        frmPrincipal.Show()
    End Sub

    Private Sub RegresarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegresarToolStripMenuItem.Click
        Me.Close()
        frmPrincipal.Show()

    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged
        If IsNumeric(TextBox1.Text) = True Then
            ErrorProvider1.Clear()
        Else
            ErrorProvider1.SetError(sender, "Sólo se ingresa texto en el campo")

        End If
    End Sub

End Class
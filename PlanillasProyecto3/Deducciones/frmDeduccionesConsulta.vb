﻿Public Class frmDeduccionesConsulta

    Private Sub DeduccionesBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.DeduccionesBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)

    End Sub

    Private Sub frmDeduccionesConsulta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Planilla2DataSet.Deducciones' table. You can move, or remove it, as needed.
        Me.DeduccionesTableAdapter.Fill(Me.Planilla2DataSet.Deducciones)
        txtDatoConsulta.Focus()

    End Sub
    Sub eliminar()
        Try
            If MsgBox("Desea Borrar el registro", vbYesNo) = vbYes Then

                Me.DeduccionesTableAdapter.DeleteQuery(Integer.Parse(txtDatoConsulta.Text))
                MsgBox("Registro borrado exitosamente")
                txtDatoConsulta.Text = 0
            Else

            End If
        Catch ex As Exception
            MsgBox("Error" + ex.Message)
        End Try
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Try
            Me.DeduccionesTableAdapter.FillBy(Me.Planilla2DataSet.Deducciones, Integer.Parse(txtDatoConsulta.Text))
        Catch ex As Exception
            MsgBox("Verifique los parametros de busqueda: " + ex.Message)
        End Try
    End Sub

    Private Sub btnMostrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMostrar.Click
        Me.DeduccionesTableAdapter.Fill(Me.Planilla2DataSet.Deducciones)
    End Sub


    Private Sub DeduccionesDataGridView_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DeduccionesDataGridView.CellClick
        ToolStripStatusLabel1.Text = "Doble click para seleccionar un registro  de la lista"
        Try
            txtDatoConsulta.Text = Me.DeduccionesDataGridView.Rows(e.RowIndex).Cells(0).Value()
        Catch ex As Exception

        End Try

    End Sub

    Private Sub ToolStripButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton3.Click
        Me.Close()
        frmDeducciones.Show()
    End Sub

    Private Sub frmDeduccionesConsulta_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        busquedaRegistroFormExtrangero()
    End Sub

    Sub busquedaRegistroFormExtrangero()
        Try
            If Me.txtDatoConsulta.Text = "" Then
                txtDatoConsulta.Text = 0
            Else
                My.Forms.frmDeducciones.CodDeduccionTextBox.Text = Me.txtDatoConsulta.Text
                DeduccionesTableAdapter.FillBy(My.Forms.frmDeducciones.Planilla2DataSet.Deducciones, Integer.Parse(Me.txtDatoConsulta.Text))

            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub DeduccionesDataGridView_MouseDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles DeduccionesDataGridView.MouseDoubleClick
        busquedaRegistroFormExtrangero()
        Me.Close()
        frmDeducciones.Show()
    End Sub


    Private Sub GroupBox1_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox1.Enter

    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        eliminar()
        Me.DeduccionesTableAdapter.Fill(Me.Planilla2DataSet.Deducciones)
    End Sub

    Private Sub RegresarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegresarToolStripMenuItem.Click
        Me.Close()
        frmDeducciones.Show()
    End Sub

    Private Sub BuscarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BuscarToolStripMenuItem.Click
        Try
            Me.DeduccionesTableAdapter.FillBy(Me.Planilla2DataSet.Deducciones, Integer.Parse(txtDatoConsulta.Text))
        Catch ex As Exception
            MsgBox("Verifique los parametros de busqueda: " + ex.Message)
        End Try

    End Sub

    Private Sub EliminarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EliminarToolStripMenuItem.Click
        eliminar()
        Me.DeduccionesTableAdapter.Fill(Me.Planilla2DataSet.Deducciones)

    End Sub

    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click
        Me.DeduccionesTableAdapter.Fill(Me.Planilla2DataSet.Deducciones)
    End Sub

    Private Sub ToolStripButton4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton4.Click
        eliminar()
        Me.DeduccionesTableAdapter.Fill(Me.Planilla2DataSet.Deducciones)
    End Sub

    Private Sub btnBuscar_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.MouseHover
        ToolStripStatusLabel1.Text = "Guardar"
    End Sub

    Private Sub btnBuscar_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    Private Sub btnMostrar_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMostrar.MouseHover
        ToolStripStatusLabel1.Text = "Mostar Registros"
    End Sub

    Private Sub btnMostrar_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMostrar.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    Private Sub btnEliminar_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.MouseHover
        ToolStripStatusLabel1.Text = "Eliminar"
    End Sub

    Private Sub btnEliminar_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub
End Class
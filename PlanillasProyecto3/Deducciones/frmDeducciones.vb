﻿Public Class frmDeducciones
    Private intOpcion As Integer

    Private Sub frmDeducciones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Planilla2DataSet.Deducciones' table. You can move, or remove it, as needed.
        Me.DeduccionesTableAdapter.Fill(Me.Planilla2DataSet.Deducciones)
        limmpiarCajasTexto()
    End Sub



    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        guardar()
    End Sub


    Sub guardar()
        Try

            If SumaTotalTextBox.Text = "" Or REBAJOEMBARGOTextBox.Text = "" Or Préstamos_BcoPopularTextBox.Text = "" Or PERSONALTextBox.Text = "" Or PATRONALTextBox.Text = "" Or OTRAS_DEDUCCTextBox.Text = "" Or MENSUALTextBox.Text = "" Or FondoMutualTextBox.Text = "" Or Fiesta_JefaturasTextBox.Text = "" Or Fiesta_Fin_de_AñoTextBox.Text = "" Or DEDUCCCOLEGIOTextBox.Text = "" Or CodDeduccionTextBox.Text = "" Or CATORCENALTextBox.Text = "" Or SumaTotalTextBox.Text = 0 Or REBAJOEMBARGOTextBox.Text = 0 Or Préstamos_BcoPopularTextBox.Text = 0 Or PERSONALTextBox.Text = 0 Or PATRONALTextBox.Text = 0 Or OTRAS_DEDUCCTextBox.Text = 0 Or MENSUALTextBox.Text = 0 Or FondoMutualTextBox.Text = 0 Or Fiesta_JefaturasTextBox.Text = 0 Or Fiesta_Fin_de_AñoTextBox.Text = 0 Or DEDUCCCOLEGIOTextBox.Text = 0 Or CodDeduccionTextBox.Text = 0 Or CATORCENALTextBox.Text = 0 Then
                MsgBox("Existen campos vacios: Verifique por favor")
            Else
                Me.DeduccionesTableAdapter.InsertQuery(Integer.Parse(CodDeduccionTextBox.Text), Integer.Parse(MENSUALTextBox.Text), Integer.Parse(CATORCENALTextBox.Text), Integer.Parse(PERSONALTextBox.Text), Integer.Parse(PATRONALTextBox.Text), Integer.Parse(REBAJOEMBARGOTextBox.Text), Integer.Parse(DEDUCCCOLEGIOTextBox.Text), Integer.Parse(Fiesta_JefaturasTextBox.Text), Integer.Parse(Fiesta_Fin_de_AñoTextBox.Text), Integer.Parse(OTRAS_DEDUCCTextBox.Text), Integer.Parse(FondoMutualTextBox.Text), Integer.Parse(Préstamos_BcoPopularTextBox.Text), Integer.Parse(SumaTotalTextBox.Text))
                limmpiarCajasTexto()
                MsgBox("Datos almacenados exitosamente")
            End If
        Catch frm As FormatException
            MsgBox("Sólo se aceptan números")
        Catch ex As Exception
            MsgBox("Verifique los datos de ingreso por favor")
        End Try

    End Sub

    Private Sub SumaTotalTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SumaTotalTextBox.GotFocus
        Try
            SumaTotalTextBox.Text = Integer.Parse(CodDeduccionTextBox.Text) + Integer.Parse(MENSUALTextBox.Text) + Integer.Parse(CATORCENALTextBox.Text) + Integer.Parse(PERSONALTextBox.Text) + Integer.Parse(PATRONALTextBox.Text) + Integer.Parse(REBAJOEMBARGOTextBox.Text) + Integer.Parse(DEDUCCCOLEGIOTextBox.Text) + Integer.Parse(Fiesta_JefaturasTextBox.Text) + Integer.Parse(Fiesta_Fin_de_AñoTextBox.Text) + Integer.Parse(OTRAS_DEDUCCTextBox.Text) + Integer.Parse(FondoMutualTextBox.Text) + Integer.Parse(Préstamos_BcoPopularTextBox.Text)

        Catch ex As Exception

        End Try
    End Sub


    Sub limmpiarCajasTexto()


        CodDeduccionTextBox.Text = 0
        CodDeduccionTextBox.Focus()
        MENSUALTextBox.Text = 0
        CATORCENALTextBox.Text = 0
        PERSONALTextBox.Text = 0
        PATRONALTextBox.Text = 0
        REBAJOEMBARGOTextBox.Text = 0
        DEDUCCCOLEGIOTextBox.Text = 0
        Fiesta_JefaturasTextBox.Text = 0
        Fiesta_Fin_de_AñoTextBox.Text = 0
        OTRAS_DEDUCCTextBox.Text = 0
        FondoMutualTextBox.Text = 0
        Préstamos_BcoPopularTextBox.Text = 0
        SumaTotalTextBox.Text = 0
    End Sub

    Private Sub CodDeduccionTextBox_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles REBAJOEMBARGOTextBox.Click, Préstamos_BcoPopularTextBox.Click, PERSONALTextBox.Click, PATRONALTextBox.Click, OTRAS_DEDUCCTextBox.Click, MENSUALTextBox.Click, FondoMutualTextBox.Click, Fiesta_JefaturasTextBox.Click, Fiesta_Fin_de_AñoTextBox.Click, DEDUCCCOLEGIOTextBox.Click, CodDeduccionTextBox.Click, CATORCENALTextBox.Click
        Dim objTexi As TextBox
        objTexi = CType(sender, TextBox)
        objTexi.SelectAll()
    End Sub


    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click

        Try
            Me.Hide()
            frmDeduccionesConsulta.Show()

        Catch ex As Exception

        End Try
    End Sub


    Sub eliminar()
        Try
            If SumaTotalTextBox.Text = "" Or REBAJOEMBARGOTextBox.Text = "" Or Préstamos_BcoPopularTextBox.Text = "" Or PERSONALTextBox.Text = "" Or PATRONALTextBox.Text = "" Or OTRAS_DEDUCCTextBox.Text = "" Or MENSUALTextBox.Text = "" Or FondoMutualTextBox.Text = "" Or Fiesta_JefaturasTextBox.Text = "" Or Fiesta_Fin_de_AñoTextBox.Text = "" Or DEDUCCCOLEGIOTextBox.Text = "" Or CodDeduccionTextBox.Text = "" Or CATORCENALTextBox.Text = "" Or SumaTotalTextBox.Text = 0 Or REBAJOEMBARGOTextBox.Text = 0 Or Préstamos_BcoPopularTextBox.Text = 0 Or PERSONALTextBox.Text = 0 Or PATRONALTextBox.Text = 0 Or OTRAS_DEDUCCTextBox.Text = 0 Or MENSUALTextBox.Text = 0 Or FondoMutualTextBox.Text = 0 Or Fiesta_JefaturasTextBox.Text = 0 Or Fiesta_Fin_de_AñoTextBox.Text = 0 Or DEDUCCCOLEGIOTextBox.Text = 0 Or CodDeduccionTextBox.Text = 0 Or CATORCENALTextBox.Text = 0 Then

            Else

                If MsgBox("Desea Borrar el registro", vbYesNo) = vbYes Then

                    Me.DeduccionesTableAdapter.DeleteQuery(Integer.Parse(CodDeduccionTextBox.Text))
                    MsgBox("Registro borrado exitosamente")
                    limmpiarCajasTexto()
                Else

                End If

            End If

        Catch ex As Exception
            MsgBox("Error" + ex.Message)
        End Try
    End Sub

    Private Sub btnBorrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBorrar.Click

        If verificaCamposVacio() = True Then
        Else
            eliminar()
        End If

    End Sub


    Function verificaCamposVacio() As Boolean
        Dim status As Boolean
        Try
            If CodDeduccionTextBox.Text = 0 Or MENSUALTextBox.Text = 0 Or CATORCENALTextBox.Text = 0 Or PERSONALTextBox.Text = 0 Or PATRONALTextBox.Text = 0 Or REBAJOEMBARGOTextBox.Text = 0 Or DEDUCCCOLEGIOTextBox.Text = 0 Or Fiesta_JefaturasTextBox.Text = 0 Or Fiesta_Fin_de_AñoTextBox.Text = 0 Or OTRAS_DEDUCCTextBox.Text = 0 Or FondoMutualTextBox.Text = 0 Or Préstamos_BcoPopularTextBox.Text = 0 Or SumaTotalTextBox.Text = 0 Or CodDeduccionTextBox.Text = "" Or MENSUALTextBox.Text = "" Or CATORCENALTextBox.Text = "" Or PERSONALTextBox.Text = "" Or PATRONALTextBox.Text = "" Or REBAJOEMBARGOTextBox.Text = "" Or DEDUCCCOLEGIOTextBox.Text = "" Or Fiesta_JefaturasTextBox.Text = "" Or Fiesta_Fin_de_AñoTextBox.Text = "" Or OTRAS_DEDUCCTextBox.Text = "" Or FondoMutualTextBox.Text = "" Or Préstamos_BcoPopularTextBox.Text = "" Or SumaTotalTextBox.Text = "" Then
                MsgBox("Campos vacios verifique")
                status = True
            Else
                status = False
            End If
        Catch ex As Exception
            MsgBox("Verifique los datos de entrada")
        End Try
        Return status
    End Function

    Private Sub SumaTotalTextBox_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles SumaTotalTextBox.KeyPress, REBAJOEMBARGOTextBox.KeyPress, Préstamos_BcoPopularTextBox.KeyPress, PERSONALTextBox.KeyPress, PATRONALTextBox.KeyPress, OTRAS_DEDUCCTextBox.KeyPress, MENSUALTextBox.KeyPress, FondoMutualTextBox.KeyPress, Fiesta_JefaturasTextBox.KeyPress, Fiesta_Fin_de_AñoTextBox.KeyPress, DEDUCCCOLEGIOTextBox.KeyPress, CodDeduccionTextBox.KeyPress, CATORCENALTextBox.KeyPress
        Dim objeto As TextBox
        objeto = CType(sender, TextBox)
        If (Not Char.IsNumber(e.KeyChar) And e.KeyChar <> Microsoft.VisualBasic.ChrW(8)) Then
            e.Handled = True
            ToolStripStatusLabel1.Text = "Sólo se ingresa número en el campo"
            ErrorProvider1.SetError(objeto, "Sólo se ingresa número en el campo")
            Beep()
        Else
            ErrorProvider1.Clear()
            ToolStripStatusLabel1.Text = "Status"
        End If
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        guardar()
    End Sub

    Private Sub ToolStripButton5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton5.Click
        Me.Close()
        frmPrincipal.Show()
    End Sub


    Sub modificar()
        Try


            If CodDeduccionTextBox.Text = 0 Or MENSUALTextBox.Text = 0 Or CATORCENALTextBox.Text = 0 Or PERSONALTextBox.Text = 0 Or PATRONALTextBox.Text = 0 Or REBAJOEMBARGOTextBox.Text = 0 Or DEDUCCCOLEGIOTextBox.Text = 0 Or Fiesta_JefaturasTextBox.Text = 0 Or Fiesta_Fin_de_AñoTextBox.Text = 0 Or OTRAS_DEDUCCTextBox.Text = 0 Or FondoMutualTextBox.Text = 0 Or Préstamos_BcoPopularTextBox.Text = 0 Or SumaTotalTextBox.Text = 0 Or CodDeduccionTextBox.Text = "" Or MENSUALTextBox.Text = "" Or CATORCENALTextBox.Text = "" Or PERSONALTextBox.Text = "" Or PATRONALTextBox.Text = "" Or REBAJOEMBARGOTextBox.Text = "" Or DEDUCCCOLEGIOTextBox.Text = "" Or Fiesta_JefaturasTextBox.Text = "" Or Fiesta_Fin_de_AñoTextBox.Text = "" Or OTRAS_DEDUCCTextBox.Text = "" Or FondoMutualTextBox.Text = "" Or Préstamos_BcoPopularTextBox.Text = "" Or SumaTotalTextBox.Text = "" Then
                MsgBox("Campos vacios verifique")
             Else

                If verificaCamposVacio() = True Then
                    MsgBox("No se puede realizar la modificación")
                Else

                    If MsgBox("Seguro desea modificar el registro", vbYesNo) = vbYes Then
                        DeduccionesTableAdapter.UpdateQuery(Integer.Parse(CodDeduccionTextBox.Text), Integer.Parse(MENSUALTextBox.Text), Integer.Parse(CATORCENALTextBox.Text), Integer.Parse(PERSONALTextBox.Text), Integer.Parse(PATRONALTextBox.Text), Integer.Parse(REBAJOEMBARGOTextBox.Text), Integer.Parse(DEDUCCCOLEGIOTextBox.Text), Integer.Parse(Fiesta_JefaturasTextBox.Text), Integer.Parse(Fiesta_Fin_de_AñoTextBox.Text), Integer.Parse(OTRAS_DEDUCCTextBox.Text), Integer.Parse(FondoMutualTextBox.Text), Integer.Parse(Préstamos_BcoPopularTextBox.Text), Integer.Parse(SumaTotalTextBox.Text), Integer.Parse(CodDeduccionTextBox.Text))
                        MsgBox("Registro modificado")
                        limmpiarCajasTexto()

                    End If

                End If

            End If

          Catch ex As Exception

        End Try
    End Sub

    Private Sub btnActualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActualizar.Click

        If verificaCamposVacio() = True Then
            limmpiarCajasTexto()
        ElseIf verificaCamposVacio() = False Then
            modificar()
        End If

    End Sub

    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click
        Me.Hide()
        frmDeduccionesConsulta.Show()
    End Sub

    Private Sub ToolStripButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton3.Click
        If verificaCamposVacio() = True Then
        Else
            modificar()

        End If

    End Sub

    Private Sub ToolStripButton4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton4.Click

        If verificaCamposVacio() = True Then
        Else
            eliminar()
        End If

    End Sub

    Private Sub ToolStripButton6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton6.Click
        limmpiarCajasTexto()
    End Sub

    Private Sub OTRAS_DEDUCCTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OTRAS_DEDUCCTextBox.TextChanged

    End Sub

    Private Sub RegresarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegresarToolStripMenuItem.Click

    End Sub

    Private Sub btnAgregar_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.MouseHover
        ToolStripStatusLabel1.Text = "Guardar"
    End Sub

    Private Sub btnAgregar_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    Private Sub btnBuscar_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.MouseHover
        ToolStripStatusLabel1.Text = "Buscar"
    End Sub

    Private Sub btnBuscar_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    Private Sub btnActualizar_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActualizar.MouseHover
        ToolStripStatusLabel1.Text = "Modificar"
    End Sub

    Private Sub btnActualizar_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActualizar.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    Private Sub btnBorrar_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBorrar.MouseHover
        ToolStripStatusLabel1.Text = "Eliminar"
    End Sub

    Private Sub btnBorrar_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBorrar.MouseLeave
        ToolStripStatusLabel1.Text = "Status"

    End Sub

    Private Sub RegresarToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegresarToolStripMenuItem1.Click
        Me.Close()
        frmPrincipal.Show()
    End Sub

    Private Sub IngresarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IngresarToolStripMenuItem.Click
        guardar()
    End Sub

    Private Sub BuscarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BuscarToolStripMenuItem.Click
        Try
            Me.Hide()
            frmDeduccionesConsulta.Show()

        Catch ex As Exception

        End Try
    End Sub

    Private Sub ActualizarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ActualizarToolStripMenuItem.Click
        If verificaCamposVacio() = True Then
        Else
            modificar()

        End If
    End Sub

    Private Sub BorrarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BorrarToolStripMenuItem.Click
        If verificaCamposVacio() = True Then
        Else
            eliminar()
        End If

    End Sub

    Private Sub IngresarNuevaDeducciónToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IngresarNuevaDeducciónToolStripMenuItem.Click
        limmpiarCajasTexto()
    End Sub
End Class
﻿Public Class frmDatosEmpleadoEliminar

    
    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        eliminarPersona()
    End Sub


    Sub eliminarPersona()


        Try
            Me.DatosPersonalesTableAdapter.FillBy1BuscarPersona(Me.Planilla2DataSet.DatosPersonales, Integer.Parse(txtDatoConsulta.Text))

            If MsgBox("¿ESTÁ SEGURO DE ELIMINAR ESTE REGISTRO?", MsgBoxStyle.OkCancel, "CONFIRMAR") = MsgBoxResult.Ok Then

                Dim datocedula As Integer = Integer.Parse(txtDatoConsulta.Text)
                Dim lleno As Integer = Me.DatosPersonalesTableAdapter.FillBy1BuscarSiEstaPersona(Me.Planilla2DataSet.DatosPersonales, datocedula)

                If lleno <> 0 Then
                    Me.DatosPersonalesTableAdapter.DeletePersona(datocedula)
                    MsgBox("Se Elimino satisfactoriamente La persona")
                    Me.DatosPersonalesTableAdapter.Fill(Me.Planilla2DataSet.DatosPersonales)
                Else
                    MsgBox("No se Ecuentra Ninguana Cedula Guarda, Compruebe En el Boton De Consulta")
                End If
            End If
        Catch ex As Exception
            MsgBox(" Verifique Que la Cedula sea Númerica")
        End Try

        'Me.DatosPersonalesTableAdapter.DeletePersona(Integer.Parse(txtDatoConsulta.Text))
    End Sub


    Private Sub DatosPersonalesBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.DatosPersonalesBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)

    End Sub

    Private Sub frmDatosEmpleadoEliminar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'Planilla2DataSet.DatosPersonales' Puede moverla o quitarla según sea necesario.
        'Me.DatosPersonalesTableAdapter.Fill(Me.Planilla2DataSet.DatosPersonales)

    End Sub

    Private Sub btnConsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsultar.Click
        Me.Hide()
        formdatosEmpleadosActualizar.TipoDeFormularioDatosResonales("Eliminar")

    End Sub

    Private Sub ToolStripButton6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton6.Click
        Me.Hide()
        frmPrincipal.Show()
    End Sub

    Private Sub RegesarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegesarToolStripMenuItem.Click
        Me.Hide()
        frmPrincipal.Show()
    End Sub

    Private Sub EliminarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EliminarToolStripMenuItem.Click
        eliminarPersona()
    End Sub

    Private Sub ConsultarDatosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultarDatosToolStripMenuItem.Click
        Me.Hide()
        formdatosEmpleadosActualizar.TipoDeFormularioDatosResonales("Eliminar")
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        eliminarPersona()
    End Sub

    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click
        Me.Hide()
        formdatosEmpleadosActualizar.TipoDeFormularioDatosResonales("Eliminar")
    End Sub

    Private Sub frmDatosEmpleadoEliminar_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDatoConsulta.MouseLeave, ToolStripButton6.MouseLeave, ToolStripButton2.MouseLeave, ToolStripButton1.MouseLeave, DatosPersonalesDataGridView.MouseLeave, btnEliminar.MouseLeave, btnConsultar.MouseLeave, ArchivoToolStripMenuItem.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    Private Sub RegesarToolStripMenuItem_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegesarToolStripMenuItem.MouseLeave, MyBase.MouseLeave, AccionesToolStripMenuItem.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    Private Sub btnConsultar_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsultar.MouseHover
        ToolStripStatusLabel1.Text = "Consultar Los Datos que estan Guardados"
    End Sub

    Private Sub ToolStripButton2_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.MouseHover
        ToolStripStatusLabel1.Text = "Consultar Los Datos que estan Guardados"
    End Sub

    Private Sub ConsultarDatosToolStripMenuItem_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultarDatosToolStripMenuItem.MouseHover
        ToolStripStatusLabel1.Text = "Consultar Los Datos que estan Guardados"
    End Sub

    Private Sub ConsultarDatosToolStripMenuItem_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultarDatosToolStripMenuItem.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    Private Sub btnEliminar_MouseMove(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnEliminar.MouseMove
        ToolStripStatusLabel1.Text = "Eliminar Empleado Por el Numero de Cedula"
    End Sub

    Private Sub ToolStripButton1_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.MouseHover
        ToolStripStatusLabel1.Text = "Eliminar Empleado Por el Numero de Cedula"
    End Sub

    Private Sub EliminarToolStripMenuItem_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EliminarToolStripMenuItem.MouseHover
        ToolStripStatusLabel1.Text = "Eliminar Empleado Por el Numero de Cedula"
    End Sub

    Private Sub EliminarToolStripMenuItem_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EliminarToolStripMenuItem.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    Private Sub RegesarToolStripMenuItem_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegesarToolStripMenuItem.MouseHover
        ToolStripStatusLabel1.Text = "Regresar A la Pagina Principal"
    End Sub

    Private Sub txtDatoConsulta_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDatoConsulta.MouseHover
        ToolStripStatusLabel1.Text = "Dato Por el cual se va a Buscar el Empleado"
    End Sub

    Private Sub ToolStripButton6_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton6.MouseHover
        ToolStripStatusLabel1.Text = "Regresar A la Pagina Principal"
    End Sub
End Class